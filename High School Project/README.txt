Tavernkeep is a game that I started in high school and finished during my first semester at UPEI. I am proud of the work that I did, however, it is very outdated and I am a much better programmer now then I was when I made it!

Here is a link to Tavernkeep's itch.io listing:
https://lilmisshine.itch.io/tavernkeep

The folder titled "Screenshots and Videos" has a set of screenshots and gameplay videos.