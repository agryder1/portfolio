using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: January 29th, 2022

//This code spills shit all over my nice floor
public class SpillSpawner : MonoBehaviour
{

    public GameObject[] spillTypes; //The shit we can spill
    private GameObject spill; //A spill

    public Vector3[] spawnPoints; //Where we can spill it
    public List<int> filledSpots; //And where we already have

    private int spotSelect; //Pick a spot

    public Table[] tables; //All the table

    private bool hasGuests; //Is there people here?

    private int spawnCap; //How many spills we've made

    private int maxSpawn = 6; //And how many we can make

    public GameManager gm; //The Game Manager

    // Update is called once per frame
    void Update()
    {
        //Update how many spills we have atm
        spawnCap = filledSpots.Count;
    }

    public void Spawn()
    {
        var chance = Random.Range(1, 3);
        if (chance == 2)
        {
            StartCoroutine("SpawnGuest");
        }
    }

    //This coroutine has been taken from GuestSpawner and modified accordingly

    //Spawn a bitch (the coroutine)
    IEnumerator SpawnGuest()
    {
        //Let the code know we're spawnin' a bitch and wait a sec or two for the DramaTM
        yield return new WaitForSeconds(Random.Range(8f, 11f));

        //While loop because we don't know how many times we're gonna have to run this to find a spawnpoint
        while (spawnCap < maxSpawn)
        {
            //Pick a random spawnpoint!
            spotSelect = Random.Range(0, spawnPoints.Length);

            //If there is somebody there
            if (filledSpots.Contains(spotSelect))
            {
                continue; //Pick a different spot (start the loop again)
            }
            else //If there ISN'T somebody there
            {

                spill = spillTypes[Random.Range(0, spillTypes.Length)];
                //Set the new guest's spawn to the point we picked!
                spill.transform.position = spawnPoints[spotSelect];
                spill.gameObject.GetComponent<AleSpill>().id = spotSelect;

                //Set the spot as full
                filledSpots.Add(spotSelect);

                //And slap that bitch right into there
                Instantiate(spill);

                FindObjectOfType<Tutorial>().NewTutorial(14);

                //break outta the loop
                break;
            }

        }
    }
}
