﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: October 13th, 2021
//This code manages all the kegs and anything that dispenses an item

public class DispenseItem : MonoBehaviour
{

    public PlayerController player; //The player
    public bool inCollider; //Bool for if the player is nearby
    public Animator anim; //The animator
    public string item; //Whatever is being dispensed

    public GameManager gm; //The Game Manager

    public Animator xAnim; //The X bubble's animator
    private int kegStat; //The number of drinks left in the keg
    private int kegMax = 5; //The max number of drinks in a keg
    private bool empty; //Pretty self-explanatory

    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>(); //Find the player
        anim = GetComponent<Animator>(); //Set up the animator
        inCollider = false; //The player is not nearby at the start
        xAnim.Play("XInvisible"); //Hide the X Bubble
        kegStat = kegMax; //Fill up the keg
        empty = false; //It's not empty!
    }

    void Update()
    {
        //If the keg is empty
        if (kegStat <= 0)
        {
            //Make sure it shows up that way!
            empty = true;
            xAnim.Play("XBubble2");
            Tutorial tut = FindObjectOfType<Tutorial>();
            tut.NewTutorial(7);
        }
        else if (xAnim.GetCurrentAnimatorStateInfo(0).IsName("XInvisible"))
        {
            //But if you can't see the X bubble then it isn't empty
            empty = false;
        }
    }

    public void OnClick()
    {
        if (inCollider && gm.open == false)
        {
            //If there's booze in the keg and the player wants it
            if (empty == false)
            {
                //Play the animation and hand it over
                anim.Play("BeerKeg");
                player.holding = item;

                //Take one "out" of the keg
                kegStat -= 1;

                //Play the tutorial for dropping an item
                FindObjectOfType<Tutorial>().NewTutorial(16);

            }
            else if (empty && gm.money >= 35) //But if it's empty and the player has enough money to fill it
            {
                gm.money -= 35; //Take the money
                kegStat = kegMax; //Fill the pot
                xAnim.Play("BubbleKegBeer"); //And play a cute animation
            }
            else if (empty && gm.money < 35)
            {
                //Play the tutorial for not being able to afford things
                FindObjectOfType<Tutorial>().NewTutorial(15);
            }
        }
    }

    //If the player is in the collider (nearby)
    private void OnTriggerStay2D(Collider2D other)
    {

        if (other.gameObject.tag == "Player")
        {
            //Let the code know they're near
            inCollider = true;
        }

    }

    //If the player leaves the collider
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //Let the code know they've moved away
            inCollider = false;
        }
    }

}
