﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: October 12th, 2021
//This is the code for managing the player

public class PlayerController : MonoBehaviour
{
    //The player's speed and animator
    public float moveSpeed = 5;
    public Animator anim;

    //What the player is currently holding
    public string holding;

    //The direction the player is looking
    Vector2 lookDirection = new Vector2(1, 0);

    public bool canMove;

    public GameManager gm;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>(); //Set up the animator
        gm = FindObjectOfType<GameManager>();
        holding = "nothing";
        canMove = true;
    }

    void FixedUpdate()
    {
        if (canMove == true)
        {
            //This particular bit of code is taken from Cavo's Adventure

            //Start

            //~ Talk about moving Cavo ~

            //Establish the Horizontal and Vertical input buttons and use them to tell the
            //game where the player wants to go

            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            Vector2 move = new Vector2(horizontal, vertical);

            //This crap tidies up Cavo's movement and figures out which direction she's facing
            //Honestly, I'm not entirely sure how it works...

            if (!Mathf.Approximately(move.x, 0.0f) || !Mathf.Approximately(move.y, 0.0f))
            {
                lookDirection.Set(move.x, move.y);
                lookDirection.Normalize();
            }

            //Send that information to animator so that Cavo plays the right animation
            //based on what direction she is facing and if she is moving

            anim.SetFloat("LookX", lookDirection.x);
            anim.SetFloat("LookY", lookDirection.y);
            anim.SetFloat("Speed", move.magnitude); // No speed = Idle (no shit, right?)

            //End

            //Moving the player along the Horizontal axis
            if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f)
            {
                transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime, 0f, 0f));

            }

            //Moving the player along the Vertical axis
            if (Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f)
            {
                transform.Translate(new Vector3(0f, Input.GetAxisRaw("Vertical") * moveSpeed * Time.deltaTime, 0f));

            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //If we're brawling then we shouldn't be able to hold anything
        if (gm.brawling)
        {
            holding = "nothing";
        }

        //Switch case for what the player is holding
        //(Changes the player's animation)
        switch (holding)
        {
            //Beer
            case "beer":
                anim.SetBool("holdingThing", true);
                anim.SetBool("Beer", true);
                break;

            //Ale
            case "ale":
                anim.SetBool("holdingThing", true);
                anim.SetBool("Ale", true);
                break;
            
            //Soup
            case "soup":
                anim.SetBool("holdingThing", true);
                anim.SetBool("Bowl", true);
                break;

            case "nothing":
                DropEverything();
                break;
        }

        //If the player wants to drop something
        if (Input.GetKeyDown(KeyCode.X))
        {
            DropEverything();
        }

    }

    //Method for dropping whatever the player is holding
    public void DropEverything()
    {
        //Set holding to nothing
        holding = "nothing";

        //Set the holding animations to false
        anim.SetBool("holdingThing", false);
        anim.SetBool("Beer", false);
        anim.SetBool("Ale", false);
        anim.SetBool("Bowl", false);
    }

    //Function for spilling something when we trip
    public IEnumerator Spill()
    {
        //We shouldn't be moving
        canMove = false;

        //Find what we're holding then animate tripping
        switch (holding)
        {
            //Beer
            case "beer":
                anim.Play("SuzannaTrip(Beer)");
                break;

            //Ale
            case "ale":
                anim.Play("SuzannaTrip(Ale)");
                break;

            //Soup
            case "soup":
                anim.Play("SuzannaTrip(Soup)");
                break;

            case "nothing":
                anim.Play("SuzannaTrip");
                break;

        }
        holding = "nothing"; //And drop it

        //Let the animation play
        yield return new WaitForSeconds(1.3f);

        //Then let the player move again
        canMove = true;
    }

    //Coroutine for when we're stunned
    public IEnumerator Stun()
    {
        //Play the animation and hold still for 2secs
        canMove = false;
        anim.SetBool("Stunned", true);

        yield return new WaitForSeconds(2);

        //Then move on
        anim.SetBool("Stunned", false);
        canMove = true;
    }

    //Detect when we step near a brawler or a spill
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision != null)
        {
            if (collision.gameObject.tag == "Brawler")
            {
                collision.gameObject.GetComponent<Brawler>().inCollider = true;
            }
            else if (collision.gameObject.tag == "Spill")
            {
                collision.gameObject.GetComponent<AleSpill>().colliding = true;
            }
        }
    }

    //And when we step away
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision != null)
        {
            if (collision.gameObject.tag == "Brawler")
            {
                collision.gameObject.GetComponent<Brawler>().inCollider = false;
            }
            else if (collision.gameObject.tag == "Spill")
            {
                collision.gameObject.GetComponent<AleSpill>().colliding = false;
            }
        }
    }

}
