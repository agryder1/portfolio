using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: September 30th, 2021
//This code manages the time cycle that is shown on the windows

public class Window : MonoBehaviour
{
    public Animator anim; //We need the animator
    private SpriteRenderer render; //And the gameobject's sprite
    public int current;
    public Sprite[] time;

    public bool bought;
    public bool showing;

    private void Awake() //ASAP
    {
        //Set up the Animator so that the BG Music can find it
        anim = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Find our sprite
        render = GetComponent<SpriteRenderer>();

        //And make sure we aren't playing anything yet
        anim.enabled = false;
        bought = false;
        showing = false;
    }

    public void CheckTime(int current)
    {
        if (bought && showing)
        {
            render.sprite = time[current];
            anim.SetInteger("Time", current);
        }
    }
}
