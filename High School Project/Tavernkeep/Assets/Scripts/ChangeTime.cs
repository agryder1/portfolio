using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: October 13th, 2021

public class ChangeTime : MonoBehaviour
{
    //We're gonna need the windows to display the time change
    public Window[] windows;
    public int time; //And a variable to tell other scripts what time it is

    private int dayCount = 0;
    public Image digit1;
    public Image digit2;
    public Sprite[] numbers;

    // Start is called before the first frame update
    void Start()
    {
        //As soon as the game starts so does the daylight cycle
        StartCoroutine("Cycle");
    }

    //Coroutine to cycle through times
    IEnumerator Cycle()
    {
        //Daytime
        foreach (Window window in windows) //Find every window
        {
            //And let it know it needs to change the time
            window.CheckTime(0);
            time = 0;
        }
        yield return new WaitForSeconds(15); //Wait 20secs so that the day cycle is one minute long

        //Evening
        foreach (Window window in windows)
        {
            window.CheckTime(1);
            time = 1;
        }
        yield return new WaitForSeconds(15);

        //Nightime
        foreach (Window window in windows)
        {
            window.CheckTime(2);
            time = 2;
        }
        yield return new WaitForSeconds(15);

        //Morning
        foreach (Window window in windows) //Find every window
        {
            //And let it know it needs to change the time
            window.CheckTime(3);
            time = 3;
        }
        yield return new WaitForSeconds(15);

        dayCount++;
        UpdateCount();

        //Then when the cycle completes start it again
        StartCoroutine("Cycle");
    }

    public void UpdateCount()
    {
        if (FindObjectOfType<GameManager>().brawling == false)
        {
            digit1.sprite = numbers[dayCount / 10];
            digit2.sprite = numbers[dayCount % 10];
        }
        else
        {
            dayCount = 0;
            digit1.sprite = numbers[0];
            digit2.sprite = numbers[0];
        }
    }
}
