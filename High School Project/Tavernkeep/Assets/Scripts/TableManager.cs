﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: January 30th 2021
//This code manages all of the tables and their behaviour

public class TableManager : MonoBehaviour
{

    public Table[] tables; //All of the tables
    public NPC groupSelect = null; //The NPC/Group of NPCs that the player has selected
    public Table tableSelect = null; //The table the player has selected

    public GameManager gm;

    //Variable for choosing which item the table wants
    private int selectItem;

    // Update is called once per frame
    void Update()
    {

        /* SO, this is here because this code only needs to run once and it only needs
         * to run the very first time the player clicks on the table. BUT detecting which
         * table is selected and then running all of this code for it would be a lot more
         * complicated than just detecting the click that selects the table in the first place
         * so this is called "technically" whenever the player clicks, but in reality it will
         * only be called when the player is selecting a table.*/
        if (Input.GetMouseButtonDown(0))
        {
            //If the player has a table and a group selected
            if (tableSelect != null && groupSelect != null)
            {
                tableSelect.hasGuests = true; //The table has guests now
                gm.hasGuests = true;
                tableSelect.SeatGuests(groupSelect); //Tell the table who's sitting at it

                groupSelect.die = true;

                //Set the selected table and group to null so the player can repeat the action
                tableSelect = null;
                groupSelect = null;
            }
        }
        
    }

}
