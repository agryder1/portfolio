﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

//Aurora Ryder LU: September 15th, 2021
/*Head bitch in charge of the game stats. Manages the player's money and hearts, the rage
  level of the tavern and updates the UI. Also used to pause the game*/

public class GameManager : MonoBehaviour
{
    public Text moneyText; //The Money UI
    public int money; //The player's money
    private int maxMoney;

    public int rage; //The tavern's rage level
    private int maxRage; //The max rage level (what causes a brawl)
    private bool rage1;

    public bool paused; //Bool to recognize when the game is paused

    public GameObject pauseMenu; //The pause menu's UI GameObject

    public GameObject ledgerMenu; //The ledger's GameObject
    public bool ledger;

    public Text heartText; //Heart UI
    public int hearts; //The player's hearts
    private int maxHearts;
    private bool heart1;

    public RageMeterUI rageMeter; //The rage UI

    public bool hasGuests; //Whether or not the tavern has guests

    public GameObject shopUI; //The store's GameObject
    private bool toggle; //Bool to toggle the store being open/closed
    public BuyHiredHelp[] fakeUI; //The fake button for the Hired Help
    public bool open; //Bool to check if the store is open

    //The arrays for the wall, floor, and bar stores
    public Item[] wallItemList;
    public Item[] floorItemList;
    public Item[] barItemList;

    public bool brawling; //Bool to check if we're brawlin'
    private Brawl brawl; //The brawl code

    public Table[] tables; //And all of the tables!

    // Start is called before the first frame update
    void Start()
    {
        //Set money to fifty because we're nice
        money = 50;
        maxMoney = 999; //The max amt of money you can get
        hearts = 0; //No hearts tho
        maxHearts = 99;
        rage = 0; //No one's angry yet
        maxRage = 100; //The maximum rage level you can have

        hasGuests = false; //We don't have guests yet
        rageMeter.slider.maxValue = maxRage; //Make sure the slider knows how far it can go
        paused = false; //No it's not paused!
        heart1 = false;

        //We shouldn't be able to see any UI and the store should be closed
        pauseMenu.SetActive(false);
        ledgerMenu.SetActive(false);
        ledger = false;
        toggle = false;
        shopUI.SetActive(false);

        //Find the brawl code
        brawl = GetComponent<Brawl>();
    }

    // Update is called once per frame
    void Update()
    {
        //If the player wants to pause the game via escape button
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //Open the pause menu
            PauseButton();
        }

        //If this is the first heart we've received
        if (hearts == 1 && heart1 == false)
        {
            //Play the tutorial explaining hearts
            FindObjectOfType<Tutorial>().NewTutorial(12);
            heart1 = true;
        }

        //If this is the first real rage we've had
        if (rage >= 10 && rage1 == false)
        {
            //Play the tutorial explaining rage
            FindObjectOfType<Tutorial>().NewTutorial(3);
            rage1 = true;
        }

        //If the rage level is high enough to start a brawl
        if (rage >= (maxRage + 1) && brawling == false && hasGuests == true)
        {
            //Do it
            brawling = true;
            hasGuests = false;
        
            brawl.StartBrawl();

            FindObjectOfType<ChangeTime>().UpdateCount();
        }
        else if (rage < (maxRage + 1)) //If it's not then
        {
            //Let everyone know we're cool
            brawling = false;
        }

        if (rage <= 0) //Make sure that rage is never negative
        {
            rage = 1; //(Set to 1 instead of 0 for better UI appearance)
        }


        if (money < 0) //Same goes for money
        {
            money = 0;
        }
        else if (money > maxMoney)
        {
            money = maxMoney;
        }


        if (hearts < 0) //Same goes for hearts
        {
            hearts = 0;
        }
        else if (hearts > maxHearts)
        {
            hearts = maxHearts;
        }

        //Update the UI to match the player's money/hearts/rage
        moneyText.text = money.ToString();
        heartText.text = hearts.ToString();

        rageMeter.SetRage(rage);

    }
    
    //If you press the pause button
    public void PauseButton()
    {
        //Show the UI and pause
        pauseMenu.SetActive(true);
        Pause();
    }

    //If you press it again
    public void UnPauseButton()
    {
        //Unpause the game and hide the UI
        pauseMenu.SetActive(false);
        UnPause();
    }

    //Pause the game by setting the time to zero
    public void Pause()
    {
        //It's paused!
        Time.timeScale = 0;
        paused = true;
    }

    //Unpause the game by setting the time to rights
    public void UnPause()
    {
        //Unpause!
        Time.timeScale = 1;
        paused = false;
    }

    //Open the ledger
    public void Ledger()
    {
        ledger = true;
        ledgerMenu.SetActive(true);
        ledgerMenu.GetComponent<LedgerUI>().MainMenu();
        Pause(); //And pause the game
        FindObjectOfType<Tutorial>().NewTutorial(8);
    }

    //Close the ledger
    public void NoLedger()
    {
        ledger = false;
        ledgerMenu.SetActive(false);
        UnPause(); //And set the time to rights
    }

    //Open/Close the shop
    public void ToggleShop()
    {
        //If we're closed
        if (toggle == false)
        {
            //inverse toggle
            toggle = !toggle;
            //And open up
            shopUI.SetActive(true);
            open = true;

            //Then find every store
            var items = GameObject.FindGameObjectsWithTag("Store");
            foreach (var item in items)
            {
                //And open it
                item.GetComponent<Store>().Open();
            }

            //Then find all the fake buttons
            for (int i = 0; i < fakeUI.Length; i++)
            {
                //And open those, too
                fakeUI[i].Open();
            }

            //And pause the game
            Pause();
        }
        else if (toggle) //But if it is open
        {
            //Inverse toggle
            toggle = !toggle;
            //And close up
            shopUI.SetActive(false);
            open = false;

            //And tell everyone else to close, too
            var items = GameObject.FindGameObjectsWithTag("Store");
            foreach (var item in items)
            {
                item.GetComponent<Store>().Close();
            }

            for (int i = 0; i < fakeUI.Length; i++)
            {
                fakeUI[i].Close();
            }

            //Then unpause
            UnPause();
        }

    }

    //Function that checks whether or not the tavern has guests
    public bool CheckForGuests()
    {
        //Set the variable to false to start
        var guests = false;

        //Then check every table
        foreach (Table table in tables)
        {
            //And if we find a guest
            if (table.hasGuests)
            {
                //return true
                guests = true;
            }
        }
        
        //Otherwise return false
        return guests;
    }

}
