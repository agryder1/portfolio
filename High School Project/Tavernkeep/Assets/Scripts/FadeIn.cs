using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: ???
//This code was stolen from 'Run Bitch'

public class FadeIn : MonoBehaviour
{

    //Set up the canvasGroup
    public CanvasGroup canvasGroup;

    //The self is a construct
    public GameObject self;

    void Start()
    {
        //Get the Canvas Group set up and set it to invisible
        canvasGroup = GetComponent<CanvasGroup>();
    }

    //Fade In
    public void PlayFadeIn()
    {
        //Set the object to invisible then activate it and start fading in
        canvasGroup.alpha = 0;
        self.SetActive(true);
        StartCoroutine("fadeInDelay");
    }

    //The Corountine to fade in
    IEnumerator fadeInDelay()
    {
        //Repeat ~10 times
        for (float i = 0; i < 1.1; i += 0.2f)
        {
            //Actually fade in
            canvasGroup.alpha = i;
            //Short delay for the aesthic
            yield return new WaitForSeconds(0.1f);
        }

    }

    //Fade Out
    public void PlayFadeOut()
    {
        //Set self to visible and start fading out
        self.SetActive(true);
        StartCoroutine("fadeOutDelay");
    }

    //The Coroutine to fade out
    IEnumerator fadeOutDelay()
    {
        //Repeat ~10 times
        for (float i = 1; i > -1; i -= 0.8f)
        {
            //Slowly fade out
            canvasGroup.alpha = i;
            //Short delay for the aesthic
            yield return new WaitForSeconds(0.1f);
        }

        //Set active to false so that the object doesn't get in the way
        self.SetActive(false);
    }

    //Set object to visible
    public void Visible()
    {
        self.SetActive(true);
        canvasGroup.alpha = 1;
    }

    //Set object to invisible
    public void Invisible()
    {
        self.SetActive(false);
        canvasGroup.alpha = 0;
    }
}
