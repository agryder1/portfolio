using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: June 11th, 2021

//This code manages the Ledger's UI
public class LedgerUI : MonoBehaviour
{
    public GameObject mainMenu; //The main menu
    public GameObject entry; //A ledger entry
    public GameObject doodle; //The doodles

    public GameObject right; //The righthand button
    public GameObject left; //The lefthand button

    public Text menuType; //The guest type that is shown at the main menu
    //(changes depending on what the player scrolls over)

    //Entry data
    public Text type; //The guest type
    public Text groupName; //name
    public Image portrait; //group pic
    public Image likeIcon; //type that they like
    public Image hateIcon; //And the two types they don't
    public Image hateIcon2;

    public int dispId; //Int to flick through entries
    public NPC[] guestList; //The list of guests/entries

    public Sprite[] typeIcons;

    void Start()
    {
        MainMenu();
    }

    public void MainMenu()
    {
        //Hide the lefthand button and the entry menu
        left.SetActive(false);
        entry.SetActive(false);
        doodle.SetActive(false);
        //Show the main menu
        mainMenu.SetActive(true);
        //And get ready to flick through entries
        dispId = -1;
    }

    //The righthand button
    public void RightArrow()
    {
        //Show the entry and the buttons
        right.SetActive(true);
        left.SetActive(true);
        entry.SetActive(true);
        mainMenu.SetActive(false); //Hide the main menu
        doodle.SetActive(false); //Hide the doodles

        //Add one to flip to the next entry
        dispId += 1;

        //If we've reached the end
        if (dispId >= guestList.Length)
        {
            //Make sure we can't go furthur and show the doodles
            right.SetActive(false);
            entry.SetActive(false);
            doodle.SetActive(true);
        }
        else
        {
            //Set the entry data to the guest's stuff
            type.text = guestList[dispId].groupType;
            groupName.text = guestList[dispId].groupName;
            portrait.sprite = guestList[dispId].gameObject.GetComponent<SpriteRenderer>().sprite;
            PickIcons();
        }

    }

    //The lefthand button
    public void LeftArrow()
    {
        //Show the entry and the buttons
        right.SetActive(true);
        left.SetActive(true);
        entry.SetActive(true);
        mainMenu.SetActive(false); //Hide the main menu
        doodle.SetActive(false); //Hide the doodles

        //Subtract one to flip backwards
        dispId -= 1;

        //If we've gone too far
        if (dispId < 0)
        {
            //Go back to the main menu
            MainMenu();
        }
        else
        {
            //Set the entry data to the guest's stuff
            type.text = guestList[dispId].groupType;
            groupName.text = guestList[dispId].groupName;
            portrait.sprite = guestList[dispId].gameObject.GetComponent<SpriteRenderer>().sprite;
            PickIcons();
        }

    }

    //Function that picks which icons to display on the ledger
    //Based on the guests' likes and hates
    private void PickIcons()
    {
        switch (guestList[dispId].likes)
        {
            case "Royal":

                likeIcon.sprite = typeIcons[0];
                break;

            case "Bastard":

                likeIcon.sprite = typeIcons[1];
                break;

            case "Holy":

                likeIcon.sprite = typeIcons[2];
                break;

            case "Unholy":

                likeIcon.sprite = typeIcons[3];
                break;

            case "Magic":

                likeIcon.sprite = typeIcons[4];
                break;

            case "Fairy":

                likeIcon.sprite = typeIcons[5];
                break;

            case "Party":

                likeIcon.sprite = typeIcons[6];
                break;

            case "Loner":

                likeIcon.sprite = typeIcons[7];
                break;

            case "no":

                likeIcon.sprite = typeIcons[8];
                break;
        }

        switch (guestList[dispId].hate1)
        {
            case "Royal":

                hateIcon.sprite = typeIcons[0];
                break;

            case "Bastard":

                hateIcon.sprite = typeIcons[1];

                break;

            case "Holy":

                hateIcon.sprite = typeIcons[2];
                break;

            case "Unholy":

                hateIcon.sprite = typeIcons[3];
                break;

            case "Magic":

                hateIcon.sprite = typeIcons[4];
                break;

            case "Fairy":

                hateIcon.sprite = typeIcons[5];
                break;

            case "Party":

                hateIcon.sprite = typeIcons[6];
                break;

            case "Loner":

                hateIcon.sprite = typeIcons[7];
                break;

            case "no":

                hateIcon.sprite = typeIcons[8];
                break;
        }

        switch (guestList[dispId].hate2)
        {
            case "Royal":

                hateIcon2.sprite = typeIcons[0];
                break;

            case "Bastard":

                hateIcon2.sprite = typeIcons[1];

                break;

            case "Holy":

                hateIcon2.sprite = typeIcons[2];
                break;

            case "Unholy":

                hateIcon2.sprite = typeIcons[3];
                break;

            case "Magic":

                hateIcon2.sprite = typeIcons[4];
                break;

            case "Fairy":

                hateIcon2.sprite = typeIcons[5];
                break;

            case "Party":

                hateIcon2.sprite = typeIcons[6];
                break;

            case "Loner":

                hateIcon2.sprite = typeIcons[7];
                break;

            case "no":

                hateIcon2.sprite = typeIcons[8];
                break;
        }
    }
}