using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tablecloth : MonoBehaviour
{

    public SpriteRenderer clothRender;
    public SpriteRenderer edgeRender;
    public Sprite[] mains;
    public Sprite[] edges;

    // Update is called once per frame
    void Update()
    {
        if (clothRender.sprite == mains[0])
        {
            edgeRender.sprite = edges[0];
        }
        else if (clothRender.sprite == mains[1])
        {
            edgeRender.sprite = edges[1];
        }
        else if (clothRender.sprite == mains[2])
        {
            edgeRender.sprite = edges[2];
        }
        else if (clothRender.sprite == mains[3])
        {
            edgeRender.sprite = edges[3];
        }
        else
        {
            edgeRender.sprite = edges[4];
        }
    }
}
