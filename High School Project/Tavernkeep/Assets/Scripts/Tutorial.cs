using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: October 12th, 2021

//This code manages the displaying of tutorial blurbs
public class Tutorial : MonoBehaviour
{
    private GameManager gm; //We need the GM

    public TText[] blurbs; //An array of the blurbs we're gonna display
    public Text blurb; //As well as the text we'll be altering
    private CanvasGroup cg; //And the canvas group we're storing it in

    public Text words; //This is the text for the tutorial toggle switch

    //The variables we'll use to control the logic flow
    private int active;
    public bool on;
    private bool off;

    //The store elements that overlap the tutorial and need to be hidden
    public GameObject[] overlapElements;
    public GameObject underlap;

    private void Awake()
    {
        //As soon as possible find the variables GameObject and check if tutorials are on or off
        on = Variables.tutorial;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Set up our components
        gm = FindObjectOfType<GameManager>();
        cg = GetComponent<CanvasGroup>();

        //Flip the on values
        //We have to do this because Variables and Tutorial read the on variable differently
        on = !on;
        ToggleTutorial();

        //Then call the first tutorial
        NewTutorial(0);
    }

    //This is the function that chooses which tutorial to display and displays it
    public void NewTutorial(int index)
    {
        if (off == false) //If we're allowed to show tutorials
        {
            if (blurbs[index].played == false) //And the one we're trying to load hasn't been played before
            {
                //Make sure the store UI isn't in the way
                foreach(GameObject obj in overlapElements)
                {
                    obj.SetActive(false);
                }

                underlap.SetActive(true);

                //Then pause the game and display the tutorial
                Time.timeScale = 0;
                blurb.text = blurbs[index].script;
                if (cg != null)
                {
                    cg.alpha = 1f;
                }
                active = index;
            }
        }
        else
        {
            foreach (GameObject obj in overlapElements)
            {
                obj.SetActive(true);
            }
            underlap.SetActive(false);
        }
    }

    //This is the function that hides the tutorial
    public void HideTutorial()
    {
        //If the tutorial we just played was the first tutorial
        if (active == 0)
        {
            //Then load it's counterpart as the intro is two tutorials long
            NewTutorial(13);
        }
        else //Otherwise just unpause the game, let the tutorial know its been played, and then hide it
        {
            //Put the store UI back in place
            foreach (GameObject obj in overlapElements)
            {
                obj.SetActive(true);
            }

            underlap.SetActive(false);

            if (gm.paused == false)
            {
                Time.timeScale = 1;
            }
            blurbs[active].played = true;
            if (cg != null)
            {
                cg.alpha = 0f;
            }

        }
    }

    //This function toggles whether or not we have tutorials enabled
    public void ToggleTutorial()
    {
        //Flip the on variable and then toggle it to on/off accordingly
        on = !on;
        if (on)
        {
            off = false;
            words.text = "On";
        }
        else
        {
            off = true;
            words.text = "Off";
            if (cg != null) //Also hide the canvas group so that if we had one displayed it goes away
            {
                if (cg.alpha == 1f)
                {
                    cg.alpha = 0f;
                }
            }
        }
        Variables.tutorial = on;
    }
}
