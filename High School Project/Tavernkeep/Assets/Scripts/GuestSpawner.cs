﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: August 15th, 2021
//This code handles everything to do with spawning guests in the tavern

public class GuestSpawner : MonoBehaviour
{
    
    public GameObject[] guests; //Array holding the different types of guests
    private int guestSelect; //Int for picking a random guest

    public int capacity; //How many guests are in the tavern atm
    private int maxCapacity = 6; //The max number of guests allowed in the tavern

    public int spawnCap; //How many guests are waiting for a tavern
    private int maxSpawn = 4; //The max number of guests allowed to wait for a table

    private bool inProgress; //Bool to check if a guest is currently being spawned

    public Vector3[] spawnPoints; //Array of places guests can be spawned
    public List<int> filledSpots; //List of which spawnpoints are currently taken

    private int spotSelect; //Int to pick which spawnpoint we want to send the next guest to

    public GameManager gm;
    private bool first;
    private int lowerLim = 5;
    private int higherLim = 7;

    // Start is called before the first frame update
    void Start()
    {
        capacity = 0; //No one is here yet!
        inProgress = false;
        first = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Keep the number of people waiting accurate
        spawnCap = filledSpots.Count;

        if (gm.brawling)
        {
            inProgress = false;
            StopCoroutine("SpawnGuest");
        }

        //If the tavern isn't full and there is room for them to wait and we're not in the process of spawing someone
        if ((capacity < maxCapacity) && (!inProgress) && (spawnCap < maxSpawn) && (gm.brawling == false))
        {
            //Spawn a bitch
            StartCoroutine("SpawnGuest");
        }
    }

    //Spawn a bitch (the coroutine)
    IEnumerator SpawnGuest()
    {
        //Let the code know we're spawnin' a bitch and wait a sec or two for the DramaTM
        inProgress = true;
        if (first)
        {
            yield return new WaitForSeconds(3);
            first = false;
            StartCoroutine("Delay");
        }
        else
        {
            yield return new WaitForSeconds(Random.Range(lowerLim, higherLim));
        }

        //Pick a random guest!
        guestSelect = Random.Range(0, guests.Length);
        GameObject newGuest = guests[guestSelect];

        //While loop because we don't know how many times we're gonna have to run this to find a spawnpoint
        while (spawnCap < maxSpawn)
        {
            //Pick a random spawnpoint!
            spotSelect = Random.Range(0, spawnPoints.Length);

            //If there is somebody there
            if (filledSpots.Contains(spotSelect))
            {
                continue; //Pick a different spot (start the loop again)
            }
            else //If there ISN'T somebody there
            {
                //Set the new guest's spawn to the point we picked!
                newGuest.transform.position = spawnPoints[spotSelect];
                newGuest.gameObject.GetComponent<NPC>().spawn = spotSelect;

                //Set the spot as full
                filledSpots.Add(spotSelect);

                //And slap that bitch right into there
                Instantiate(newGuest);

                //Up the tavern's capacity and break outta the loop
                capacity += 1;
                inProgress = false;

                break;
            }

        }

    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(120);
        lowerLim = 4;

        yield return new WaitForSeconds(180);
        lowerLim = 3;
    }

}
