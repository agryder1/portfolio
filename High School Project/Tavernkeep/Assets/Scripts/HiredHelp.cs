using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: ???
//This baby manages everything to do with the hired help!

public class HiredHelp : MonoBehaviour
{

    public Animator anim; //The animator
    public GameManager gm; //Game Manager
    public BoxCollider2D collid; //Collider

    // Start is called before the first frame update
    void Start()
    {
        //Set up the collider and then turn it off because we only need it when
        //you can see the hired help
        collid = GetComponent<BoxCollider2D>();
        collid.enabled = false;
    }

    void Update()
    {
        if (gm.brawling)
        {
            anim.Play("EskoInvis");
        }

        //If you can't see them
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("EskoInvis"))
        {
            collid.enabled = false; //You shouldn't be colliding with them
        }
    }

    //These are all functions so that they can be triggered by the UI Buttons
    public void Esko()
    {
        PlayAnimation("EskoScandal"); //Passes a clip to the function that'll "hire" them
    }

    public void Clovis()
    {
        PlayAnimation("Clovis");
    }

    public void Barley()
    {
        PlayAnimation("Barley");
    }

    //"Hire the help"
    public void PlayAnimation(string clip) //needs an animation clip
    {
        if (gm.brawling == false)
        {
            //If the player can afford to hire them
            if (gm.money >= 25 && (gm.open == false))
            {
                //do so
                gm.money -= 25;
                collid.enabled = true;

                anim.Play(clip);
                gm.rage -= 30;

                //If the player hires Esko then they might get extra hearts
                if (clip == "EskoScandal")
                {
                    var chance = Random.Range(1, 3);
                    if (chance == 2)
                    {
                        gm.hearts += Random.Range(1, 3);
                    }
                }
                else if (clip == "Clovis") // If Clovis is hired they might get extra money
                {
                    var chance = Random.Range(1, 3);
                    if (chance == 2)
                    {
                        gm.money += Random.Range(10, 30);
                    }
                }
                else if (clip == "Barley")
                {
                    var chance = Random.Range(1, 3);
                    if (chance == 2)
                    {
                        foreach (AleSpill spill in FindObjectsOfType<AleSpill>())
                        {
                            spill.CleanUp(true);
                        }
                    }
                }

                //And trigger the hired help effect on all the tables that have people at them
                foreach (Table table in gm.tables)
                {
                    if (table.hasGuests)
                    {
                        table.HelpEffect();
                    }
                }
            }
        }
    }
}
