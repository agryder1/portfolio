using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Aurora Ryder LU: September 30th, 2021
//This code manages the Background Music of the main game

public class BGMusic : MonoBehaviour
{

    private AudioSource source; //The audio source
    public AudioClip[] music; //An array of tracks we can play
    public FXToggle toggle; //The music toggle switch
    private AudioManager am; //The AudioManager
    private GameManager gm; //The GameManager
    private ChangeTime ct;

    //Before you do anything else
    private void Awake()
    {
        //Find our components
        source = gameObject.GetComponent<AudioSource>();
        toggle = FindObjectOfType<FXToggle>();
        am = FindObjectOfType<AudioManager>();
        gm = FindObjectOfType<GameManager>();
        ct = FindObjectOfType<ChangeTime>();
    }

    //Start
    void Start()
    {
        am.Play("Fire"); //Start the fireplace bg sounds
        source.clip = music[Random.Range(0, 2)]; //Pick which track to play
        source.Play(); //Play it
    }

    // Update is called once per frame
    void Update()
    {

        //If the track has stopped
        if (source.isPlaying == false)
        {
            //And we're not brawling
            if (gm.brawling == false)
            {
                var chance = Random.Range(0, 2);

                source.clip = music[chance];
                source.Play();

            }
            else //If we are brawling
            {
                //Then play that funky music white boy
                BrawlMusic();
            }
        }
    }

    //This function plays the music and FX that go with brawls
    public void BrawlMusic()
    {
        //Play the music
        source.clip = music[2];
        source.Play();

        //And make people yell
        am.Play("Yellsm");
        am.Play("Yellsf");
    }

    //This function stops the brawl music and FX
    public IEnumerator NoBrawlMusic()
    {
        //Stop yelling
        am.Stop("Yellsm");
        am.Stop("Yellsf");

        //And be shushy
        source.mute = true;

        //Then after a short rest
        yield return new WaitForSeconds(4);

        //Pick which music to play
        source.clip = music[Random.Range(0, 2)];
        source.Play();

        //And double check that we can unmute
        if (toggle.on)
        {
            source.mute = false;
        }
    }

}
