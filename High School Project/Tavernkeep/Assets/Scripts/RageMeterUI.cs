using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: April 16th, 2021

//Credit to Brackley (the amazing and retired youtuber) for this code

public class RageMeterUI : MonoBehaviour
{
    public Slider slider; //Set up the slider

    void Start()
    {
        slider.value = slider.minValue; //Make sure it's at zero
    }

    public void SetRage(int val)
    {
        slider.value = val; //And update the slider
    }

}
