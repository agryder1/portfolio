using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: January 6th, 2022

//This code manages all of the stores
public class Store : MonoBehaviour
{

    private GameManager gm; //The Game Manager
    private AudioManager am;
    public Window window; //The window scripts
    public Animator anim; //The Animator for the hearth

    private SpriteRenderer render; //The sprite we'll be changing
    public Item[] itemList; //The items we can pick from
    public int dispId; //The item we're on
    public bool hearth;

    public Sprite invis;
    private Sprite lastItem; //The item we just had on display (that we own)

    public Text priceTag; //The price tag
    public Text heartTag;

    void Start()
    {
        gm = FindObjectOfType<GameManager>(); //Find the gm
        am = FindObjectOfType<AudioManager>();
        render = GetComponent<SpriteRenderer>(); //Find our sprite renderer
        priceTag.gameObject.SetActive(false); //Hide the price tag(s)
        heartTag.gameObject.SetActive(false);
        dispId = 0; //And start at zero

        //Then set the last item to be the default item
        lastItem = render.sprite;
    }

    //The righthand button
    public void RightArrow()
    {
        //Add one to flick to the next item
        dispId += 1;

        //But if we've reached the end
        if (dispId >= itemList.Length)
        {
            //Then start from the beginning
            dispId = 0;
            priceTag.gameObject.SetActive(false);
            heartTag.gameObject.SetActive(false);
        }

        //If we own the item that we're showing
        if (itemList[dispId].bought)
        {
            //Make sure it looks like we own it
            render.sprite = itemList[dispId].forDisp;
            priceTag.gameObject.SetActive(false);
            heartTag.gameObject.SetActive(false);
        }
        else if (itemList[dispId].bought == false) //But if we don't
        {
            //Then show the buying info
            priceTag.gameObject.SetActive(true);
            render.sprite = itemList[dispId].forSale;
            priceTag.text = itemList[dispId].price.ToString();
            if (itemList[dispId].heartP == 0)
            {
                heartTag.gameObject.SetActive(false);
            }
            else
            {
                heartTag.gameObject.SetActive(true);
                heartTag.text = itemList[dispId].heartP.ToString();
            }
        }
    }

    //The lefthand button
    public void LeftArrow()
    {
        //Subtract one to go backwards
        dispId -= 1;

        //If we've gone too far
        if (dispId < 0)
        {
            //Then start at the top
            dispId = itemList.Length - 1;
            priceTag.gameObject.SetActive(false);
            heartTag.gameObject.SetActive(false);
        }

        //Same as right hand
        if (itemList[dispId].bought)
        {
            render.sprite = itemList[dispId].forDisp;
            priceTag.gameObject.SetActive(false);
            heartTag.gameObject.SetActive(false);
        }
        else if (itemList[dispId].bought == false) //Same as righthand
        {
            priceTag.gameObject.SetActive(true);
            render.sprite = itemList[dispId].forSale;
            priceTag.text = itemList[dispId].price.ToString();
            if (itemList[dispId].heartP == 0)
            {
                heartTag.gameObject.SetActive(false);
            }
            else
            {
                heartTag.gameObject.SetActive(true);
                heartTag.text = itemList[dispId].heartP.ToString();
            }
        }
    }

    //When the store opens
    public void Open()
    {
        priceTag.gameObject.SetActive(false);
        heartTag.gameObject.SetActive(false);
        lastItem = render.sprite; //Set the last item to be whatever it was when we opened the store
        FindObjectOfType<Tutorial>().NewTutorial(9);
    }

    //When the store closes
    public void Close()
    {
        //If we don't own the item we're on
        if (itemList[dispId].bought == false)
        {
            //Then show what we had there originally
            render.sprite = lastItem;
        }
        else
        {
            if (window != null) //If this store displays windows
            {
                if (dispId == 4) //And we're displaying a window when we close
                {
                    //Then let the window know and make sure we're showing the right time
                    window.showing = true;
                    window.CheckTime(FindObjectOfType<ChangeTime>().time);
                }
                else
                {
                    //Otherwise just make sure the window doesn't pop up out of nowhere
                    window.showing = false;
                }
            }
            if (hearth)
            {
                switch (dispId)
                {
                    case 0:
                        anim.SetFloat("Colour", 0);
                        break;

                    case 1:
                        anim.SetFloat("Colour", 0.33f);
                        break;

                    case 2:
                        anim.SetFloat("Colour", 0.66f);
                        break;

                    case 3:
                        anim.SetFloat("Colour", 1);
                        break;
                }

                render.sprite = invis;
            }
        }
    }

    //If the player buys something
    public void Buy()
    {
        //Check if they own it and can afford it
        if ((itemList[dispId].bought == false) && (itemList[dispId].price <= gm.money) && (itemList[dispId].heartP <= gm.hearts))
        {
            //Then take their money and set up the item
            am.Play("Buy");
            gm.money -= itemList[dispId].price;
            gm.hearts -= itemList[dispId].heartP;
            itemList[dispId].bought = true;
            priceTag.gameObject.SetActive(false);
            heartTag.gameObject.SetActive(false);
            render.sprite = itemList[dispId].forDisp;

            //If we just bought a window
            if (dispId == 4)
            {
                GetComponent<Window>().bought = true;
            }
        }
        else
        {
            am.Play("NoBuy");
        }
    }

}
