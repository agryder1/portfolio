using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AleSpillCollider : MonoBehaviour
{

    private AleSpill dad;
    private bool inCollider;

    private void Start()
    {
        dad = transform.GetComponentInParent<AleSpill>();
    }

    private void OnMouseDown()
    {
        if (inCollider)
        {
            dad.CleanUp(false);
        }
    }


    //When the player is standing in the collider (is nearby)
    private void OnTriggerStay2D(Collider2D other)
    {

        if (other != null)
        {
            //Let the code know the player is nearby
            inCollider = true;
        }

    }

    //When the player leaves the collider
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision != null)
        {
            //Let the code know the player has moved away
            inCollider = false;
        }
    }
}
