using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: February 24th, 2022
//This short 'n sweet script is for swapping enabling/disabling music, sound, and tutorials in-game
//by toggling their respective static variables.

public class Toggle : MonoBehaviour
{
    public void Music() // Controls sounds (don't ask me why)
    {
        Variables.music = !Variables.music;
    }

    public void Sound() // Controls Music (seriously I don't know why)
    {
        Variables.sound = !Variables.sound;
    }

    public void Tutorials() // Controls Tutorials (a method that does what it says?? In this economy??)
    {
        Variables.tutorial = !Variables.tutorial;
    }
}
