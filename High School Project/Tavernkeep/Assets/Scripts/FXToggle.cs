using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: August 15th, 2021
//This code toggles the music and sound effects

public class FXToggle : MonoBehaviour
{
    public bool on; //Bool for turning the tunes on and off

    public AudioSource source; //The AudioSource
    public AudioManager audioM; //The AudioManager

    public Text words; //The button's text

    //As soon as possible
    private void Awake()
    {
        //Figure out if we should be turned on or off based on the main menu's settings
        if (source != null) //If we're playing music
        {
            on = Variables.sound;
        }
        else //If we're playing sounds
        {
            on = Variables.music;
        }
    }

    //Start
    void Start()
    {
        //Reset things
        on = !on;
        audioM = FindObjectOfType<AudioManager>();
        Toggle();
    }

    //This function toggles the music or FX
    public void Toggle()
    {
        //Flip the variable
        on = !on;

        //If we're supposed to be on
        if (on)
        {
            //If we play music
            if (source != null)
            {
                //Turn up dem tunes
                source.mute = false;
                words.text = "On";

                Variables.music = on;
            }
            else //If we play sounds
            {
                //Find all of the sounds
                foreach (Sound s in audioM.sounds)
                {
                    //And play dem
                    s.source.mute = false;
                    words.text = "On";
                }

                Variables.sound = on;
            }
        }
        else //If we're supposed to be off
        {
            if (source != null) //And we play tunes
            {
                //Turn dat beat all da way down
                source.mute = true;
                words.text = "Off";

                Variables.sound = on;
            }
            else //And we play sounds
            {
                //Then tell all dem bitches
                foreach (Sound s in audioM.sounds)
                {
                    //To be shushy
                    s.source.mute = true;
                    words.text = "Off";
                }

                Variables.sound = on;
            }
        }
    }
}
