using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: August 13th, 2021
//This code is for creating a kill switch that ends a fight between two brawlers

public class KillSwitch : MonoBehaviour
{
    //The two brawlers (these are sent to us - they'll be null until they're given to us)
    public GameObject fight1;
    public GameObject fight2;


    // Start is called before the first frame update
    void Start()
    {
        //Set the kill switch's position to inbetween the two brawlers
        transform.position = new Vector3((fight1.transform.position.x + fight2.transform.position.x) / 2, fight1.transform.position.y, transform.position.z);
    }

    //This function triggers the switch and kills the brawlers
    public void End()
    {
        //Find both brawlers
        Brawler fightA = fight1.GetComponent<Brawler>();
        Brawler fightB = fight2.GetComponent<Brawler>();

        //And if we're near either of them
        if (fightA.inCollider || fightB.inCollider)
        {
            //End the fight
            fightA.EndFight();
            fightB.EndFight();

            //And destroy the switch
            Destroy(gameObject);
        }
    }
}
