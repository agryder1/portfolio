using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: ???
//This bb code creats the tutorial text class that lets us make tutorials

[System.Serializable]
public class TText
{
    //Bool for if it has been played, the text box, and the tutorial script
    public bool played;
    [TextArea(5,10)]
    public string script;

}
