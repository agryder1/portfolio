using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Aurora Ryder LU: ???

//NAMING ERROR: THIS CODE ORIGINALLY MOVED THE CAMERA BUT NOW IT ONLY STARTS/ENDS THE GAME
//AS SUCH IT IS CALLED MOVECAM BUT IT REALLY SHOULDN'T BE CAUSE THAT'S NOT WHAT IT DOES

public class MoveCam : MonoBehaviour
{
    //All of the parts we need to start the game or end it
    public FadeIn fade2;
    public AudioSource source;
    public string loadName;
    private bool stopped;

    //On start
    private void Start()
    {
        //hide the fade effect
        fade2.Invisible();
    }

    //Funtion for when the player presses play
    public void Play()
    {
        //If tutorials are turned on
        if (FindObjectOfType<Tutorial>() != null && FindObjectOfType<Tutorial>().on)
        {
            //Then turn them off real quick so they do not trigger and mess with the timing of the fade
            FindObjectOfType<Tutorial>().on = false;
            stopped = true;
        }
        else //Otherwise contine
        {
            stopped = false;
        }

        //Make sure we aren't paused
        Time.timeScale = 1;

        //Then start fading out the music and fading in the black screen
        StartCoroutine(FadeOut(source, 3, stopped, loadName));
        fade2.PlayFadeIn();
    }

    /*Coroutine that fades the music out
    This coroutine was written by myownbot as a modification to a script posted by Boris1998
    I found it on the Unity Forum and have modified it a bit to suit my needs
    https://forum.unity.com/threads/fade-out-audio-source.335031/ */

    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime, bool stop, string leName)
    {
        //If we're calling this from in-game then find the AudioManager and shush it
        if (FindObjectOfType<AudioManager>() != null)
        {
            FindObjectOfType<AudioManager>().gameObject.SetActive(false);
        }

        //Find the volume we're starting at
        float startVolume = audioSource.volume;

        //Then slowly quiet it
        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.unscaledDeltaTime / FadeTime;

            yield return null;
        }

        //Then stop it completely and reset the volume so we don't mess with anything
        audioSource.Stop();
        audioSource.volume = startVolume;

        //Turn the tutorials back on if we turned them off
        if (stop)
        {
            FindObjectOfType<Tutorial>().on = true;
        }

        //Make sure we're not paused and load the menu or the game
        Time.timeScale = 1;
        SceneManager.LoadScene(leName);
    }
}
