using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: October 13th, 2021
//This code manages all the kegs and anything that dispenses an item

public class DispenseItemVariant : MonoBehaviour
{

    private PlayerController player; //The player
    private GameManager gm;
    public bool inCollider; //Bool for if the player is nearby
    public Animator anim; //The animator
    public string item; //Whatever is being dispensed

    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>(); //Find the player
        gm = FindObjectOfType<GameManager>();
        anim = GetComponent<Animator>(); //Set up the animator
        inCollider = false; //The player is not nearby at the start
    }

    public void OnClick()
    {
        if (inCollider && gm.open == false)
        {
            //Play the animation and "give" the player whatever the item is
            anim.Play("BeerKeg");
            player.holding = item;

            //Play the tutorial for dropping an item
            FindObjectOfType<Tutorial>().NewTutorial(16);
        }
    }

    public void Update()
    {
        if (gm.brawling)
        {
            anim.SetBool("brawling", true);
        }
        else
        {
            anim.SetBool("brawling", false);
        }
    }

    //If the player is in the collider (nearby)
    private void OnTriggerStay2D(Collider2D other)
    {

        if (other.gameObject.tag == "Player")
        {
            //Let the code know they're near
            inCollider = true;
        }

    }

    //If the player leaves the collider
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //Let the code know they've moved away
            inCollider = false;
        }
    }

}