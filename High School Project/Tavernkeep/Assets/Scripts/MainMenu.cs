using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Aurora Ryder LU: ???
/*This is the head bitch in charge of the Main Menu.
it displays the titles, plays the intro, manages the buttons and music,
and allows the player to toggle their settings before starting the game*/

public class MainMenu : MonoBehaviour
{
    public static bool played = false; //If the intro has been played yet
    public GameObject logo; //The 'lilmisshine' title
    public Animator anim; //The animator
    public AudioSource source; //The audio source for the BG music

    public GameObject title; //The title screen
    public GameObject[] buttons; //And it's buttons
    public Vector2 target; //As well as the target location it will move to

    public FadeIn fade; //Object that manages the fade in/out effects

    private bool call = false; //Variable to let the code know it has moved the title to the target
    private bool on = false; //Variable for toggling the credits screen

    //Gameobjects that help navigate the menus
    public GameObject menu;
    public GameObject more;
    public GameObject credits;

    // Start is called before the first frame update
    void Start()
    {
        //Hide the menus we aren't using yet
        more.SetActive(false);
        credits.SetActive(false);

        //And if we haven't played the intro yet
        if (played == false)
        {
            //Play it
            logo.SetActive(true);
            fade.Visible();
            anim.Play("Logo");
            foreach (GameObject button in buttons)
            {
                button.SetActive(false);
            }
            played = true;
        }
        else //And if we have played the intro
        {
            //Then skip ahead and display everything
            Animator titan = title.GetComponent<Animator>();
            titan.Play("StopTitle");
            logo.SetActive(false);
            Music();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //If we have not moved the title screen into place yet
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Invis") && call == false)
        {
            //Then move it and make sure we don't keep moving it
            fade.PlayFadeOut();
            Music();
            StartCoroutine("Appear");
            call = true;
        }
    }

    //Coroutine that moves the title into place
    IEnumerator Appear()
    {
        //Get the animator
        Animator titan = title.GetComponent<Animator>();

        //Play the animation as we move into place
        titan.Play("Title");
        for (float i = 0; i < 1; i += 0.01f)
        {
            yield return new WaitForSeconds(0.01f);
            title.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, i);
        }

        //Wait for the AestheticTM
        yield return new WaitForSeconds(0.6f);
        titan.Play("StopTitle");

        //And slowly make the buttons appear
        for (int i = 0; i < buttons.Length; i++)
        {
            yield return new WaitForSeconds(0.3f);
            buttons[i].SetActive(true);
        }
    }

    //Function to quit the game
    public void Quit()
    {
        Application.Quit();
    }

    //Function to switch to the settings menu
    public void More()
    {
        menu.SetActive(false);
        more.SetActive(true);
    }

    //Function to switch to the main menu
    public void Menu()
    {
        more.SetActive(false);
        menu.SetActive(true);
    }

    //Fucntion to toggle the credits
    public void ToggleCredits()
    {
        //If we're showing the credits
        if (on)
        {
            //Hide them
            on = false;
            credits.SetActive(false);
        }
        else //And if we've hidden the credits
        {
            //Show them
            on = true;
            credits.SetActive(true);
        }
    }

    //Function to check if we should be playing music when the
    //player navigates back to the main menu from in-game
    public void Music()
    {
        if (Variables.sound)
        {
            source.mute = false;
        }
        else
        {
            source.mute = true;
        }

        source.Play();
    }
}
