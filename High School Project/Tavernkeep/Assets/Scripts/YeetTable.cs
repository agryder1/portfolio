using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: January 29th, 2022

//This code yeets tables
public class YeetTable : MonoBehaviour
{
    private bool colliding; //If the table is touching something
    public Rigidbody2D rigid; //The table's rigidbody
    private int moveDirect; //The direction we wanna move
    private int pushDirect; //The direction we're being shoved from
    public bool toppled; //And whether or not we've been shoved (yeeted? yote?)
    public Table table;

    public SpriteRenderer render;
    public SpriteRenderer decorRender;
    public Sprite last;
    public Sprite lastDecor;
    public Sprite broken;
    public Sprite invis;

    // Start is called before the first frame update
    void Start()
    {
        colliding = false; //We're not touching anything
        toppled = false; //And we haven't been (yoten??)
        rigid = GetComponent<Rigidbody2D>(); //Find the bitch's body
        table = GetComponent<Table>();
    }

    // Update is called once per frame
    void Update()
    {
        //if we're touching something and we haven't been yeeted
       if (colliding && (toppled == false))
        {
            //Then figure out which way to move
            switch (moveDirect)
            {
                case 0: //Moving up

                    //And self-yeet
                    transform.Translate(Vector2.up * Time.deltaTime * 2);
                    break;

                case 1: //Moving to the right

                    transform.Translate(Vector2.right * Time.deltaTime * 2);
                    break;

                case 2: //Moving down

                    transform.Translate(Vector2.down * Time.deltaTime * 2);
                    break;

                case 3: //Moving to the left

                    transform.Translate(Vector2.left * Time.deltaTime * 2);
                    break;
            }

        }

       if (toppled) //If the table has been broken
        {
            if (render.sprite != broken)
            {
                last = render.sprite;
                lastDecor = decorRender.sprite;
            }
            table.toppled = true; //Then let it know
            render.sprite = broken;
            decorRender.sprite = invis;
            if (FindObjectOfType<GameManager>().brawling == false)
            {
                FindObjectOfType<Tutorial>().NewTutorial(5);
            }
        }
    }

    //Method to reset the table's stats so that we can flip it again
    public void ResetTable()
    {
        colliding = false;
        toppled = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //If we're colliding with something
        if (collision != null)
        {
            //And that something is a brawler
            if (colliding == false && collision.gameObject.tag == "Brawler")
            {
                //Figure out which way they're pushing us
                pushDirect = collision.gameObject.GetComponent<Brawler>().walkDirection;

                //And go the opposite way
                switch (pushDirect)
                {
                    case 0:
                        moveDirect = 2;
                        break;

                    case 1:
                        moveDirect = 3;
                        break;

                    case 2:
                        moveDirect = 0;
                        break;

                    case 3:
                        moveDirect = 1;
                        break;
                }

                //And let everybody know we're movin'
                colliding = true;
            }
            else if (colliding) //And once we're movin'
            {
                //if something else hits us then stop and be yote
                colliding = false;
                toppled = true;
                transform.Translate(Vector2.zero);
            }
        }
    }

}
