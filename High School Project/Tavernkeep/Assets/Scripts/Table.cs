﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: January 29th, 2022
//This is the Table class (the thing that tells the code what tf's a table)
/*It also "seats" guests, orders their food, animates the table, handles most interactions
 with the player and clears the table when the guests leave / pays their tab. It's a big helpful boy!*/

public class Table : MonoBehaviour
{
    // ~ The technical bits ~

    //Assign a table number
    public int tableNumber;

    //The people at the table
    public string tableGuests;
    public string guestType;

    //The people the guests at the table don't like
    public string hatedType;
    public string hatedType2;

    //And who they do like
    public string likeType;

    //Are there people at the table?
    public bool hasGuests;

    //The Table Manager, Guest Spawner, Game Manager, and Player
    public TableManager tm;
    public GuestSpawner gs;
    public GameManager gm;
    private PlayerController player;
    private AudioManager am;
    private SpillSpawner ss;

    public string wantItem; //Var for what the guests want to order
    private bool inCollider; //Bool to check if the player is near

    public Animator bubbleAnim; //The animator for the speech bubble
    public Animator tableAnim; //The animator for the table itself

    //Variable for choosing which item the table wants
    private int selectItem;

    private int orderAgain; //Int to decide if the guest(s) will order again

    private int tab; //The Table's bill
    private int tabMulti; //The bill multiplier (Four people means 4x the money)

    public Animator XBubble; //Animator for the X Bubble

    public Table[] neighbors; //Array to keep track of whose at the table(s) nearby

    private bool inProgress; //Self-explanatory
    private bool stopFeeling;

    private int chance; //Var for running random ranges that don't necessarily have a set value

    //The positions where brawlers will be spawned
    public GameObject top;
    public GameObject right;
    public GameObject bottom;
    public GameObject left;

    public bool toppled; //Is the table broken?
    private YeetTable yt; //The script that breaks this table
    private int tableCost; //How much it costs to fix this table

    private Vector2 ogPos; //Where it usually is
    private bool fixing; //Are we fixing it atm?

    public bool tooAngry;

    void Start()
    {
        player = FindObjectOfType<PlayerController>(); //Find the player
        yt = GetComponent<YeetTable>(); //Find the yeeting script
        am = FindObjectOfType<AudioManager>(); //Find the AudioManager
        ss = FindObjectOfType<SpillSpawner>();

        //Hide the bubbles we're not using yet
        bubbleAnim.Play("Invisible");
        XBubble.Play("XInvisible");

        inCollider = false; //The player is not nearby
        inProgress = false; //We're not doing anything
        toppled = false; //The table is not broken
        fixing = false; //So we're not fixing it
        tooAngry = false; //And we're definitely not too angry yet
        stopFeeling = false;

        ogPos = transform.position; //Remember where we started
        tab = 0; //Set the bill to zero (cause no one has bought anything, yet!)
        tabMulti = 1; //Set the multiplier to the base value
        tableCost = 75;
    }

    public void ClearTable()
    {
        //"Pay" the tab, clear the table, and lower the tavern's current capacity
        hasGuests = false;
        if (gm.brawling)
        {
            tableAnim.Play("NoTable");
        }
        else
        {
            tableAnim.SetBool("noGuests", true);
        }
        bubbleAnim.Play("Invisible");
        StopAllCoroutines();
        gs.capacity -= 1;
        gm.money += tab;
        tab = 0;
        guestType = "NA";
        hatedType = "none";
        hatedType2 = "none";
        if (gm.CheckForGuests() == false)
        {
            am.Stop("Crowd");
            am.Stop("Cups");
        }
    }
    
    //If the brawl has ended
    public void BrawlEnd()
    {
        //Then check if this table has been toppled and make sure there's no one sitting at it
        if (toppled == false)
        {
            tableAnim.SetBool("noGuests", true);
        }
    }

    //On click
    private void OnMouseDown()
    {
        if (toppled == false)
        {
            //If this table is empty and clicked on
            if (!hasGuests)
            {
                tm.tableSelect = this; //Set it as the Table Manager's selected table
            }

            //If the player is nearby and trying to submit an item
            if (inCollider)
            {
                //Check if the player is holding what the table wants
                if (wantItem == player.holding)
                {
                    //If they are then hide the player's item and the speech bubble
                    //("give" the item to the table)
                    player.DropEverything();
                    bubbleAnim.Play("Invisible");
                    tm.tableSelect = null;
                    StopCoroutine("GetAngry");
                    gm.rage -= 5;

                    if (am.FindClip("Cups") == false)
                    {
                        am.Play("Cups");
                    }

                    //Maybe the player will get a heart!
                    chance = Random.Range(1, 4);
                    if (chance == 3)
                    {
                        am.Play("Heart");
                        gm.hearts += 1;
                    }

                    //Decide if the table will order something else
                    orderAgain = Random.Range(1, 4);

                    //The table has a one in four chance of reordering
                    if (orderAgain == 1)
                    {
                        //Reorder
                        Invoke("PickMenuItem", Random.Range(1f, 4f));
                        ss.Spawn(); // If the table decides to order again spill something for fun
                    }
                    else
                    {
                        //"Pay" the tab, clear the table, and lower the tavern's current capacity
                        ClearTable();
                        am.Play("Coin");
                        gm.rage -= 10;
                    }
                }
                else if (player.holding != "nothing") //If the player brought the table the wrong item
                {
                    //Let them know and make the table mad
                    XBubble.Play("XBubble");
                    gm.rage += 10;
                }
            }
        }
        else if (toppled) //if the table is broken
        {
            //and there isn't a brawl going on, the player can afford it, is in range, and the table isn't already being fixed
            if ((gm.brawling == false) && (tableCost <= gm.money) && inCollider && fixing == false)
            {
                //Then fix the table
                gm.money -= tableCost;
                XBubble.Play("FixTable");
                FindObjectOfType<AudioManager>().Play("BuyS");

            }
            else
            {
                if (gm.brawling == false)
                {
                    XBubble.Play("XBubble");
                    FindObjectOfType<Tutorial>().NewTutorial(15);
                }
            }
        }
    }

    void Update()
    {
        //Continuously check if we're brawling to make sure that no one sits at the tables accidentally
        if (gm.brawling)
        {
            ClearTable();
        }

        //If the table is being fixed
        if (XBubble.GetCurrentAnimatorStateInfo(0).IsName("FixTable"))
        {
            fixing = true; //Let the code know
        }
        
        //And when it is done being fixed (Fixing animation stops)
        if (fixing && XBubble.GetCurrentAnimatorStateInfo(0).IsName("XInvisible"))
        {
            fixing = false; //Tell the code
            yt.render.sprite = yt.last;
            yt.decorRender.sprite = yt.lastDecor;
            tableAnim.SetBool("noGuests", true);
            transform.position = ogPos; //Put the table back in place
            toppled = false; //Let everyone know it is fixed
            yt.ResetTable(); //And reset the code to break it so that we can repeat the process
        }

        //If there are guests at the table
        if (hasGuests)
        {

            //Check who is sitting nearby
            for (int i = 0; i < neighbors.Length; i++)
            {
                //And if we're not already interacting with someone
                if (inProgress == false && stopFeeling == false)
                {
                    //Figure out if we like the neighbors or not
                    if ((neighbors[i].guestType == hatedType) || (neighbors[i].guestType == hatedType2))
                    {
                        StartCoroutine(Angry());
                    }
                    //No one likes Bastards and that's a FactTM
                    else if (neighbors[i].guestType == "Bastard")
                    {
                        StartCoroutine(Angry());
                    }
                    //Oh we like those people
                    else if ((tooAngry == false) && ((neighbors[i].guestType == "Fairy") || (neighbors[i].guestType == likeType)))
                    {
                        StartCoroutine("Happy");
                    }

                }

            }
        }

    }

    // ~ Table Technical Bits ~

    //This function "seats" the guests at the table
    public void SeatGuests(NPC guests)
    {
        //Set the guests ID up so we know who to animate and let the table know it has guests
        tableGuests = guests.groupName;
        guestType = guests.groupType;
        hatedType = guests.hate1;
        hatedType2 = guests.hate2;
        likeType = guests.likes;

        tooAngry = false;

        tableAnim.SetBool("noGuests", false);
        bubbleAnim.SetInteger("Anger", 0);

        //Switch/case to check who is sitting at the table and seat them
        switch (tableGuests)
        {
            //The Elf Gang
            case "Elf Gang":
                tableAnim.Play("ElfTable"); //The table animation
                tabMulti = 4; //Their tab multiplier
                break;
            
            //Eras and Alexsi
            case "Eras & Alexsi":
                tableAnim.Play("ENATable");
                tabMulti = 3;
                break;
            
            //The Cute Coven
            case "Cute Coven":
                tableAnim.Play("CCovenTable");
                tabMulti = 3;
                break;
            
            //The ChurchTM
            case "The ChurchTM":
                tableAnim.Play("ChurchTable");
                tabMulti = 4;
                break;

            //Jason (The BardTM)
            case "Jason":
                tableAnim.Play("JasonTable");
                tabMulti = 1;
                break;

            //The Happy Couple
            case "Happy Couple":
                tableAnim.Play("HCoupleTable");
                tabMulti = 2;
                break;

            //The Aasimar Pair
            case "Aasimar Pair":
                tableAnim.Play("AaTable");
                tabMulti = 2;
                break;

            //Finn the Graveyard Keeper
            case "Finn & Xhudar":
                tableAnim.Play("FNXTable");
                tabMulti = 2;
                break;

            //Baldric and Co
            case "Baldric & Co":
                tableAnim.Play("BNCTable");
                tabMulti = 4;
                break;

            //The Knights
            case "The Knights":
                tableAnim.Play("KnightsTable");
                tabMulti = 3;
                break;
            
            //Jas
            case "Jas":
                tableAnim.Play("JasTable");
                tabMulti = 1;
                break;

            //Sunshine Fairy
            case "Sunshine Fairy":
                tableAnim.Play("SFairy");
                tabMulti = 2;
                break;

            //Rose Fairy
            case "Rose Fairy":
                tableAnim.Play("RoFairyTable");
                tabMulti = 2;
                break;

            //River Fairy
            case "River Fairy":
                tableAnim.Play("RivFairyTable");
                tabMulti = 2;
                break;
            
            //Orc Prince (and bros)
            case "Orc Prince":
                tableAnim.Play("OrcPrinceTable");
                tabMulti = 4;
                break;

            //Tiefling Biker Gang
            case "Tiefling Biker Gang":
                tableAnim.Play("TBGTable");
                tabMulti = 4;
                break;

            //Fae Pair
            case "Fae Pair":
                tableAnim.Play("FaePTable");
                tabMulti = 2;
                break;

            //Me and Nicole!
            case "Rora & Nicole":
                tableAnim.Play("RNNTable");
                tabMulti = 3;
                break;

            //Poly Gang
            case "Poly Gang":
                tableAnim.Play("PolyGTable");
                tabMulti = 3;
                break;

            //Princess and Her Knight
            case "Princess & Her Knight":
                tableAnim.Play("PAHKTable");
                tabMulti = 4;
                break;

            //Breakfast Break-Fist
            case "Breakfast Break-Fist":
                tableAnim.Play("BreakTable");
                tabMulti = 1;
                break;

            //Igraldor
            case "Igraldor":
                tableAnim.Play("IgraldorTable");
                tabMulti = 1;
                break;

            //King and Queen
            case "King and Queen":
                tableAnim.Play("KingsTable");
                tabMulti = 5;
                break;

            //Shady Fucker
            case "Shady":
                tableAnim.Play("ShadyTable");
                tabMulti = 1;
                break;

            //Lord's Son
            case "Lord's Son":
                tableAnim.Play("LordsTable");
                tabMulti = 2;
                break;


            //Catch-all case for if something goes wrong
            case null:
                Debug.Log("Oops!");
                break;
        }

        if (am.FindClip("Crowd") == false)
        {
            am.Play("Crowd");
        }

        FindObjectOfType<Tutorial>().NewTutorial(2);

        //Pick something to order!
        Invoke("PickMenuItem", Random.Range(1f, 4f));

    }

    //Coroutine that picks what the table will be ordering
    public void PickMenuItem()
    {
        //Randomly pick the item via a switch case
        selectItem = Random.Range(0, 3);

        switch (selectItem)
        {
            //Play the corresponding animation

            case 0: //Beer

                bubbleAnim.Play("BeerBubble"); //The animation that *should* be playing
                wantItem = "beer"; //The item the table wants
                tab += 3 * tabMulti; //The price of the order
                break;

            case 1: //Ale

                //See above
                bubbleAnim.Play("AleBubble");
                wantItem = "ale";
                tab += 5 * tabMulti;
                break;

            case 2: //Soup

                //See above
                bubbleAnim.Play("SoupBubble");
                wantItem = "soup";
                tab += 10 * tabMulti;
                break;

        }

        //Start the coroutine that will make the table get progressively angrier
        StartCoroutine("GetAngry");
    }

    //This coroutine makes people angry!
    public IEnumerator GetAngry()
    {
        //After ten seconds
        yield return new WaitForSeconds(5f);

        //The table is annoyed
        bubbleAnim.SetInteger("Anger", 1);
        gm.rage += 5;

        //Another five
        yield return new WaitForSeconds(5f);

        //And they're angry
        bubbleAnim.SetInteger("Anger", 2);
        gm.rage += 10;

        //Another five will make them
        yield return new WaitForSeconds(5f);

        //Fucking pissed
        bubbleAnim.SetInteger("Anger", 3);
        gm.rage += 15;
        tooAngry = true;

        //That's five seconds too many!
        yield return new WaitForSeconds(5f);

        //Play the "leaving" animation
        bubbleAnim.Play("BubbleBurst");
        gm.rage += 20;

        //Short delay to make sure it looks right
        yield return new WaitForSeconds(1f);
        ss.Spawn(); // And spill something for the DramaTM

        //Then remove the guests from the table
        ClearTable();
    }

    public IEnumerator Angry()
    {
        inProgress = true; //We're interacting with someone

        yield return new WaitForSeconds(4);
        gm.rage += 7; //Be angry about it
        XBubble.Play("AngryBubble");
        ss.Spawn(); // And chuck a drink for fun

        FindObjectOfType<Tutorial>().NewTutorial(6);
        inProgress = false;
    }

    public IEnumerator Happy()
    {
        inProgress = true; //We're interacting with someone
        yield return new WaitForSeconds(4);

        gm.rage -= 5; //We get happy!
        XBubble.Play("HappyBubble");

        //And if the player is lucky
        chance = Random.Range(1, 5);
        if (chance == 4)
        {
            //They'll get a heart!
            am.Play("Heart");
            gm.hearts += 1;
        }

        FindObjectOfType<Tutorial>().NewTutorial(11);
        inProgress = false;
    }

    //If the hired help is doing something fun!
    public void HelpEffect()
    {
        //Be happy about it! Calm everbody down!
        stopFeeling = true;

        XBubble.Play("HeartBubble");
        bubbleAnim.SetInteger("Anger", 0);
        StopCoroutine("GetAngry");
        StopCoroutine("Angry");

        //Give the player a heart
        gm.hearts += 1;

        //Figure out what the table ordered earlier and
        switch (selectItem)
        {
            //Play the corresponding animation all over again

            case 0: //Beer

                bubbleAnim.Play("BeerBubble"); //The animation that *should* be playing
                break;

            case 1: //Ale

                //See above
                bubbleAnim.Play("AleBubble");
                break;

            case 2: //Soup

                //See above
                bubbleAnim.Play("SoupBubble");
                break;

        }

        gm.money += Random.Range(5, 30);

        StartCoroutine("Wait");
    }

    public IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);

        //Okay fun time is over it's time to be mad again
        stopFeeling = false;
        StartCoroutine("GetAngry");
    }

    // ~ This bit accepts the player's input ~

    //When the player is standing in the collider (is nearby)
    private void OnTriggerStay2D(Collider2D other)
    {

        if (other != null)
        {
            if (other.gameObject.name == "Player")
            {
                //Let the code know the player is nearby
                inCollider = true;
            }
        }

    }

    //When the player leaves the collider
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision != null)
        {
            if (collision.gameObject.name == "Player")
            {
                //Let the code know the player has moved away
                inCollider = false;
            }
        }
    }
}
