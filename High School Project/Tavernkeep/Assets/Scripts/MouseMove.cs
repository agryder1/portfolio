using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMove : MonoBehaviour
{
    public float speed = 1.5f;
    private Vector3 target;
    Vector2 lookDirection = new Vector2(1, 0);
    public Animator anim;
    public Rigidbody2D rgb;

    void Start()
    {
        anim = GetComponent<Animator>();
        rgb = GetComponent<Rigidbody2D>();
        target = transform.position;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            target.z = transform.position.z;
        }

        lookDirection.Set(target.x - transform.position.x, target.y - transform.position.y);

        anim.SetFloat("LookX", lookDirection.x);
        anim.SetFloat("LookY", lookDirection.y);
        anim.SetFloat("Speed", target.magnitude);

        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        if (transform.position == target)
        {
            anim.SetFloat("LookX", target.x);
            anim.SetFloat("LookY", target.y);
            anim.SetFloat("Speed", 0);
        }

    }
}
