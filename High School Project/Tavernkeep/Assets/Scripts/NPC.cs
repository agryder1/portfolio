﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: October 13th, 2021
//Short 'n sweet code that tells the game what an NPC is and does a few other things

public class NPC : MonoBehaviour
{
    public TableManager tm; //The Table Manager
    public GuestSpawner gs; //The Guest Spawner
    public GameManager gm; //The Game Manager

    public Animator xBubble;

    public Animator anim;
    public string stand;
    public string wave;

    public string groupName; //The name of the group
    public string groupType; //Their type
    public string hate1; //Who they don't like
    public string hate2;
    public string likes; //And who they do
    public bool die; //When it's time to go
    public int spawn; //The numerical id of the NPC's spawnpoint

    private void Awake()
    {
        xBubble.Play("XInvisible");
    }

    void Start()
    {
        die = false; //It's not time to die (you were just born!)
        
        //Set up the tm and gs
        tm = GameObject.Find("Table Manager").GetComponent<TableManager>();
        gs = GameObject.Find("GuestSpawn").GetComponent<GuestSpawner>();
        gm = GameObject.Find("Game Manager").GetComponent<GameManager>();

        anim = GetComponent<Animator>();
        anim.Play(stand);

        FindObjectOfType<Tutorial>().NewTutorial(1);

        StartCoroutine("Countdown");
    }

    //When the group is clicked on
    private void OnMouseDown()
    {
        if (gm.open == false)
        {
            //Set the Table Manager's group selection to this group
            tm.groupSelect = this;
            xBubble.Play("HappyBubble");
            anim.Play(wave);
        }
    }

    void Update()
    {
        if (gm.brawling)
        {
            die = true;
        }

        //If the NPC has been "seated"
        if (die)
        {
            //Remove 1 from the spawn capacity so a new guest can enter
            gs.filledSpots.Remove(spawn);

            //And get rid of this gameobject because we don't need it anymore
            Destroy(gameObject);
        }
    }

    public IEnumerator Countdown()
    {
        yield return new WaitForSeconds(10f);
        xBubble.Play("AngryBubble");
        anim.Play(wave);
        gm.rage += 10;

        yield return new WaitForSeconds(7f);
        xBubble.Play("XBubble");

        yield return new WaitForSeconds(1);
        gm.rage += 20;
        Destroy(gameObject);
    }
}
