using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: ???

//This code manages the variables that have to transition between the main menu and the game
public static class Variables
{
    //The Booleans we'll use to toggle the variables in-between scenes
    public static bool music = true;
    public static bool sound = true;
    public static bool tutorial = true;

}
