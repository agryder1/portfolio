using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: ???

//This code allows the player to buy the HiredHelp buttons
public class BuyHiredHelp : MonoBehaviour
{
    private GameManager gm; //The game manager

    public Text priceTag; //The price tag

    public int price; //The price

    public Button realButton; //The "real" hired help button

    void Start()
    {
        realButton.gameObject.SetActive(false); //Get rid of the real one so that the player can't use it
        gm = GameObject.Find("Game Manager").GetComponent<GameManager>(); //Find the gm
        priceTag.text = price.ToString(); //Set up the price tag
        priceTag.gameObject.SetActive(false); //And hide it
    }

    //If the store is open
    public void Open()
    {
        //Show the tag
        priceTag.gameObject.SetActive(true);
    }

    //If it is closed
    public void Close()
    {
        //Hide the tag
        priceTag.gameObject.SetActive(false);
    }

    //Function for the player tries to buy the button
    public void Buy()
    {
        //Check if they can afford it and the store is open
        if ((price <= gm.hearts) && gm.open)
        {
            //Then take the money and play a fun sound
            gm.hearts -= price;
            FindObjectOfType<AudioManager>().Play("BuyS");

            //And swap out the buttons so that the real one can be pressed
            gameObject.SetActive(false);
            priceTag.gameObject.SetActive(false);
            realButton.gameObject.SetActive(true);

            //Play the tutorial that lets us know we've hired a friend
            FindObjectOfType<Tutorial>().NewTutorial(10);
        }
        else
        {
            //If we can't afford it then play a sad sound
            FindObjectOfType<AudioManager>().Play("NoBuy");
        }
    }
}
