using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: June 18th, 2021

//These are the things that an item needs to be sold
[System.Serializable]
public class Item
{
    public Sprite forDisp; //The sprite
    public Sprite forSale; //The blacked out sprite
    public int price; //The price
    public int heartP;
    public bool bought; //And whether or not it has been bought

}
