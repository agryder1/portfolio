using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: ???
//This is the cute little code for the secret flirt button

public class Flirt : MonoBehaviour
{
    //The Animators
    public Animator Suzanna;
    public Animator BaShuk;

    public GameManager gm;

    // Start is called before the first frame update
    void Start()
    {
        //Set up Suzanna
        Suzanna = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //If the secret button is pressed ( ` )
        if (Input.GetKeyDown(KeyCode.BackQuote) && gm.paused == false)
        {
            //Flirt :D
            Suzanna.Play("SuzannaFlirt");
            BaShuk.Play("Ba'ShukFlirt");
        }
    }
}
