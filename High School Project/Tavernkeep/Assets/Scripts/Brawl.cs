using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Aurora Ryder LU: September 15th, 2021

//This chonker spawns in the brawlers

public class Brawl : MonoBehaviour
{
    //Array for all the different skins brawlers can have (different people)
    public Sprite[] skins;

    //Vectors to mark where they should spawn
    public Vector3 top;
    public Vector3 right;
    public Vector3 bottom;
    public Vector3 left;

    //Brawler game object (that all others will be based on)
    public GameObject brawler;

    //The Game Manager, GuestSpawner, and BGMusic
    private GameManager gm;
    private GuestSpawner gs;
    private BGMusic bgm;
    
    //Body count (literally)
    public int bCount;

    public AnimatorOverrideController[] newAnim;
    //anim.runtimeAnimatorController = newAnim[0];

    // Start is called before the first frame update
    void Start()
    {
        //Find the components and reset the body count
        gm = GetComponent<GameManager>();
        gs = FindObjectOfType<GuestSpawner>();
        bgm = FindObjectOfType<BGMusic>();
        bCount = 0;
    }

    //Start a brawl
    public void StartBrawl()
    {
        //Reset the body counter and drop the beat
        bCount = 0;
        bgm.BrawlMusic();

        //Check every table and
        foreach (Table table in gm.tables)
        {
            //If the table has guests
            if (table.hasGuests)
            {
                //Then set up the vectors so we spawn people in the right places
                left = table.left.gameObject.transform.position;
                right = table.right.gameObject.transform.position;
                top = table.top.gameObject.transform.position;
                bottom = table.bottom.gameObject.transform.position;

                //Check who is sitting at the table
                switch (table.tableGuests)
                {
                    //The Aasimar Pair 1
                    case "Aasimar Pair":
                        //Then spawn them in at their proper locations
                        SpawnBrawler(0, top);
                        SpawnBrawler(1, right);
                        break;

                    //Baldric and Co 2
                    case "Baldric & Co":
                        SpawnBrawler(2, top);
                        SpawnBrawler(3, bottom);
                        SpawnBrawler(4, right);
                        SpawnBrawler(5, left);
                        break;

                    //The Cute Coven 3
                    case "Cute Coven":
                        SpawnBrawler(6, top);
                        SpawnBrawler(7, right);
                        SpawnBrawler(8, left);
                        break;

                    //The Elf Gang 4
                    case "Elf Gang":
                        SpawnBrawler(9, top);
                        SpawnBrawler(10, bottom);
                        SpawnBrawler(11, right);
                        SpawnBrawler(12, left);
                        break;

                    //Eras and Alexsi 5
                    case "Eras & Alexsi":
                        SpawnBrawler(13, top);
                        SpawnBrawler(14, right);
                        break;

                    //Fae Pair 6
                    case "Fae Pair":
                        SpawnBrawler(15, top);
                        SpawnBrawler(16, right);
                        break;

                    //Finn the Graveyard Keeper 7
                    case "Finn & Xhudar":
                        SpawnBrawler(17, top);
                        SpawnBrawler(18, right);
                        break;

                    //The Happy Couple 8
                    case "Happy Couple":
                        SpawnBrawler(19, top);
                        SpawnBrawler(20, right);
                        break;

                    //Jas 9
                    case "Jas":
                        SpawnBrawler(21, top);
                        break;

                    //Jason (The BardTM) 10
                    case "Jason":
                        SpawnBrawler(22, top);
                        break;

                    //Orc Prince (and bros) 11
                    case "Orc Prince":
                        SpawnBrawler(23, top);
                        SpawnBrawler(24, right);
                        SpawnBrawler(25, left);
                        break;

                    //Poly Gang 12
                    case "Poly Gang":
                        SpawnBrawler(26, right);
                        SpawnBrawler(27, top);
                        SpawnBrawler(28, left);
                        break;

                    //Me and Nicole! 13
                    case "Rora & Nicole":
                        SpawnBrawler(29, right);
                        SpawnBrawler(30, top);
                        break;

                    //The ChurchTM 14
                    case "The ChurchTM":
                        SpawnBrawler(31, top);
                        SpawnBrawler(32, right);
                        SpawnBrawler(33, left);
                        SpawnBrawler(34, bottom);
                        break;


                    //The Knights 15
                    case "The Knights":
                        SpawnBrawler(35, left);
                        SpawnBrawler(36, top);
                        SpawnBrawler(37, right);
                        break;

                    //Tiefling Biker Gang 16
                    case "Tiefling Biker Gang":
                        SpawnBrawler(38, top);
                        SpawnBrawler(39, left);
                        SpawnBrawler(40, right);
                        SpawnBrawler(41, bottom);
                        break;

                    //Princess and Her Knight
                    case "Princess & Her Knight":
                        SpawnBrawler(42, top);
                        SpawnBrawler(43, right);
                        break;

                    //King and Queen
                    case "King and Queen":
                        SpawnBrawler(44, top);
                        SpawnBrawler(45, right);
                        SpawnBrawler(46, bottom);
                        SpawnBrawler(46, left);
                        break;

                    //Igraldor
                    case "Igraldor":
                        SpawnBrawler(47, top);
                        break;

                    //Lord's Son
                    case "Lord's Son":
                        SpawnBrawler(48, top);
                        break;

                    //Shady
                    case "Shady":
                        SpawnBrawler(49, top);
                        break;

                    //Breakfast Break-Fist
                    case "Breakfast Break-Fist":
                        SpawnBrawler(50, top);
                        break;

                    case "NA":
                        //do nothing!
                        break;

                    //Catch-all case for if (when?) something goes wrong
                    default:
                        break;
                }

            }

        }

        //Check if we spawned any brawlers (because sometimes a brawl starts but no one is here)
        CheckCount();

        //Play the tutorial for when a brawl starts
        FindObjectOfType<Tutorial>().NewTutorial(4);

    }

    //Method to spawn in a brawler takes an index (brawler ID) and a spawn point (where they were sitting)
    public void SpawnBrawler(int index, Vector3 spawn)
    {
        //Make a new brawler
        GameObject nb = brawler;
        //Set their position to be next to their chair
        nb.transform.position = spawn;
        //Give them the proper skin
        if (index != 0)
        {
            nb.GetComponent<Animator>().runtimeAnimatorController = newAnim[index];
        }
        //nb.GetComponent<SpriteRenderer>().sprite = skins[index];
        //And plop them in place
        Instantiate(nb);

        //Add a body to the count
        bCount += 1;
    }

    //This function checks how many brawlers are left and if there is none it ends the brawl
    public void CheckCount()
    {
        //If there's no more brawlers
        if (bCount == 0)
        {
            //Set rage to zero, end the music
            gm.rage = 0;
            bgm.StartCoroutine("NoBrawlMusic");

            //And end the brawl
            foreach (Table table in gm.tables)
            {
                table.BrawlEnd();
            }
        }
    }
}
