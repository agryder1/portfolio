using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: ???

//This code temporarily disables the Hired Help buttons (puts them on cooldown)
public class DisableButton : MonoBehaviour
{
    private GameManager gm; //The Game Manager
    public bool ready; //Bool to check if the button is ready
    private Button button; //The button

    private void Start()
    {
        gm = GameObject.Find("Game Manager").GetComponent<GameManager>(); //Find the gm
        button = GetComponent<Button>(); //Find the button
        ready = true; //Hasn't been pushed yet so we're ready
    }

    //When the button is clicked
    public void Click()
    {
        //If the store is not open
        if (ready && (gm.open == false))
        {
            ready = false; //Start the cooldown
            StartCoroutine("Cooldown");
        }
    }

    IEnumerator Cooldown()
    {
        //Turn off the button so it can't be pressed again
        button.interactable = false;

        //Wait 20secs
        yield return new WaitForSeconds(20f);

        //Then turn on the button again and let the code know we're ready
        button.interactable = true;
        ready = true;
    }

}
