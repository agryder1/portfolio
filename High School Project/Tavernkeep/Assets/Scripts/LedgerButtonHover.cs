using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//Aurora Ryder LU: June 11th, 2021

//This code manages all of the input and user interaction of the ledger's main menu buttons
public class LedgerButtonHover : MonoBehaviour, IPointerEnterHandler
{
    public Text menuType; //The text we change
    private LedgerUI ledger; //And the ledger itself

    void Start()
    {
        //Find them both
        menuType = GameObject.Find("Menu Type").GetComponent<Text>();
        ledger = GameObject.Find("Ledger UI").GetComponent<LedgerUI>();
    }

    //If the player hovers over a button
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        //Change the menu text to the button's name (guest type)
        menuType.text = name;
    }

    //If the player clicks a button
    public void JumpTo()
    {
        //Figure out which guest type it is
        switch(name)
        {
            case "Royal":

                //Subtract one from the dispID to account for the one added by the RightArrow()
                ledger.dispId = -1;
                ledger.RightArrow(); //Then jump to the first entry of that guest type
                break;

            case "Bastard":

                ledger.dispId = 3;
                ledger.RightArrow();
                break;

            case "Holy":

                ledger.dispId = 6;
                ledger.RightArrow();
                break;

            case "Unholy":

                ledger.dispId = 7;
                ledger.RightArrow();
                break;

            case "Magic":

                ledger.dispId = 9;
                ledger.RightArrow();
                break;

            case "Fairy":

                ledger.dispId = 12;
                ledger.RightArrow();
                break;

            case "Party":

                ledger.dispId = 15;
                ledger.RightArrow();
                break;

            case "Loner":

                ledger.dispId = 20;
                ledger.RightArrow();
                break;

            default:
                Debug.Log("Oops!");
                break;
        }
    }

}
