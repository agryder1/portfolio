using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Aurora Ryder LU: October 12th, 2021
//This code spawns ale, beer, or soup spills when the tavern has guests

public class AleSpill : MonoBehaviour
{
    //The Player
    public PlayerController player;

    //Bool to check if the player is colliding with the spill
    public bool colliding;

    //The Game manager
    public GameManager gm;
    public SpillSpawner ss;

    //Bool to give the player temporary immunity
    private bool immune;

    public int id;
    public bool trip;

    // Start is called before the first frame update
    void Start()
    {
        //Find the gm and player objects
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        gm = GameObject.Find("Game Manager").GetComponent<GameManager>();
        ss = FindObjectOfType<SpillSpawner>();
        colliding = false;
        trip = false;
    }

    //Method to "clean up" the spill when the player clicks on it
    public void CleanUp(bool barley)
    {
        //It the player's hands are empty
        if (player.holding == "nothing" && barley == false)
        {
            //Destroy the spill and lower the rage
            Destroy(gameObject);
            ss.filledSpots.Remove(id);
            gm.rage -= 3;
        }
        else if (barley)
        {
            Destroy(gameObject);
            ss.filledSpots.Remove(id);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //If the player is colliding and they are not immune
        if (colliding && immune == false && trip == false)
        {
            trip = true;
            //Freeze them and spill whatever they are holding / trip them
            player.StartCoroutine("Spill");
            if (player.holding != "nothing")
            {
                //If they are holding something up the rage
                gm.rage += 5;
            }

            //Give the player immunity
            StartCoroutine("Immunity");
        }

    }

    //The immunity coroutine
    IEnumerator Immunity()
    {
        //One second pause to play the animation
        yield return new WaitForSeconds(1);
        immune = true;

        //Then a pause to let the player move away
        yield return new WaitForSeconds(1);
        immune = false;
        trip = false;
    }

}
