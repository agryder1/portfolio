using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: ???
//This code switches the on/off sprites for the main menu's buttons

public class ChangeSprite : MonoBehaviour
{
    private Button button; //The button we're messing with
    public Sprite on; //The on sprite
    public Sprite off; //The off sprite
    public string id; //The button's id

    private void Start()
    {
        //Get the components
        button = GetComponent<Button>();

        //Check which button this is
        switch(id)
        {
            //And if the variable is false then set the sprite to off
            case "music":
                if (Variables.music == false)
                {
                    button.image.sprite = off;
                }
                break;

            case "sound":
                if (Variables.sound == false)
                {
                    button.image.sprite = off;
                }
                break;

            case "help":
                if (Variables.tutorial == false)
                {
                    button.image.sprite = off;
                }
                break;
        }
    }

    //This function switches the sprite to the opposite of what it starts as
    public void Switch()
    {
        //If it's on
        if (button.image.sprite == on)
        {
            //Turn it off
            button.image.sprite = off;
        }
        else if (button.image.sprite == off) //And vice versa
        {
            button.image.sprite = on;
        }
    }
}
