using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Aurora Ryder LU: August 15th, 2021
//Stolen from Cavo's Adventure

public class Brawler : MonoBehaviour
{
    /* This code makes an NPC move in a random pattern by
   assigning the four directions a number
   randomingly choosing one of those numbers
   and starting a timer for how long we want them to walk
   and for how long we want them to hold still for.*/

    private Vector2 minWalkPoint; //The bottom left corner of the NPC's walk area
    private Vector2 maxWalkPoint; //The top right corner of the NPC's walk area

    public float moveSpeed; //Self-explanatory

    //Check if the NPC is walking
    public bool isWalking;

    //Timer for how long the NPC is walking / a variable for counting down
    public float walkTime;
    private float walkCounter;

    //Timer for how long the NPC is standing still / variable for counting that down
    public float waitTime;
    private float waitCounter;

    public int walkDirection; //The direction that the NPC will move

    public Collider2D walkzone; //The area that the NPC is allowed to walk in

    private bool hasWalkZone; //Variable used to check if the NPC has to stay in a designated area

    private bool stopWalk; //Stop walking bool

    private int prevDirect; //The last direction we were walking

    private float otherY; //The person we're fighting with's Y coordinate
    private Brawler otherGuy; //The person we're fighting with

    private PlayerController player; //The player
    private Brawl brawl; //The brawl starter
    private int chance; //An int for when we have a random int calculation

    public bool inCollider; //Bool for when someone is in our area
    public bool stopLook; //Bool to stop looking for an opponent
    private bool fighting; //Bool for when we're fighting
    private bool inProgress; //Bool for when we're busy
    public int fighter; //Whether we are the first or second fighter

    public Animator anim; //Our Animator
    public Button killS; //The button that kills us
    public Canvas canvas; //The canvas

    // Start is called before the first frame update
    void Start()
    {
        //Find the components we need
        player = FindObjectOfType<PlayerController>();
        canvas = FindObjectOfType<Canvas>();
        brawl = FindObjectOfType<GameManager>().GetComponent<Brawl>();
        anim = GetComponent<Animator>();

        //Reset the walk and wait counters
        waitCounter = waitTime;
        walkCounter = walkTime;

        //Get the NPC moving as soon as the game starts
        StartCoroutine(Walk());
        fighter = 0;

        //Reset our bools
        fighting = false;
        inProgress = false;
        stopLook = false;
    }

    // Update is called once per frame
    void Update()
    {
        //If we're not fighting and we get clicked on
        if (inCollider && fighting == false && Input.GetMouseButtonDown(0))
        {
            //Fuckin' die
            Die();
        }

        //If we're supposed to be walking
        if (stopWalk == false)
        {
            //If the NPC is supposed to be walking...
            if (isWalking == true)
            {
                //Then hecking walk??
                walkCounter -= Time.deltaTime; //Countdown the walk timer so the NPC will stop walking
                anim.SetBool("Walking", true); //Let Animator know we're walking

                switch (walkDirection) //Switch statement for the four directions
                {
                    case 0: //Moving up

                        transform.Translate(Vector2.up * Time.deltaTime);
                        anim.SetFloat("LookX", 0);
                        anim.SetFloat("LookY", 0.5f);
                        break;

                    case 1: //Moving to the right
                            //See case 0 for comments
                        transform.Translate(Vector2.right * Time.deltaTime);
                        anim.SetFloat("LookX", 0.5f);
                        anim.SetFloat("LookY", 0);
                        break;

                    case 2: //Moving down
                            //See case 0 for comments
                        transform.Translate(Vector2.down * Time.deltaTime);
                        anim.SetFloat("LookX", 0);
                        anim.SetFloat("LookY", -0.5f);
                        break;

                    case 3: //Moving to the left
                            //See case 0 for comments
                        transform.Translate(Vector2.left * Time.deltaTime);
                        anim.SetFloat("LookX", -0.5f);
                        anim.SetFloat("LookY", 0);
                        break;

                }

                //If the walk counter has reached zero
                if (walkCounter < 0)
                {
                    isWalking = false; //Stop walking
                    waitCounter = waitTime; //Reset the wait counter
                    anim.SetBool("Walking", false); //Let Animator know we've stopped
                }

            }
            else
            {
                waitCounter -= Time.deltaTime; //Countdown the wait counter
                transform.Translate(Vector2.zero); //Stop the NPC movement (Change position by zero)

                //If the wait counter has reached zero
                if (waitCounter < 0)
                {
                    //Choose a new direction to walk and start the cycle over again
                    ChooseDirection();
                }
            }
        }
        else //If we're not supposed to be walking
        {
            anim.SetBool("Walking", false); //Let Animator know we've stopped
        }

        //If we're throwin' hands
        if (fighting && inProgress == false)
        {
            //Then show it
            StartCoroutine("Attack");
        }

    }

    //Function used at the start to get things going
    public IEnumerator Walk()
    {
        //Wait for a few seconds
        yield return new WaitForSeconds(Random.Range(0, 4));

        //And then choose a direction
        ChooseDirection();
    }

    //Function that chooses the direction that the NPC will move in and then moves
    public void ChooseDirection()
    {
        //reset the walk counter
        walkDirection = Random.Range(0, 4);
        isWalking = true;
        walkCounter = walkTime;
    }

    //Function used to let this brawler know they should be fighting
    public void StartFight()
    {
        fighting = true;
        stopWalk = true;
        transform.Translate(Vector2.zero); //Stop the NPC movement (Change position by zero)
    }

    //Function usued to end the fight we're in
    public void EndFight()
    {
        //Stop fighting
        fighting = false;
        StopAllCoroutines();

        //And either
        chance = Random.Range(0, 4);

        switch (chance)
        {
            //Die
            case 1:
                Die();
                break;

            //Run away
            case 2:
                break;

            //Or stun the player
            case 3:
                player.StartCoroutine("Stun");
                break;
        }

        //Then stop walking and stop looking for someone to fight
        stopWalk = false;
        stopLook = false;

        //Then walk away
        ChooseDirection();
    }

    //Coroutine for animating attacks
    IEnumerator Attack()
    {
        //This is repeated as necessary

        inProgress = true;

        //Wait for a few seconds
        yield return new WaitForSeconds(Random.Range(1f, 4f));
        anim.Play("AasimarFight"); //Then strike

        inProgress = false;
    }

    //This function kills the brawler
    public void Die()
    {
        //If we're not dead already
        if (gameObject != null)
        {
            //Then lower the body count, check if the brawl should end
            brawl.bCount -= 1;
            brawl.CheckCount();
            Destroy(gameObject); //And then die
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other != null)
        {
            //If a brawler enters the trigger area
            if (other.gameObject.tag == "Brawler" && (stopLook == false) && (other.gameObject.GetComponent<Brawler>().stopLook == false))
            {
                //Get on their level
                otherY = other.gameObject.transform.position.y;
                otherGuy = other.gameObject.GetComponent<Brawler>();
                otherGuy.stopLook = true;

                stopLook = true;
                
                //Look em in the eyes
                if (otherGuy.transform.position.x < transform.position.x)
                {
                    anim.SetFloat("LookX", -0.5f);
                    otherGuy.anim.SetFloat("LookX", 0.5f);
                }
                else
                {
                    anim.SetFloat("LookX", 0.5f);
                    otherGuy.anim.SetFloat("LookX", -0.5f);
                }

                //And throw hands
                StartFight();
                otherGuy.StartFight();

                //Make sure we're on the same level
                gameObject.transform.position = new Vector3(transform.position.x, otherY, transform.position.z);

                //Set up both kill switches
                Button nk = killS;
                nk.GetComponent<KillSwitch>().fight1 = gameObject;
                nk.GetComponent<KillSwitch>().fight2 = other.gameObject;

                //And then spawn it in
                Button nb = Instantiate(nk, canvas.transform);
                nb.gameObject.transform.SetParent(canvas.transform);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        //If we collide with something
        if (collision != null)
        {
            //Find which way we were going
            switch (walkDirection)
            {
                //And go the opposite direction
                case 0:
                    walkDirection = 2;
                    break;

                case 1:
                    walkDirection = 3;
                    break;

                case 2:
                    walkDirection = 0;
                    break;

                case 3:
                    walkDirection = 1;
                    break;
            }
        }
    }
}