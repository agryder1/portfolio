﻿using UnityEngine.Audio;
using UnityEngine;

//This code is the counterpart to the one from Brackeys
//Aurora Ryder LU: ???

//This bb creates the Sound class that makes it easy for us to play sound effects/music
[System.Serializable]
public class Sound {

	public string name;

	public AudioClip clip;

	[Range(0f, 1f)]
	public float volume = .75f;
	[Range(0f, 1f)]
	public float volumeVariance = .1f;

	[Range(.1f, 3f)]
	public float pitch = 1f;
	[Range(0f, 1f)]
	public float pitchVariance = .1f;

	public bool loop = false;

	public AudioMixerGroup mixerGroup;

	[HideInInspector]
	public AudioSource source;

}
