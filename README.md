# Portfolio

## High School Project
This folder contains the complete Unity project for Tavernkeep, which is a game that I created, published, and sold independently.

## Pixel Art and Animation
This folder contains some of my favourite pixel art and animations that I have made over the years.

## University Projects
This folder contains the source code of my two best Graphics projects, and a Design Document that I created for my Video Game Design class last semester.