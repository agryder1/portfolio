I'm really proud of this one.

Cool things I did that you should consider:
1) Pathfinding algorithm that mimics modern infrastructure and connects randomly placed buildings
2) Building variation that includes fully textured semi-complex house shapes
2a) I did all the geometry by hand. I am now good at geometry.
3) All buildings are placed -- and have their geometry chosen -- randomly, so you always get a new world when you regenerate
3) Cool Medieval textures that I made myself!
3b) Grass textures are randomly chosen, too!
4) The buildings take very little time to generate/regenerate