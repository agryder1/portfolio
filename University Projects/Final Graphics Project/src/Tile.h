#pragma once
#include "general.h"

class Tile {
    private:
        TileType tileType = OPEN;
        TileGridPlacement gridPlacement;
        Vertex origin;
        int facing = -1;
        int tilesetIndex;
        bool checked = false;
        int buildingLength = 1;
        int buildingWidth = 1;
        float xOffset = 0.0f;
        float yOffset = 0.0f;

    public:
        Tile(const Vertex tileOrigin, const TileGridPlacement placement, const int index);
        
        Tile();

        int DistanceToPoint(const Vertex destination) { //this isn't going to change so we can declare it here
            return abs((int) this->origin.x - (int) destination.x) + abs((int) this->origin.z - (int) destination.z);
        };

        void setType(const TileType newType); //For everything except big buildings
        void setType(const TileType newType, const Direction newFacing, const int length, const int width); //For big buildings
        TileType getType() {return this->tileType;};

        const TileGridPlacement getGridPlacement() {return this->gridPlacement;}; 

        const Vertex getTileOrigin() {return this->origin;};

        const int getFacing() {return this->facing;};
        const int getIndex() {return this->tilesetIndex;};

        void setChecked(const bool checkVal) {this->checked = checkVal;};
        bool getChecked() {return this->checked;};

        const int getLength() {return this->buildingLength;};
        const int getWidth() {return this->buildingWidth;};

        void setOffset(const float x, const float y) {this->xOffset = x; this->yOffset = y;};
        const glm::vec2 getOffset() {return glm::vec2{this->xOffset, this->yOffset};};
};