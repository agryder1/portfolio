#pragma once
#include "..\thirdparty\glew\include\GL\glew.h"
#include "..\thirdparty\glfw\include\GLFW\glfw3.h"
#include "..\thirdparty\glfw\include\GLFW\glfw3native.h"
#include "..\thirdparty\glm\glm\glm.hpp"
#include "..\wolf\wolf.h"
#include "general.h"
#include "Geometry.h"
#include "Tile.h"

class Baseplate {
    private:
        const int TOTAL_TILES;
        const int NUM_VERTICES;

        GLuint buildingShader;
        GLuint B_VAO, B_VBO, B_EBO;
        int numBuildIndices = 0;

        GLuint bigShader;
        GLuint BB_VAO, BB_VBO, BB_EBO;
        int numBigIndices = 0;

        GLuint grassNPathShader;
        GLuint GNP_VAO, GNP_VBO, GNP_EBO;
        int numGrassNPathIndices = 0;

        wolf::Texture* allTextures;

    public:
        Baseplate(const int length, const int numBuildings);
        ~Baseplate();
        
        //I know the std::vector<Tile>*s aren't const. I do not know how to fix that.
        bool CheckTileNeighbors(std::vector<Tile>* tileSet, const int length, const int index);
        int FindBestPath(std::vector<Tile>* tileset, const int length, const char* possibleLocs, const int srcIndex, const int lastSrcIndex, const Vertex destOrigin);
        void Render(const glm::mat4 projection, const glm::mat4 view, const glm::mat4 world);
};