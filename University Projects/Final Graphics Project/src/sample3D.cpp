#include "sample3D.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../samplefw/Grid3D.h"

//I recommend trying to keep these two within a sensical ratio.
const int Sample3D::BASEPLATE_SIZE = 100; //The actual baseplate size will be this number squared
const int Sample3D::NUM_BUILDINGS = 500;

//Clean up!
Sample3D::~Sample3D()
{
	printf("Destroying 3D Sample\n");
	delete m_pGrid;
	delete m_pDecl;
	wolf::BufferManager::DestroyBuffer(m_pVB);
	wolf::ProgramManager::DestroyProgram(m_pProgram);
}

void Sample3D::init()
{
    glEnable(GL_DEPTH_TEST);
	m_pApp->disableCursor();
        
    // Only init if not already done
    if(!m_pProgram)
    {
		//Create our world and camera
		this->baseplate = new Baseplate(BASEPLATE_SIZE, NUM_BUILDINGS);
		this->camera = new Camera(glm::vec3(50.0f, 50.0f, -50.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		this->lastMousePos = m_pApp->getScreenSize() / 2.0f;
    }

    printf("Successfully initialized 3D Sample\n");
}

void Sample3D::update(float dt) 
{
	//Need a different version of deltatime because we need to count time since last frame not time elapsed
	currentTime = (float) glfwGetTime();
	m_deltaTime = currentTime - lastTime;
	lastTime = currentTime;

	//Process mouse movement
	mousePos = m_pApp->getMousePos();
	glm::vec2 mouseMovement = mousePos - lastMousePos;
	camera->ProcessMouseMovement(mouseMovement.x, -mouseMovement.y);
	lastMousePos = mousePos;
}

void Sample3D::render(int width, int height)
{
	glClearColor(0.01f, 0.80f, 1.0f, 1.0f); //Blue background cause I didn't have enough time to build a skybox
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Create our projection, view, and world matrices
	glm::mat4 projection = glm::perspective(glm::radians(camera->Zoom), (float)width / (float)height, 0.1f, 5000.0f);
	glm::mat4 view = camera->GetViewMatrix();
	glm::mat4 world = glm::mat4(1.0f);

	//Camera controls
	if (m_pApp->isKeyDown('W')) {
		camera->ProcessKeyboard(FORWARD, m_deltaTime);
	}

	if (m_pApp->isKeyDown('A')) {
		camera->ProcessKeyboard(LEFT, m_deltaTime);
	}

	if (m_pApp->isKeyDown('S')) {
		camera->ProcessKeyboard(BACKWARD, m_deltaTime);
	}

	if (m_pApp->isKeyDown('D')) {
		camera->ProcessKeyboard(RIGHT, m_deltaTime);
	}

	//Shuffle!
	//(I recommend holding it down for maximum silliness)
	if (m_pApp->isKeyDown('R')) {
		delete(this->baseplate);
		this->baseplate = new Baseplate(BASEPLATE_SIZE, NUM_BUILDINGS);
	}
	
	//Render
	this->baseplate->Render(projection, view, world);
}