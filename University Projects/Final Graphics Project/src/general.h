#pragma once
#include "..\thirdparty\glew\include\GL\glew.h"
#include "..\thirdparty\glfw\include\GLFW\glfw3.h"
#include "..\thirdparty\glfw\include\GLFW\glfw3native.h"
#include "..\thirdparty\glm\glm\glm.hpp"
#include <iostream>
#include <vector>
#include <algorithm>

struct Vertex {
    GLfloat x, y, z;
    GLfloat u, v;
};

//It is much simpler to store where a given section of a
//texture is than it is to try and keep all the numbers straight,
//so I made a lil' struct
struct TextureCoordinates {
    GLfloat TLu, TLv,
            TRu, TRv,
            BLu, BLv,
            BRu, BRv;
};

//We use compass directions for readibility
enum Direction {
    NORTH = 0,
    EAST = 1,
    SOUTH = 2,
    WEST = 3
};

//Tiles can have a variety of different types
enum TileType {
    OPEN,
    PATH,
    PATH_START,
    BUILDING,
    BIG_BUILDING_PIECE,
    LHOUSE,
    COMMUNITY_HOUSE,
    LONG_STONE,
    STAIR_HOUSE,
    HALF_HOUSE,
    CHURCH
};

//This enum keeps track of where on the grid a tile is so that we can stay within our array bounds
enum TileGridPlacement {
    TOP_LEFT,
    TOP_MIDDLE,
    TOP_RIGHT,
    MIDDLE_LEFT,
    MIDDLE,
    MIDDLE_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_MIDDLE,
    BOTTOM_RIGHT
};

//I use two different random generators, one for numbers within a range, and one for picking a compass direction
int genRandom(const int lower, const int upper);
int pickDirection(const Direction* numberSet, const int length);
