#pragma once
#include "general.h"

/*Pseudo random number generator that generates
  numbers within a specified range.
  lower: the lower bound
  upper: the upper bound
  returns: a random number*/
int genRandom(const int lower, const int upper) {
  return (rand() % (upper - lower + 1)) + lower;
}

//Randomly index a direction from an array of choices
int pickDirection(const Direction* numberSet, const int length)
{
  return numberSet[genRandom(0, length - 1)];
}