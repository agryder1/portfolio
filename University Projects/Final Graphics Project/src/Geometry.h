#pragma once
#include "general.h"

//It's a good thing I like repetive tasks or this geometry might not have existed..

//Rather than try to remember how to access each individual part of the
//texture, I've saved them in these
static const TextureCoordinates STONE1 = {
    0, 0, //Top Left
    0.333f, 0, //Top Right
    0, 0.25f, //Bottom Left
    0.333f, 0.25f //Bottom Right
};

static const TextureCoordinates STONE2 = {
    0.333f, 0,
    0.666f, 0,
    0.333f, 0.25f,
    0.666f, 0.25f
};

static const TextureCoordinates STONE3 = {
    0.666f, 0,
    1.0f, 0,
    0.666f, 0.25f,
    1.0f, 0.25f,
};

static const TextureCoordinates GRASS1 = {
    0, 0.25f,
    0.333f, 0.25f,
    0, 0.5f,
    0.333f, 0.5f,
};

static const TextureCoordinates GRASS2 = {
    0.333f, 0.25f,
    0.666f, 0.25f,
    0.333f, 0.5f,
    0.666f, 0.5f
};

static const TextureCoordinates GRASS3 = {
    0.666f, 0.25f,
    1.0f, 0.25f,
    0.666f, 0.5f,
    1.0f, 0.5f,
};

static const TextureCoordinates WOOD = {
    0, 0.5f,
    0.333f, 0.5f,
    0, 0.75f,
    0.333f, 0.75f
};

static const TextureCoordinates PLASTER1 = {
    0.333f, 0.5f,
    0.666f, 0.5f,
    0.333f, 0.75f,
    0.666f, 0.75f
};

static const TextureCoordinates PLASTER2 = {
    0.666f, 0.5f,
    1.0f, 0.5f,
    0.666f, 0.75f,
    1.0f, 0.75f,
};

static const TextureCoordinates ROAD = {
    0, 0.75f,
    0.333f, 0.75f,
    0, 1.0f, 
    0.333f, 1.0f,
};

static const TextureCoordinates ROOF1 = {
    0.333f, 0.75f,
    0.666f, 0.75f,
    0.333f, 1.0f,
    0.666f, 1.0f,
};

static const TextureCoordinates ROOF2 = {
    0.666f, 0.75f,
    1.0f, 0.75f,
    0.666f, 1.0f,
    1.0f, 1.0f,
};

//0g
static const Vertex Grass1Vertices[] = {
    //Base
    {0.0f, 0.0f, 0.0f, GRASS1.TLu, GRASS1.TLv},
    {0.0f, 0.0f, 1.0f, GRASS1.BLu, GRASS1.BLv},
    {1.0f, 0.0f, 1.0f, GRASS1.BRu, GRASS1.BRv},
    {1.0f, 0.0f, 0.0f, GRASS1.TRu, GRASS1.TRv},
};

static const int GrassIndices[] = {
    0, 1, 2,
    2, 3, 0,
};

static const Vertex Grass2Vertices[] = {
    //Base
    {0.0f, 0.0f, 0.0f, GRASS2.TLu, GRASS2.TLv},
    {0.0f, 0.0f, 1.0f, GRASS2.BLu, GRASS2.BLv},
    {1.0f, 0.0f, 1.0f, GRASS2.BRu, GRASS2.BRv},
    {1.0f, 0.0f, 0.0f, GRASS2.TRu, GRASS2.TRv},
};

static const Vertex Grass3Vertices[] = {
    //Base
    {0.0f, 0.0f, 0.0f, GRASS3.TLu, GRASS3.TLv},
    {0.0f, 0.0f, 1.0f, GRASS3.BLu, GRASS3.BLv},
    {1.0f, 0.0f, 1.0f, GRASS3.BRu, GRASS3.BRv},
    {1.0f, 0.0f, 0.0f, GRASS3.TRu, GRASS3.TRv},
};


//0p
static const Vertex PathVertices[] = {
    //Base
    {0.0f, 0.0f, 0.0f, ROAD.TLu, ROAD.TLv},
    {0.0f, 0.0f, 1.0f, ROAD.BLu, ROAD.BLv},
    {1.0f, 0.0f, 1.0f, ROAD.BRu, ROAD.BRv},
    {1.0f, 0.0f, 0.0f, ROAD.TRu, ROAD.TRv},
};

static const int PathIndices[] = {
    0, 1, 2,
    2, 3, 0,
};

//1
static const Vertex House1Vertices[] = {
    //Base
    {0.0f, 0.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv},
    {0.0f, 0.0f, 1.0f, PLASTER2.BRu, PLASTER2.BRv},
    {1.0f, 0.0f, 1.0f, PLASTER2.BRu, PLASTER2.BRv},
    {1.0f, 0.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv},

    //Top
    {0.0f, 0.99f, 0.0f, PLASTER2.TLu, PLASTER2.TLv}, //4
    {0.0f, 0.99f, 1.0f, PLASTER2.TRu, PLASTER2.TRv},
    {1.0f, 0.99f, 1.0f, PLASTER2.TRu, PLASTER2.TRv},
    {1.0f, 0.99f, 0.0f, PLASTER2.TLu, PLASTER2.TLv},

    //Roof Front
    {0.0f, 0.99f, 0.0f, PLASTER2.BLu, PLASTER2.BLv}, //8
    {1.0f, 0.99f, 0.0f, PLASTER2.BRu, PLASTER2.BRv},
    {0.5f, 1.5f, 0.0f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Roof Back
    {0.0f, 0.99f, 1.0f, PLASTER2.BLu, PLASTER2.BLv}, //11
    {1.0f, 0.99f, 1.0f, PLASTER2.BRu, PLASTER2.BRv},
    {0.5f, 1.5f, 1.0f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Roof Left
    {0.5f, 1.5f, -0.05f, ROOF1.TRu, ROOF1.TRv}, //14
    {-0.25f, 0.75f, -0.05f, ROOF1.BRu, ROOF1.BRv},
    {-0.25f, 0.75f, 1.05f, ROOF1.BLu, ROOF1.BLv},
    {0.5f, 1.5f, 1.05f, ROOF1.TLu, ROOF1.TLv},

    //Roof right
    {0.5f, 1.5f, -0.05f, ROOF1.TRu, ROOF1.TRv}, //18
    {1.25f, 0.75f, -0.05f, ROOF1.BRu, ROOF1.BRv},
    {1.25f, 0.75f, 1.05f, ROOF1.BLu, ROOF1.BLv},
    {0.5f, 1.5f, 1.05f, ROOF1.TLu, ROOF1.TLv},

    //Front
    {0.0f, 0.99f, 1.0f, PLASTER2.TLu, PLASTER2.TLv}, //22
    {0.0f, 0.0f, 1.0f, PLASTER2.BLu, PLASTER2.BLv},
    {1.0f, 0.0f, 1.0f, PLASTER2.BRu, PLASTER2.BRv},
    {1.0f, 0.99f, 1.0f, PLASTER2.TRu, PLASTER2.TRv},

    //Back
    {1.0f, 0.99f, 0.0f, PLASTER2.TLu, PLASTER2.TLv}, //26
    {1.0f, 0.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv},
    {0.0f, 0.0f, 0.0f, PLASTER2.BRu, PLASTER2.BRv},
    {0.0f, 0.99f, 0.0f, PLASTER2.TRu, PLASTER2.TRv},
};

static const int House1Indices[] = {
    //Bottom
    0, 1, 2,
    2, 3, 0,

    //Left
    4, 0, 1,
    1, 5, 4,

    //Front
    22, 23, 24,
    24, 25, 22,

    //Right
    6, 2, 3,
    3, 7, 6,

    //Back
    26, 27, 28,
    28, 29, 26,

    //Top
    4, 5, 6,
    6, 7, 4,

    //Roof Front and back
    8, 9, 10,
    11, 12, 13,

    //Roof Left
    14, 15, 16,
    16, 17, 14,

    //Roof right
    18, 19, 20,
    20, 21, 18,
};

//2
static const Vertex House2Vertices[] = {
    //Base
    {0.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {0.0f, 0.0f, 0.6f, STONE1.BRu, STONE1.BRv},
    {1.0f, 0.0f, 0.6f, STONE1.BRu, STONE1.BRv},
    {1.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},

    //Top
    {0.0f, 0.75f, 0.0f, STONE1.TLu, STONE1.TLv},
    {0.0f, 0.75f, 0.6f, STONE1.TRu, STONE1.TRv},
    {1.0f, 0.75f, 0.6f, STONE1.TRu, STONE1.TRv},
    {1.0f, 0.75f, 0.0f, STONE1.TLu, STONE1.TLv},

    //Ground
    {0.0f, 0.0f, 0.6f, ROAD.TLu, ROAD.TLv}, //(8)
    {0.0f, 0.0f, 1.0f, ROAD.BLu, ROAD.BLv},
    {1.0f, 0.0f, 1.0f, ROAD.BRu, ROAD.BRv},
    {1.0f, 0.0f, 0.6f, ROAD.TRu, ROAD.TRv},

    //Pole 1
    {0.0f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv}, //(12)
    {0.0f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {0.1f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {0.1f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv},

    {0.0f, 0.75f, 0.9f, WOOD.TLu, WOOD.TLv}, //(16)
    {0.0f, 0.75f, 1.0f, WOOD.TRu, WOOD.TRv},
    {0.1f, 0.75f, 1.0f, WOOD.TRu, WOOD.TRv},
    {0.1f, 0.75f, 0.9f, WOOD.TLu, WOOD.TLv},

    //Pole 2
    {0.45f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv}, //(20)
    {0.45f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {0.55f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {0.55f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv},

    {0.45f, 0.75f, 0.9f, WOOD.TLu, WOOD.TLv}, //(24)
    {0.45f, 0.75f, 1.0f, WOOD.TRu, WOOD.TRv},
    {0.55f, 0.75f, 1.0f, WOOD.TRu, WOOD.TRv},
    {0.55f, 0.75f, 0.9f, WOOD.TLu, WOOD.TLv},

    //Pole 3
    {0.9f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv}, //(28)
    {0.9f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {1.0f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {1.0f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv},

    {0.9f, 0.75f, 0.9f, WOOD.TLu, WOOD.TLv}, //(32)
    {0.9f, 0.75f, 1.0f, WOOD.TRu, WOOD.TRv},
    {1.0f, 0.75f, 1.0f, WOOD.TRu, WOOD.TRv},
    {1.0f, 0.75f, 0.9f, WOOD.TLu, WOOD.TLv},

    //Upper Level
    {0.0f, 0.75f, 0.0f, PLASTER1.BLu, PLASTER1.BLv}, //(36)
    {0.0f, 0.75f, 1.0f, PLASTER1.BRu, PLASTER1.BRv},
    {1.0f, 0.75f, 1.0f, PLASTER1.BRu, PLASTER1.BRv},
    {1.0f, 0.75f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},

    {0.0f, 1.5f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //(40)
    {0.0f, 1.5f, 1.0f, PLASTER1.TRu, PLASTER1.TRv},
    {1.0f, 1.5f, 1.0f, PLASTER1.TRu, PLASTER1.TRv},
    {1.0f, 1.5f, 0.0f, PLASTER1.TLu, PLASTER1.TLv},

    //Roof Left
    {0.0f, 1.5f, 0.0f, PLASTER2.BLu, PLASTER2.BLv}, //(44)
    {0.0f, 1.5f, 1.0f, PLASTER2.BRu, PLASTER2.BRv},
    {0.0f, 2.0f, 0.5f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Roof Right
    {1.0f, 1.5f, 0.0f, PLASTER2.BLu, PLASTER2.BLv}, //(47)
    {1.0f, 1.5f, 1.0f, PLASTER2.BRu, PLASTER2.BRv},
    {1.0f, 2.0f, 0.5f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Roof Front
    {-0.05f, 2.0f, 0.5f, ROOF2.TLu, ROOF2.TLv}, //(50)
    {-0.05f, 1.5f, 1.03f, ROOF2.BLu, ROOF2.BLv},
    {1.05f, 1.5f, 1.03f, ROOF2.BRu, ROOF2.BRv},
    {1.05f, 2.0f, 0.5f, ROOF2.TRu, ROOF2.TRv},

    //Roof Back
    {1.05f, 2.0f, 0.5f, ROOF2.TLu, ROOF2.TLv}, //(54)
    {1.05f, 1.5f, -0.03f, ROOF2.BLu, ROOF2.BLv},
    {-0.05f, 1.5f, -0.03f, ROOF2.BRu, ROOF2.BRv},
    {-0.05f, 2.0f, 0.5f, ROOF2.TRu, ROOF2.TRv},

    //Pole 1 (Texture)
    {0.0f, 0.75f, 0.9f, WOOD.TLu, WOOD.TLv}, //(58)
    {0.0f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv},
    {0.1f, 0.0f, 0.9f, WOOD.BRu, WOOD.BRv},
    {0.1f, 0.75f, 0.9f, WOOD.TRu, WOOD.TRv},

    {0.0f, 0.75f, 1.0f, WOOD.TLu, WOOD.TLv}, //(62)
    {0.0f, 0.0f, 1.0f, WOOD.BLu, WOOD.BLv},
    {0.1f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {0.1f, 0.75f, 1.0f, WOOD.TRu, WOOD.TRv},

    //Pole 2 (Texture)
    {0.45f, 0.75f, 0.9f, WOOD.TLu, WOOD.TLv}, //(66)
    {0.45f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv},
    {0.55f, 0.0f, 0.9f, WOOD.BRu, WOOD.BRv},
    {0.55f, 0.75f, 0.9f, WOOD.TRu, WOOD.TRv},

    {0.45f, 0.75f, 1.0f, WOOD.TLu, WOOD.TLv}, //(70)
    {0.45f, 0.0f, 1.0f, WOOD.BLu, WOOD.BLv},
    {0.55f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {0.55f, 0.75f, 1.0f, WOOD.TRu, WOOD.TRv},

    //Pole 3 (Texture)
    {0.9f, 0.75f, 0.9f, WOOD.TLu, WOOD.TLv}, //(74)
    {0.9f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv},
    {1.0f, 0.0f, 0.9f, WOOD.BRu, WOOD.BRv},
    {1.0f, 0.75f, 0.9f, WOOD.TRu, WOOD.TRv},

    {0.9f, 0.75f, 1.0f, WOOD.TLu, WOOD.TLv}, //(78)
    {0.9f, 0.0f, 1.0f, WOOD.BLu, WOOD.BLv},
    {1.0f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {1.0f, 0.75f, 1.0f, WOOD.TRu, WOOD.TRv},

    //Bottom (Texture)
    {0.0f, 0.75f, 0.0f, STONE1.TLu, STONE1.TLv}, //(82)
    {0.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 0.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 0.75f, 0.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 0.75f, 0.6f, STONE2.TLu, STONE2.TLv}, //(86)
    {0.0f, 0.0f, 0.6f, STONE2.BLu, STONE2.BLv},
    {1.0f, 0.0f, 0.6f, STONE2.BRu, STONE2.BRv},
    {1.0f, 0.75f, 0.6f, STONE2.TRu, STONE2.TRv},

    //Top(Texture)
    {0.0f, 0.75f, 0.0f, PLASTER1.BLu, PLASTER1.BLv}, //(90)
    {0.0f, 1.5f, 0.0f, PLASTER1.TLu, PLASTER1.TLv},
    {1.0f, 1.5f, 0.0f, PLASTER1.TRu, PLASTER1.TRv},
    {1.0f, 0.75f, 0.0f, PLASTER1.BRu, PLASTER1.BRv},

    {0.0f, 0.75f, 1.0f, PLASTER1.BLu, PLASTER1.BLv}, //(94)
    {0.0f, 1.5f, 1.0f, PLASTER1.TLu, PLASTER1.TLv},
    {1.0f, 1.5f, 1.0f, PLASTER1.TRu, PLASTER1.TRv},
    {1.0f, 0.75f, 1.0f, PLASTER1.BRu, PLASTER1.BRv},
};

static const int House2Indices[] = {
    //Bottom
    0, 1, 2,
    2, 3, 0,

    //Left
    4, 0, 1,
    1, 5, 4,

    //Front
    82, 83, 84,
    84, 85, 82,

    //Right
    6, 2, 3,
    3, 7, 6,

    //Back
    86, 87, 88,
    88, 89, 86,

    //Ground
    8, 9, 10,
    10, 11, 8,

    //Pole 1
    12, 13, 14, //Bottom
    14, 15, 12,
    16, 12, 13, //Left
    13, 17, 16,
    58, 59, 60, //Front
    60, 61, 58,
    18, 14, 15, //Right
    15, 19, 18,
    62, 63, 64, //Back
    64, 65, 62,

    //Pole 2
    20, 21, 22, //Bottom
    22, 23, 20,
    24, 20, 21, //Left
    21, 25, 24,
    66, 67, 68, //Front
    68, 69, 66,
    26, 22, 23, //Right
    23, 27, 26,
    70, 71, 72, //Back
    72, 73, 70,

    //Pole 3
    28, 29, 30, //Bottom
    30, 31, 28,
    32, 28, 29, //Left
    29, 33, 32,
    74, 75, 76, //Front
    76, 77, 74,
    34, 30, 31, //Right
    31, 35, 34,
    78, 79, 80, //Back
    80, 81, 78,

    //Upper Level
    36, 37, 38, //Bottom
    38, 39, 36,
    40, 41, 42, //Top
    42, 43, 40,
    40, 36, 37, //Left
    37, 41, 40,
    94, 95, 96, //Front
    96, 97, 94,
    42, 38, 39, //Right
    39, 43, 42,
    90, 91, 92, //Back
    92, 93, 90,

    //Roof Right
    44, 45, 46,

    //Roof Left
    47, 48, 49,

    //Roof Front
    50, 51, 52,
    52, 53, 50,

    //Roof Back
    54, 55, 56,
    56, 57, 54,
};

//5
static const Vertex ChurchVertices[] = {
    //Base
    {0.0f, 0.0f, 1.0f, STONE1.BLu, STONE1.BLv},
    {0.0f, 0.0f, 5.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 0.0f, 5.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 0.0f, 1.0f, STONE1.BLu, STONE1.BLv},

    //Top
    {0.0f, 1.0f, 1.0f, STONE1.TLu, STONE1.TLv}, //4
    {0.0f, 1.0f, 5.0f, STONE1.TRu, STONE1.TRv},
    {3.0f, 1.0f, 5.0f, STONE1.TRu, STONE1.TRv},
    {3.0f, 1.0f, 1.0f, STONE1.TLu, STONE1.TLv},

    //Tower Bottom
    {1.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv}, //8
    {1.0f, 0.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 0.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},

    //Tower Top
    {1.0f, 3.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //12
    {1.0f, 3.0f, 1.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 3.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 3.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    //Roof
    {0.0f, 1.0f, 1.0f, STONE1.BLu, STONE1.BLv}, //16
    {1.5f, 1.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {1.5f, 2.5f, 1.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 1.0f, 5.0f, STONE1.BLu, STONE1.BLv}, //19
    {1.5f, 1.0f, 5.0f, STONE1.BRu, STONE1.BRv},
    {1.5f, 2.5f, 5.0f, STONE1.TRu, STONE1.TRv},

    {1.5f, 2.5f, 1.0f, ROOF1.TRu, ROOF1.TRv}, //22
    {0.0f, 1.0f, 1.0f, ROOF1.BRu, ROOF1.BRv},
    {0.0f, 1.0f, 5.0f, ROOF1.BLu, ROOF1.BLv},
    {1.5f, 2.5f, 5.0f, ROOF1.TLu, ROOF1.TLv},

    {1.5f, 1.0f, 1.0f, STONE1.BRu, STONE1.BRv}, //26
    {1.5f, 2.5f, 1.0f, STONE1.TRu, STONE1.TRv},
    {3.0f, 1.0f, 1.0f, STONE1.BLu, STONE1.BLv},

    {1.5f, 1.0f, 5.0f, STONE1.BRu, STONE1.BRv}, //29
    {1.5f, 2.5f, 5.0f, STONE1.TRu, STONE1.TRv},
    {3.0f, 1.0f, 5.0f, STONE1.BLu, STONE1.BLv},

    {1.5f, 2.5f, 5.0f, ROOF1.TRu, ROOF1.TRv}, //32
    {3.0f, 1.0f, 5.0f, ROOF1.BRu, ROOF1.BRv},
    {3.0f, 1.0f, 1.0f, ROOF1.BLu, ROOF1.BLv},
    {1.5f, 2.5f, 1.0f, ROOF1.TLu, ROOF1.TLv},

    //Crenellation
    //Merlon 1
    {1.0f, 3.0f, 0.0f, 0.72f, 0.140f}, //36
    {1.0f, 3.0f, 0.2f, 0.72f, 0.141f},
    {1.2f, 3.0f, 0.2f, 0.68f, 0.141f},
    {1.2f, 3.0f, 0.0f, 0.68f, 0.140f},

    {1.0f, 3.25f, 0.0f, 0.72f, 0.140f}, //40
    {1.0f, 3.25f, 0.2f, 0.72f, 0.141f},
    {1.2f, 3.25f, 0.2f, 0.68f, 0.141f},
    {1.2f, 3.25f, 0.0f, 0.68f, 0.140f},

    //Merlon 2
    {1.4f, 3.0f, 0.0f, 0.72f, 0.140f}, //44
    {1.4f, 3.0f, 0.2f, 0.72f, 0.141f},
    {1.6f, 3.0f, 0.2f, 0.68f, 0.141f},
    {1.6f, 3.0f, 0.0f, 0.68f, 0.140f},

    {1.4f, 3.25f, 0.0f, 0.72f, 0.140f}, //48
    {1.4f, 3.25f, 0.2f, 0.72f, 0.141f},
    {1.6f, 3.25f, 0.2f, 0.68f, 0.141f},
    {1.6f, 3.25f, 0.0f, 0.68f, 0.140f},

    //Merlon 3
    {1.8f, 3.0f, 0.0f, 0.72f, 0.140f}, //52
    {1.8f, 3.0f, 0.2f, 0.72f, 0.141f},
    {2.0f, 3.0f, 0.2f, 0.68f, 0.141f},
    {2.0f, 3.0f, 0.0f, 0.68f, 0.140f},

    {1.8f, 3.25f, 0.0f, 0.72f, 0.140f}, //56
    {1.8f, 3.25f, 0.2f, 0.72f, 0.141f},
    {2.0f, 3.25f, 0.2f, 0.68f, 0.141f},
    {2.0f, 3.25f, 0.0f, 0.68f, 0.140f},

    //Merlon 4
    {1.8f, 3.0f, 0.4f, 0.72f, 0.140f}, //60
    {1.8f, 3.0f, 0.6f, 0.72f, 0.141f},
    {2.0f, 3.0f, 0.6f, 0.68f, 0.141f},
    {2.0f, 3.0f, 0.4f, 0.68f, 0.140f},

    {1.8f, 3.25f, 0.4f, 0.72f, 0.140f}, //64
    {1.8f, 3.25f, 0.6f, 0.72f, 0.141f},
    {2.0f, 3.25f, 0.6f, 0.68f, 0.141f},
    {2.0f, 3.25f, 0.4f, 0.68f, 0.140f},

    //Merlon 5
    {1.8f, 3.0f, 0.8f, 0.72f, 0.140f}, //68
    {1.8f, 3.0f, 1.0f, 0.72f, 0.141f},
    {2.0f, 3.0f, 1.0f, 0.68f, 0.141f},
    {2.0f, 3.0f, 0.8f, 0.68f, 0.140f},

    {1.8f, 3.25f, 0.8f, 0.72f, 0.140f}, //72
    {1.8f, 3.25f, 1.0f, 0.72f, 0.141f},
    {2.0f, 3.25f, 1.0f, 0.68f, 0.141f},
    {2.0f, 3.25f, 0.8f, 0.68f, 0.140f},

    //Merlon 6
    {1.4f, 3.0f, 0.8f, 0.72f, 0.140f}, //76
    {1.4f, 3.0f, 1.0f, 0.72f, 0.141f},
    {1.6f, 3.0f, 1.0f, 0.68f, 0.141f},
    {1.6f, 3.0f, 0.8f, 0.68f, 0.140f},

    {1.4f, 3.25f, 0.8f, 0.72f, 0.140f}, //80
    {1.4f, 3.25f, 1.0f, 0.72f, 0.141f},
    {1.6f, 3.25f, 1.0f, 0.68f, 0.141f},
    {1.6f, 3.25f, 0.8f, 0.68f, 0.140f},

    //Merlon 7
    {1.0f, 3.0f, 0.8f, 0.72f, 0.140f}, //84
    {1.0f, 3.0f, 1.0f, 0.72f, 0.141f},
    {1.2f, 3.0f, 1.0f, 0.68f, 0.141f},
    {1.2f, 3.0f, 0.8f, 0.68f, 0.140f},

    {1.0f, 3.25f, 0.8f, 0.72f, 0.140f}, //88
    {1.0f, 3.25f, 1.0f, 0.72f, 0.141f},
    {1.2f, 3.25f, 1.0f, 0.68f, 0.141f},
    {1.2f, 3.25f, 0.8f, 0.68f, 0.140f},

    //Merlon 8
    {1.0f, 3.0f, 0.4f, 0.72f, 0.140f}, //92
    {1.0f, 3.0f, 0.6f, 0.72f, 0.141f},
    {1.2f, 3.0f, 0.6f, 0.68f, 0.141f},
    {1.2f, 3.0f, 0.4f, 0.68f, 0.140f},

    {1.0f, 3.25f, 0.4f, 0.72f, 0.140f}, //96
    {1.0f, 3.25f, 0.6f, 0.72f, 0.141f},
    {1.2f, 3.25f, 0.6f, 0.68f, 0.141f},
    {1.2f, 3.25f, 0.4f, 0.68f, 0.140f},

    //Ground
    {0.0f, 0.0f, 0.0f, GRASS1.TLu, GRASS1.TLv}, //100
    {0.0f, 0.0f, 1.0f, GRASS1.BLu, GRASS1.BLv},
    {3.0f, 0.0f, 1.0f, GRASS1.BRu, GRASS1.BRv},
    {3.0f, 0.0f, 0.0f, GRASS1.TRu, GRASS1.TRv},

    //Front (Textures)
    {0.0f, 1.0f, 1.0f, STONE1.TLu, STONE1.TLv}, //104
    {0.0f, 0.0f, 1.0f, STONE1.BLu, STONE1.BLv},
    {1.5f, 0.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {1.5f, 1.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    {1.5f, 1.0f, 1.0f, STONE1.TLu, STONE1.TLv}, //108
    {1.5f, 0.0f, 1.0f, STONE1.BLu, STONE1.BLv},
    {3.0f, 0.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 1.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    //Back (Textures)
    {0.0f, 1.0f, 5.0f, STONE1.TLu, STONE1.TLv}, //112
    {0.0f, 0.0f, 5.0f, STONE1.BLu, STONE1.BLv},
    {1.5f, 0.0f, 5.0f, STONE1.BRu, STONE1.BRv},
    {1.5f, 1.0f, 5.0f, STONE1.TRu, STONE1.TRv},

    {1.5f, 1.0f, 5.0f, STONE1.TLu, STONE1.TLv}, //116
    {1.5f, 0.0f, 5.0f, STONE1.BLu, STONE1.BLv},
    {3.0f, 0.0f, 5.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 1.0f, 5.0f, STONE1.TRu, STONE1.TRv},

    //Left (Textures)
    {0.0f, 1.0f, 1.0f, STONE3.TLu, STONE3.TLv}, //120
    {0.0f, 0.0f, 1.0f, STONE3.BLu, STONE3.BLv},
    {0.0f, 0.0f, 2.0f, STONE3.BRu, STONE3.BRv},
    {0.0f, 1.0f, 2.0f, STONE3.TRu, STONE3.TRv},

    {0.0f, 1.0f, 2.0f, STONE3.TLu, STONE3.TLv}, //124
    {0.0f, 0.0f, 2.0f, STONE3.BLu, STONE3.BLv},
    {0.0f, 0.0f, 3.0f, STONE3.BRu, STONE3.BRv},
    {0.0f, 1.0f, 3.0f, STONE3.TRu, STONE3.TRv},

    {0.0f, 1.0f, 3.0f, STONE3.TLu, STONE3.TLv}, //128
    {0.0f, 0.0f, 3.0f, STONE3.BLu, STONE3.BLv},
    {0.0f, 0.0f, 4.0f, STONE3.BRu, STONE3.BRv},
    {0.0f, 1.0f, 4.0f, STONE3.TRu, STONE3.TRv},

    {0.0f, 1.0f, 4.0f, STONE3.TLu, STONE3.TLv}, //132
    {0.0f, 0.0f, 4.0f, STONE3.BLu, STONE3.BLv},
    {0.0f, 0.0f, 5.0f, STONE3.BRu, STONE3.BRv},
    {0.0f, 1.0f, 5.0f, STONE3.TRu, STONE3.TRv},

    //Right (Textures)
    {3.0f, 1.0f, 1.0f, STONE3.TLu, STONE3.TLv}, //136
    {3.0f, 0.0f, 1.0f, STONE3.BLu, STONE3.BLv},
    {3.0f, 0.0f, 2.0f, STONE3.BRu, STONE3.BRv},
    {3.0f, 1.0f, 2.0f, STONE3.TRu, STONE3.TRv},

    {3.0f, 1.0f, 2.0f, STONE3.TLu, STONE3.TLv}, //140
    {3.0f, 0.0f, 2.0f, STONE3.BLu, STONE3.BLv},
    {3.0f, 0.0f, 3.0f, STONE3.BRu, STONE3.BRv},
    {3.0f, 1.0f, 3.0f, STONE3.TRu, STONE3.TRv},

    {3.0f, 1.0f, 3.0f, STONE3.TLu, STONE3.TLv}, //144
    {3.0f, 0.0f, 3.0f, STONE3.BLu, STONE3.BLv},
    {3.0f, 0.0f, 4.0f, STONE3.BRu, STONE3.BRv},
    {3.0f, 1.0f, 4.0f, STONE3.TRu, STONE3.TRv},

    {3.0f, 1.0f, 4.0f, STONE3.TLu, STONE3.TLv}, //148
    {3.0f, 0.0f, 4.0f, STONE3.BLu, STONE3.BLv},
    {3.0f, 0.0f, 5.0f, STONE3.BRu, STONE3.BRv},
    {3.0f, 1.0f, 5.0f, STONE3.TRu, STONE3.TRv},

    //Tower Front (Textures)
    {2.0f, 1.0f, 0.0f, STONE2.TLu, STONE2.TLv}, //152
    {2.0f, 0.0f, 0.0f, STONE2.BLu, STONE2.BLv},
    {1.0f, 0.0f, 0.0f, STONE2.BRu, STONE2.BRv},
    {1.0f, 1.0f, 0.0f, STONE2.TRu, STONE2.TRv},

    {2.0f, 2.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //156
    {2.0f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 1.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 2.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 3.0f, 0.0f, STONE3.TLu, STONE3.TLv}, //160
    {2.0f, 2.0f, 0.0f, STONE3.BLu, STONE3.BLv},
    {1.0f, 2.0f, 0.0f, STONE3.BRu, STONE3.BRv},
    {1.0f, 3.0f, 0.0f, STONE3.TRu, STONE3.TRv},

    //Left
    {1.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //164
    {1.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 0.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 1.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    {1.0f, 2.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //168
    {1.0f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 1.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 2.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    {1.0f, 3.0f, 0.0f, STONE3.TLu, STONE3.TLv}, //172
    {1.0f, 2.0f, 0.0f, STONE3.BLu, STONE3.BLv},
    {1.0f, 2.0f, 1.0f, STONE3.BRu, STONE3.BRv},
    {1.0f, 3.0f, 1.0f, STONE3.TRu, STONE3.TRv},

    //Right
    {2.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //176
    {2.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 0.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 1.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 2.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //180
    {2.0f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 1.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 2.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 3.0f, 0.0f, STONE3.TLu, STONE3.TLv}, //184
    {2.0f, 2.0f, 0.0f, STONE3.BLu, STONE3.BLv},
    {2.0f, 2.0f, 1.0f, STONE3.BRu, STONE3.BRv},
    {2.0f, 3.0f, 1.0f, STONE3.TRu, STONE3.TRv},

    //Back (Textures)
    {2.0f, 2.0f, 1.0f, STONE1.TLu, STONE1.TLv}, //188
    {2.0f, 1.0f, 1.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 1.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 2.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 3.0f, 1.0f, STONE3.TLu, STONE3.TLv}, //192
    {2.0f, 2.0f, 1.0f, STONE3.BLu, STONE3.BLv},
    {1.0f, 2.0f, 1.0f, STONE3.BRu, STONE3.BRv},
    {1.0f, 3.0f, 1.0f, STONE3.TRu, STONE3.TRv},
};

static const int ChurchIndices[] = {
    //Bottom
    0, 1, 2,
    2, 3, 0,

    //Left
    120, 121, 122,
    122, 123, 120,
    124, 125, 126,
    126, 127, 124,
    128, 129, 130,
    130, 131, 128,
    132, 133, 134,
    134, 135, 132,

    //Front
    104, 105, 106,
    106, 107, 104,
    108, 109, 110,
    110, 111, 108,

    //Right
    136, 137, 138,
    138, 139, 136,
    140, 141, 142,
    142, 143, 140,
    144, 145, 146,
    146, 147, 144,
    148, 149, 150,
    150, 151, 148,

    //Back
    112, 113, 114,
    114, 115, 112,
    116, 117, 118,
    118, 119, 116,

    //Tower
    //Bottom
    8, 9, 10,
    10, 11, 8,

    //Left
    164, 165, 166,
    166, 167, 164,
    168, 169, 170,
    170, 171, 168,
    172, 173, 174,
    174, 175, 172,

    //Front
    152, 153, 154,
    154, 155, 152,
    156, 157, 158,
    158, 159, 156,
    160, 161, 162,
    162, 163, 160,

    //Right
    176, 177, 178,
    178, 179, 176,
    180, 181, 182,
    182, 183, 180,
    184, 185, 186,
    186, 187, 184,

    //Top
    12, 13, 14,
    14, 15, 12,

    //Back
    188, 189, 190,
    190, 191, 188,
    192, 193, 194,
    194, 195, 192,

    //Roof
    16, 17, 18,
    19, 20, 21,

    22, 23, 24,
    24, 25, 22,

    26, 28, 27,
    29, 31, 30,

    32, 33, 34,
    34, 35, 32,

    //Merlon 1
    36, 37, 38, //Bottom
    38, 39, 36,
    40, 36, 37, //Left
    37, 41, 40,
    41, 37, 38, //Front
    38, 42, 41,
    42, 38, 39, //Right
    39, 43, 42,
    43, 39, 36, //Back
    36, 40, 43,
    40, 41, 42, //Top
    42, 43, 40,

    //Merlon 2
    44, 45, 46, //Bottom
    46, 47, 44,
    48, 44, 45, //Left
    45, 49, 48,
    49, 45, 46, //Front
    46, 50, 49,
    50, 46, 47, //Right
    47, 51, 50,
    51, 47, 44, //Back
    44, 48, 51,
    48, 49, 50, //Top
    50, 51, 48,

    //Merlon 3
    52, 53, 54, //Bottom
    54, 55, 52,
    56, 52, 53, //Left
    53, 57, 56,
    57, 53, 54, //Front
    54, 58, 57,
    58, 54, 55, //Right
    55, 59, 58,
    59, 55, 52, //Back
    52, 56, 59,
    56, 57, 58, //Top
    58, 59, 56,

    //Merlon 4
    60, 61, 62, //Bottom
    62, 63, 60,
    64, 60, 61, //Left
    61, 65, 64,
    65, 61, 62, //Front
    62, 66, 65,
    66, 62, 63, //Right
    63, 67, 66,
    67, 63, 60, //Back
    60, 64, 67,
    64, 65, 66, //Top
    66, 67, 64,

    //Merlon 5
    68, 69, 70, //Bottom
    70, 71, 68,
    72, 68, 69, //Left
    69, 73, 72,
    73, 69, 70, //Front
    70, 74, 73,
    74, 70, 71, //Right
    71, 75, 74,
    75, 71, 68, //Back
    68, 72, 75,
    72, 73, 74, //Top
    74, 75, 72,

    //Merlon 6
    76, 77, 78, //Bottom
    78, 79, 76,
    80, 76, 77, //Left
    77, 81, 80,
    81, 77, 78, //Front
    78, 82, 81,
    82, 78, 79, //Right
    79, 83, 82,
    83, 79, 76, //Back
    76, 80, 83,
    80, 81, 82, //Top
    82, 83, 80,

    //Merlon 7
    84, 85, 86, //Bottom
    86, 87, 84,
    88, 84, 85, //Left
    85, 89, 88,
    89, 85, 86, //Front
    86, 90, 89,
    90, 86, 87, //Right
    87, 91, 90,
    91, 87, 84, //Back
    84, 88, 91, 
    88, 89, 90, //Top
    90, 91, 88,

    //Merlon 8
    92, 93, 94, //Bottom
    94, 95, 92,
    96, 92, 93, //Left
    93, 97, 96,
    97, 93, 94, //Front
    94, 98, 97,
    98, 94, 95, //Right
    95, 99, 98,
    99, 95, 92, //Back
    92, 96, 99,
    96, 97, 98, //Top
    98, 99, 96,

    //Ground
    100, 101, 102,
    102, 103, 100,
};

//6
static const Vertex LHouseVertices[] = {
    //Pole 1
    {0.0f, 0.0f, 1.8f, WOOD.BLu, WOOD.BLv},
    {0.0f, 0.0f, 1.9f, WOOD.BRu, WOOD.BRv},
    {0.1f, 0.0f, 1.9f, WOOD.BRu, WOOD.BRv},
    {0.1f, 0.0f, 1.8f, WOOD.BLu, WOOD.BLv},

    {0.0f, 1.05f, 1.8f, WOOD.TLu, WOOD.TLv + 0.015f}, //4
    {0.0f, 1.05f, 1.9f, WOOD.TRu, WOOD.TRv + 0.015f},
    {0.1f, 1.05f, 1.9f, WOOD.TRu, WOOD.TRv + 0.015f},
    {0.1f, 1.05f, 1.8f, WOOD.TLu, WOOD.TLv + 0.015f},

    //Pole 2
    {0.95f, 0.0f, 1.8f, WOOD.BLu, WOOD.BLv}, //8
    {0.95f, 0.0f, 1.9f, WOOD.BRu, WOOD.BRv},
    {1.05f, 0.0f, 1.9f, WOOD.BRu, WOOD.BRv},
    {1.05f, 0.0f, 1.8f, WOOD.BLu, WOOD.BLv},

    {0.95f, 1.05f, 1.8f, WOOD.TLu, WOOD.TLv + 0.015f}, //12
    {0.95f, 1.05f, 1.9f, WOOD.TRu, WOOD.TRv + 0.015f},
    {1.05f, 1.05f, 1.9f, WOOD.TRu, WOOD.TRv + 0.015f},
    {1.05f, 1.05f, 1.8f, WOOD.TLu, WOOD.TLv + 0.015f},

    //Pole 3
    {1.9f, 0.0f, 1.8f, WOOD.BLu, WOOD.BLv}, //16
    {1.9f, 0.0f, 1.9f, WOOD.BRu, WOOD.BRv},
    {2.0f, 0.0f, 1.9f, WOOD.BRu, WOOD.BRv},
    {2.0f, 0.0f, 1.8f, WOOD.BLu, WOOD.BLv},

    {1.9f, 1.05f, 1.8f, WOOD.TLu, WOOD.TLv + 0.015f}, //20
    {1.9f, 1.05f, 1.9f, WOOD.TRu, WOOD.TRv + 0.015f},
    {2.0f, 1.05f, 1.9f, WOOD.TRu, WOOD.TRv + 0.015f},
    {2.0f, 1.05f, 1.8f, WOOD.TLu, WOOD.TLv + 0.015f},

    //Main building bottom
    {0.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv}, //24
    {0.0f, 0.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 0.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    
    //top
    {0.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //28
    {0.0f, 1.0f, 1.0f, STONE1.TRu, STONE1.TRv},
    {3.0f, 1.0f, 1.0f, STONE1.TRu, STONE1.TRv},
    {3.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv},

    //Side building bottom
    {2.0f, 0.0f, 1.0f, STONE1.BLu, STONE1.BLv}, //32
    {2.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 0.0f, 1.0f, STONE1.BLu, STONE1.BLv},

    //top
    {2.0f, 1.0f, 1.0f, STONE1.TLu, STONE1.TLv}, //36
    {2.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},
    {3.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},
    {3.0f, 1.0f, 1.0f, STONE1.TLu, STONE1.TLv},

    //Main roof Left
    {0.0f, 2.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv}, //40
    {0.0f, 2.0f, 1.0f, PLASTER2.BRu, PLASTER2.BRv},
    {0.0f, 2.99f, 0.5f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Main roof right
    {3.0f, 2.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv}, //43
    {3.0f, 2.0f, 1.0f, PLASTER2.BRu, PLASTER2.BRv},
    {3.0f, 2.99f, 0.5f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Main roof back
    {3.05f, 3.0f, 0.5f, ROOF1.TRu, ROOF1.TRv}, //46
    {3.05f, 2.0f, 0.0f, ROOF1.BRu, ROOF1.BRv},
    {-0.05f, 2.0f, 0.0f, ROOF1.BLu, ROOF1.BLv},
    {-0.05f, 3.0f, 0.5f, ROOF1.TLu, ROOF1.TLv},

    //Main roof front
    {-0.05f, 3.0f, 0.5f, ROOF1.TRu, ROOF1.TRv}, //50
    {-0.05f, 2.0f, 1.05f, ROOF1.BRu, ROOF1.BRv},
    {3.05f, 2.0f, 1.05f, ROOF1.BLu, ROOF1.BLv},
    {3.05f, 3.0f, 0.5f, ROOF1.TLu, ROOF1.TLv},

    //Side roof front
    {2.0f, 2.0f, 2.0f, PLASTER2.BLu, PLASTER2.BLv}, //54
    {3.0f, 2.0f, 2.0f, PLASTER2.BRu, PLASTER2.BRv},
    {2.5f, 2.99f, 2.0f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Side roof left
    {2.5f, 3.0f, 0.5f, ROOF1.TRu, ROOF1.TRv}, //57
    {2.0f, 2.0f, 0.5f, ROOF1.BRu, ROOF1.BRv},
    {2.0f, 2.0f, 2.25f, ROOF1.BLu, ROOF1.BLv},
    {2.5f, 3.0f, 2.25f, ROOF1.TLu, ROOF1.TLv},

    //Side roof right
    {2.5f, 3.0f, 2.25f, ROOF1.TRu, ROOF1.TRv}, //61
    {3.0f, 2.0f, 2.25f, ROOF1.BRu, ROOF1.BRv},
    {3.0f, 2.0f, 0.5f, ROOF1.BLu, ROOF1.BLv},
    {2.5f, 3.0f, 0.5f, ROOF1.TLu, ROOF1.TLv},

    //Lil roof
    {0.0f, 1.45f, 1.0f, ROOF1.TLu, ROOF1.TLv}, //65
    {0.0f, 1.0f, 2.0f, ROOF1.BLu, ROOF1.BLv},
    {2.0f, 1.0f, 2.0f, ROOF1.BRu, ROOF1.BRv},
    {2.0f, 1.45f, 1.0f, ROOF1.TRu, ROOF1.TRv},

    //Roof friends
    {0.0f, 1.089f, 1.8f, WOOD.BLu, WOOD.BLv - 0.015f}, //69
    {0.0f, 1.04f, 1.8f, WOOD.BLu, WOOD.BLv},
    {0.0f, 1.04f, 1.9f, WOOD.BRu, WOOD.BRv},

    {0.1f, 1.09f, 1.8f, WOOD.BLu, WOOD.BLv - 0.015f}, //72
    {0.1f, 1.04f, 1.8f, WOOD.BLu, WOOD.BLv},
    {0.1f, 1.04f, 1.9f, WOOD.BRu, WOOD.BRv},

    {0.95f, 1.089f, 1.8f, WOOD.BLu, WOOD.BLv - 0.015f}, //75
    {0.95f, 1.04f, 1.8f, WOOD.BLu, WOOD.BLv},
    {0.95f, 1.04f, 1.9f, WOOD.BRu, WOOD.BRv},

    {1.5f, 1.089f, 1.8f, WOOD.BLu, WOOD.BLv - 0.015f}, //78
    {1.5f, 1.04f, 1.8f, WOOD.BLu, WOOD.BLv},
    {1.5f, 1.04f, 1.9f, WOOD.BRu, WOOD.BRv},

    {2.9f, 1.089f, 1.8f, WOOD.BLu, WOOD.BLv - 0.015f}, //81
    {2.9f, 1.04f, 1.8f, WOOD.BLu, WOOD.BLv},
    {2.9f, 1.04f, 1.9f, WOOD.BRu, WOOD.BRv},

    //Main building bottom (2nd story)
    {0.0f, 1.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv}, //84
    {0.0f, 1.0f, 1.0f, PLASTER2.BRu, PLASTER2.BRv},
    {3.0f, 1.0f, 1.0f, PLASTER2.BRu, PLASTER2.BRv},
    {3.0f, 1.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv},
    
    //top (2nd story)
    {0.0f, 2.0f, 0.0f, PLASTER2.TLu, PLASTER2.TLv}, //88
    {0.0f, 2.0f, 1.0f, PLASTER2.TRu, PLASTER2.TRv},
    {3.0f, 2.0f, 1.0f, PLASTER2.TRu, PLASTER2.TRv},
    {3.0f, 2.0f, 0.0f, PLASTER2.TLu, PLASTER2.TLv},

    //Side building bottom (2nd story)
    {2.0f, 1.0f, 1.0f, PLASTER2.BLu, PLASTER2.BLv}, //92
    {2.0f, 1.0f, 2.0f, PLASTER2.BRu, PLASTER2.BRv},
    {3.0f, 1.0f, 2.0f, PLASTER2.BRu, PLASTER2.BRv},
    {3.0f, 1.0f, 1.0f, PLASTER2.BLu, PLASTER2.BLv},

    //top (2nd story)
    {2.0f, 2.0f, 1.0f, PLASTER2.TLu, PLASTER2.TLv}, //96
    {2.0f, 2.0f, 2.0f, PLASTER2.TRu, PLASTER2.TRv},
    {3.0f, 2.0f, 2.0f, PLASTER2.TRu, PLASTER2.TRv},
    {3.0f, 2.0f, 1.0f, PLASTER2.TLu, PLASTER2.TLv},

    //Ground
    {0.0f, 0.001f, 1.0f, GRASS1.TLu, GRASS1.TLv}, //100
    {0.0f, 0.001f, 2.0f, GRASS1.BLu, GRASS1.BLv},
    {2.0f, 0.001f, 2.0f, GRASS1.BRu, GRASS1.BRv},
    {2.0f, 0.001f, 1.0f, GRASS1.TRu, GRASS1.TRv},

    //Pole 1 (Texture)
    {0.0f, 1.05f, 1.8f, WOOD.TLu, WOOD.TLv}, //(104)
    {0.0f, 0.0f, 1.8f, WOOD.BLu, WOOD.BLv},
    {0.1f, 0.0f, 1.8f, WOOD.BRu, WOOD.BRv},
    {0.1f, 1.05f, 1.8f, WOOD.TRu, WOOD.TRv},

    {0.0f, 1.05f, 1.9f, WOOD.TLu, WOOD.TLv}, //(108)
    {0.0f, 0.0f, 1.9f, WOOD.BLu, WOOD.BLv},
    {0.1f, 0.0f, 1.9f, WOOD.BRu, WOOD.BRv},
    {0.1f, 1.05f, 1.9f, WOOD.TRu, WOOD.TRv},

    //Pole 2 (Texture)
    {0.95f, 1.05f, 1.8f, WOOD.TLu, WOOD.TLv}, //(112)
    {0.95f, 0.0f, 1.8f, WOOD.BLu, WOOD.BLv},
    {1.05f, 0.0f, 1.8f, WOOD.BRu, WOOD.BRv},
    {1.05f, 1.05f, 1.8f, WOOD.TRu, WOOD.TRv},

    {0.95f, 1.05f, 1.9f, WOOD.TLu, WOOD.TLv}, //(116)
    {0.95f, 0.0f, 1.9f, WOOD.BLu, WOOD.BLv},
    {1.05f, 0.0f, 1.9f, WOOD.BRu, WOOD.BRv},
    {1.05f, 1.05f, 1.9f, WOOD.TRu, WOOD.TRv},

    //Pole 3 (Texture)
    {1.9f, 1.05f, 1.8f, WOOD.TLu, WOOD.TLv}, //(120)
    {1.9f, 0.0f, 1.8f, WOOD.BLu, WOOD.BLv},
    {2.0f, 0.0f, 1.8f, WOOD.BRu, WOOD.BRv},
    {2.0f, 1.05f, 1.8f, WOOD.TRu, WOOD.TRv},

    {1.9f, 1.05f, 1.9f, WOOD.TLu, WOOD.TLv}, //(124)
    {1.9f, 0.0f, 1.9f, WOOD.BLu, WOOD.BLv},
    {2.0f, 0.0f, 1.9f, WOOD.BRu, WOOD.BRv},
    {2.0f, 1.05f, 1.9f, WOOD.TRu, WOOD.TRv},

    //Side building bottom floor front (Texture)
    {2.0f, 1.0f, 2.0f, STONE2.TLu, STONE2.TLv}, //128
    {2.0f, 0.0f, 2.0f, STONE2.BLu, STONE2.BLv},
    {3.0f, 0.0f, 2.0f, STONE2.BRu, STONE2.BRv},
    {3.0f, 1.0f, 2.0f, STONE2.TRu, STONE2.TRv},

    //Side building upper floor front (Texture)
    {2.0f, 2.0f, 2.0f, PLASTER1.TLu, PLASTER1.TLv}, //132
    {2.0f, 1.0f, 2.0f, PLASTER1.BLu, PLASTER1.BLv},
    {3.0f, 1.0f, 2.0f, PLASTER1.BRu, PLASTER1.BRv},
    {3.0f, 2.0f, 2.0f, PLASTER1.TRu, PLASTER1.TRv},

    //Main building bottom front (Texture)
    {0.0f, 1.0f, 1.0f, STONE3.TLu, STONE3.TLv}, //136
    {0.0f, 0.0f, 1.0f, STONE3.BLu, STONE3.BLv},
    {1.0f, 0.0f, 1.0f, STONE3.BRu, STONE3.BRv},
    {1.0f, 1.0f, 1.0f, STONE3.TRu, STONE3.TRv},

    {1.0f, 1.0f, 1.0f, STONE3.TLu, STONE3.TLv}, //140
    {1.0f, 0.0f, 1.0f, STONE3.BLu, STONE3.BLv},
    {2.0f, 0.0f, 1.0f, STONE3.BRu, STONE3.BRv},
    {2.0f, 1.0f, 1.0f, STONE3.TRu, STONE3.TRv},

    //Main building bottom back (Texture)
    {0.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //144
    {0.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 0.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 1.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    {1.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //148
    {1.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 0.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 1.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //152
    {2.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {3.0f, 0.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 1.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    //Main building upper floor front (Texture)
    {0.0f, 2.0f, 1.0f, PLASTER1.TLu, PLASTER1.TLv}, //156
    {0.0f, 1.0f, 1.0f, PLASTER1.BLu, PLASTER1.BLv},
    {1.0f, 1.0f, 1.0f, PLASTER1.BRu, PLASTER1.BRv},
    {1.0f, 2.0f, 1.0f, PLASTER1.TRu, PLASTER1.TRv},

    {1.0f, 2.0f, 1.0f, PLASTER1.TLu, PLASTER1.TLv}, //160
    {1.0f, 1.0f, 1.0f, PLASTER1.BLu, PLASTER1.BLv},
    {2.0f, 1.0f, 1.0f, PLASTER1.BRu, PLASTER1.BRv},
    {2.0f, 2.0f, 1.0f, PLASTER1.TRu, PLASTER1.TRv},

    //Main building upper floor back (Texture)
    {0.0f, 2.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //164
    {0.0f, 1.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},
    {1.0f, 1.0f, 0.0f, PLASTER1.BRu, PLASTER1.BRv},
    {1.0f, 2.0f, 0.0f, PLASTER1.TRu, PLASTER1.TRv},

    {1.0f, 2.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //168
    {1.0f, 1.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},
    {2.0f, 1.0f, 0.0f, PLASTER1.BRu, PLASTER1.BRv},
    {2.0f, 2.0f, 0.0f, PLASTER1.TRu, PLASTER1.TRv},

    {2.0f, 2.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //172
    {2.0f, 1.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},
    {3.0f, 1.0f, 0.0f, PLASTER1.BRu, PLASTER1.BRv},
    {3.0f, 2.0f, 0.0f, PLASTER1.TRu, PLASTER1.TRv},
};

static const int LHouseIndices[] = {
    //Pole 1
    0, 1, 2, //Bottom
    2, 3, 0,
    4, 0, 1, //Left
    1, 5, 4,
    104, 105, 106, //Front
    106, 107, 104,
    6, 2, 3, //Right
    3, 7, 6,
    108, 109, 110, //Back
    110, 111, 108,

    //Pole 2
    8, 9, 10, //Bottom
    10, 11, 8,
    12, 8, 9, //Left
    9, 13, 12,
    112, 113, 114, //Front
    114, 115, 112,
    14, 10, 11, //Right
    11, 15, 14,
    116, 117, 118, //Back
    118, 119, 116,

    //Pole 3
    16, 17, 18, //Bottom
    18, 19, 16,
    20, 16, 17, //Left
    17, 21, 20,
    120, 121, 122, //Front
    122, 123, 120,
    22, 18, 19, //Right
    19, 23, 22,
    124, 125, 126, //Back
    126, 127, 124,

    //Main building
    24, 25, 26, //Bottom
    26, 27, 24,
    28, 24, 25, //Left
    25, 29, 28,
    136, 137, 138, //Front
    138, 139, 136,
    140, 141, 142,
    142, 143, 140,
    30, 26, 27, //Right
    27, 31, 30,
    144, 145, 146, //Back
    146, 147, 144,
    148, 149, 150,
    150, 151, 148,
    152, 153, 154,
    154, 155, 152,

    //Side building
    32, 33, 34, //Bottom
    34, 35, 32,
    36, 32, 33, //Left
    33, 37, 36,
    128, 129, 130, //Front
    130, 131, 128,
    38, 34, 35, //Right
    35, 39, 38,

    //Main roof
    40, 41, 42, //left
    43, 44, 45, //right
    46, 47, 48, //back
    48, 49, 46,
    50, 51, 52, //front
    52, 53, 50,

    //Side roof
    54, 55, 56, //front
    57, 58, 59, //left
    59, 60, 57,
    61, 62, 63, //right
    63, 64, 61,

    //Lil roof
    65, 66, 67,
    67, 68, 65,
    69, 70, 71,
    72, 73, 74,
    75, 76, 77,
    78, 79, 80,
    81, 82, 83,

    //Second story
    //Main
    84, 85, 86, //Bottom
    86, 87, 84,
    88, 84, 85, //Left
    85, 89, 88,
    156, 157, 158, //Front
    158, 159, 156,
    160, 161, 162,
    162, 163, 160,
    90, 86, 87, //Right
    87, 91, 90,
    164, 165, 166, //Back
    166, 167, 164,
    168, 169, 170,
    170, 171, 168,
    172, 173, 174,
    174, 175, 172,

    //Side
    92, 93, 94, //Bottom
    94, 95, 92,
    96, 92, 93, //Left
    93, 97, 96,
    132, 133, 134, //Front
    134, 135, 132,
    98, 94, 95, //Right
    95, 99, 98,

    //Ground
    100, 101, 102,
    102, 103, 100,
};

//7
static const Vertex CommunityHouseVertices[] = {
    //House
    {0.0f, 0.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv},
    {0.0f, 0.0f, 2.0f, PLASTER2.BRu, PLASTER2.BRv},
    {4.0f, 0.0f, 2.0f, PLASTER2.BRu, PLASTER2.BRv},
    {4.0f, 0.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv},

    {0.0f, 1.0f, 0.0f, PLASTER2.TLu, PLASTER2.TLv}, //4
    {0.0f, 1.0f, 2.0f, PLASTER2.TRu, PLASTER2.TRv},
    {4.0f, 1.0f, 2.0f, PLASTER2.TRu, PLASTER2.TRv},
    {4.0f, 1.0f, 0.0f, PLASTER2.TLu, PLASTER2.TLv},

    //Roof Left
    {0.0f, 1.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv}, //8
    {0.0f, 1.0f, 2.0f, PLASTER2.BRu, PLASTER2.BRv},
    {0.0f, 1.99f, 1.0f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Roof Right
    {4.0f, 1.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv}, //11
    {4.0f, 1.0f, 2.0f, PLASTER2.BRu, PLASTER2.BRv},
    {4.0f, 1.99f, 1.0f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Roof front
    {-0.05f, 2.0f, 1.0f, ROOF1.TRu, ROOF1.TRv}, //14
    {-0.05f, 1.0f, 2.05f, ROOF1.BRu, ROOF1.BRv},
    {4.05f, 1.0f, 2.05f, ROOF1.BLu, ROOF1.BLv},
    {4.05f, 2.0f, 1.0f, ROOF1.TLu, ROOF1.TLv},

    //Roof back
    {4.05f, 2.0f, 1.0f, ROOF1.TRu, ROOF1.TRv}, //18
    {4.05f, 1.0f, -0.05f, ROOF1.BRu, ROOF1.BRv},
    {-0.05f, 1.0f, -0.05f, ROOF1.BLu, ROOF1.BLv},
    {-0.05f, 2.0f, 1.0f, ROOF1.TLu, ROOF1.TLv},

    //Front (Texture)
    {0.0f, 1.0f, 2.0f, PLASTER1.TLu, PLASTER1.TLv}, //22
    {0.0f, 0.0f, 2.0f, PLASTER1.BLu, PLASTER1.BLv},
    {1.0f, 0.0f, 2.0f, PLASTER1.BRu, PLASTER1.BRv},
    {1.0f, 1.0f, 2.0f, PLASTER1.TRu, PLASTER1.TRv},

    {1.0f, 1.0f, 2.0f, PLASTER1.TLu, PLASTER1.TLv}, //26
    {1.0f, 0.0f, 2.0f, PLASTER1.BLu, PLASTER1.BLv},
    {2.0f, 0.0f, 2.0f, PLASTER1.BRu, PLASTER1.BRv},
    {2.0f, 1.0f, 2.0f, PLASTER1.TRu, PLASTER1.TRv},

    {2.0f, 1.0f, 2.0f, PLASTER1.TLu, PLASTER1.TLv}, //30
    {2.0f, 0.0f, 2.0f, PLASTER1.BLu, PLASTER1.BLv},
    {3.0f, 0.0f, 2.0f, PLASTER1.BRu, PLASTER1.BRv},
    {3.0f, 1.0f, 2.0f, PLASTER1.TRu, PLASTER1.TRv},

    {3.0f, 1.0f, 2.0f, PLASTER1.TLu, PLASTER1.TLv}, //34
    {3.0f, 0.0f, 2.0f, PLASTER1.BLu, PLASTER1.BLv},
    {4.0f, 0.0f, 2.0f, PLASTER1.BRu, PLASTER1.BRv},
    {4.0f, 1.0f, 2.0f, PLASTER1.TRu, PLASTER1.TRv},

    //Back (Texture)
    {1.0f, 1.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //38
    {1.0f, 0.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},
    {0.0f, 0.0f, 0.0f, PLASTER1.BRu, PLASTER1.BRv},
    {0.0f, 1.0f, 0.0f, PLASTER1.TRu, PLASTER1.TRv},

    {2.0f, 1.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //42
    {2.0f, 0.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},
    {1.0f, 0.0f, 0.0f, PLASTER1.BRu, PLASTER1.BRv},
    {1.0f, 1.0f, 0.0f, PLASTER1.TRu, PLASTER1.TRv},

    {3.0f, 1.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //46
    {3.0f, 0.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},
    {2.0f, 0.0f, 0.0f, PLASTER1.BRu, PLASTER1.BRv},
    {2.0f, 1.0f, 0.0f, PLASTER1.TRu, PLASTER1.TRv},

    {4.0f, 1.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //50
    {4.0f, 0.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},
    {3.0f, 0.0f, 0.0f, PLASTER1.BRu, PLASTER1.BRv},
    {3.0f, 1.0f, 0.0f, PLASTER1.TRu, PLASTER1.TRv},
};

static const int CommunityHouseIndices[] = {
    //House
    0, 1, 2, //Bottom
    2, 3, 0,
    4, 0, 1, //Left
    1, 5, 4,
    22, 23, 24, //Front
    24, 25, 22,
    26, 27, 28,
    28, 29, 26,
    30, 31, 32,
    32, 33, 30,
    34, 35, 36,
    36, 37, 34,
    6, 2, 3, //Right
    3, 7, 6,
    38, 39, 40, //Back
    40, 41, 38,
    42, 43, 44,
    44, 45, 42,
    46, 47, 48,
    48, 49, 46,
    50, 51, 52,
    52, 53, 50,

    4, 5, 6, //Top
    6, 7, 4,

    //Roof
    8, 9, 10,
    11, 12, 13,
    
    14, 15, 16,
    16, 17, 14,

    18, 19, 20,
    20, 21, 18,
};

//8
static const Vertex LongStoneVertices[] = {
    //Base
    {0.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {0.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {5.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {5.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},

    {0.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //4
    {0.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},
    {5.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},
    {5.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv},

    //Roof Front
    {0.5f, 2.0f, 1.0f, ROOF2.TRu, ROOF2.TRv}, //8 
    {0.5f, 1.0f, 2.0f, ROOF2.BRu, ROOF2.BRv},
    {4.5f, 1.0f, 2.0f, ROOF2.BLu, ROOF2.BLv},
    {4.5f, 2.0f, 1.0f, ROOF2.TLu, ROOF2.TLv},

    //Roof Back
    {4.5f, 2.0f, 1.0f, ROOF2.TRu, ROOF2.TRv}, //12
    {4.5f, 1.0f, 0.0f, ROOF2.BRu, ROOF2.BRv},
    {0.5f, 1.0f, 0.0f, ROOF2.BLu, ROOF2.BLv},
    {0.5f, 2.0f, 1.0f, ROOF2.TLu, ROOF2.TLv},

    //First Layer (L)
    {0.0f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv - 0.1755f}, //16
    {0.0f, 1.0f, 2.0f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {0.5f, 1.0f, 2.0f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {0.5f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv - 0.1755f},
    
    {0.0f, 1.25f, 0.0f, STONE1.TLu, STONE1.TLv}, //20
    {0.0f, 1.25f, 2.0f, STONE3.TRu, STONE3.TRv},
    {0.5f, 1.25f, 2.0f, STONE3.TRu, STONE3.TRv},
    {0.5f, 1.25f, 0.0f, STONE1.TLu, STONE1.TLv},

    //Second Layer (L)
    {0.0f, 1.25f, 0.25f, STONE1.BLu, STONE1.BLv - 0.1755f}, //24
    {0.0f, 1.25f, 1.75f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {0.5f, 1.25f, 1.75f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {0.5f, 1.25f, 0.25f, STONE1.BLu, STONE1.BLv - 0.1755f},
    
    {0.0f, 1.5f, 0.25f, STONE1.TLu, STONE1.TLv}, //28
    {0.0f, 1.5f, 1.75f, STONE3.TRu, STONE3.TRv},
    {0.5f, 1.5f, 1.75f, STONE3.TRu, STONE3.TRv},
    {0.5f, 1.5f, 0.25f, STONE1.TLu, STONE1.TLv},

    //Third Layer (L)
    {0.0f, 1.5f, 0.5f, STONE1.BLu, STONE1.BLv - 0.1755f}, //32
    {0.0f, 1.5f, 1.5f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {0.5f, 1.5f, 1.5f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {0.5f, 1.5f, 0.5f, STONE1.BLu, STONE1.BLv - 0.1755f},
    
    {0.0f, 1.75f, 0.5f, STONE1.TLu, STONE1.TLv}, //36
    {0.0f, 1.75f, 1.5f, STONE3.TRu, STONE3.TRv},
    {0.5f, 1.75f, 1.5f, STONE3.TRu, STONE3.TRv},
    {0.5f, 1.75f, 0.5f, STONE1.TLu, STONE1.TLv},

    //First Layer (R)
    {4.5f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv - 0.1755f}, //40
    {4.5f, 1.0f, 2.0f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {5.0f, 1.0f, 2.0f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {5.0f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv - 0.1755f},
    
    {4.5f, 1.25f, 0.0f, STONE1.TLu, STONE1.TLv}, //44
    {4.5f, 1.25f, 2.0f, STONE3.TRu, STONE3.TRv},
    {5.0f, 1.25f, 2.0f, STONE3.TRu, STONE3.TRv},
    {5.0f, 1.25f, 0.0f, STONE1.TLu, STONE1.TLv},

    //Second Layer (R)
    {4.5f, 1.25f, 0.25f, STONE1.BLu, STONE1.BLv - 0.1755f}, //48
    {4.5f, 1.25f, 1.75f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {5.0f, 1.25f, 1.75f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {5.0f, 1.25f, 0.25f, STONE1.BLu, STONE1.BLv - 0.1755f},
    
    {4.5f, 1.5f, 0.25f, STONE1.TLu, STONE1.TLv}, //52
    {4.5f, 1.5f, 1.75f, STONE3.TRu, STONE3.TRv},
    {5.0f, 1.5f, 1.75f, STONE3.TRu, STONE3.TRv},
    {5.0f, 1.5f, 0.25f, STONE1.TLu, STONE1.TLv},

    //Third Layer (R)
    {4.5f, 1.5f, 0.5f, STONE1.BLu, STONE1.BLv - 0.1755f}, //56
    {4.5f, 1.5f, 1.5f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {5.0f, 1.5f, 1.5f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {5.0f, 1.5f, 0.5f, STONE1.BLu, STONE1.BLv - 0.1755f},
    
    {4.5f, 1.75f, 0.5f, STONE1.TLu, STONE1.TLv}, //60
    {4.5f, 1.75f, 1.5f, STONE3.TRu, STONE3.TRv},
    {5.0f, 1.75f, 1.5f, STONE3.TRu, STONE3.TRv},
    {5.0f, 1.75f, 0.5f, STONE1.TLu, STONE1.TLv},

    //Fourth Layers I added afterwards
    {0.0f, 1.75f, 0.75f, STONE1.BLu, STONE1.BLv - 0.1755f}, //64 (L)
    {0.0f, 1.75f, 1.25f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {0.5f, 1.75f, 1.25f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {0.5f, 1.75f, 0.75f, STONE1.BLu, STONE1.BLv - 0.1755f},

    {0.0f, 2.0f, 0.75f, STONE1.TLu, STONE1.TLv}, //68 (L)
    {0.0f, 2.0f, 1.25f, STONE3.TRu, STONE3.TRv},
    {0.5f, 2.0f, 1.25f, STONE3.TRu, STONE3.TRv},
    {0.5f, 2.0f, 0.75f, STONE1.TLu, STONE1.TLv},

    {4.5f, 1.75f, 0.75f, STONE1.BLu, STONE1.BLv - 0.1755f}, //72 (R)
    {4.5f, 1.75f, 1.25f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {5.0f, 1.75f, 1.25f, STONE3.BRu, STONE3.BRv - 0.1755f},
    {5.0f, 1.75f, 0.75f, STONE1.BLu, STONE1.BLv - 0.1755f},
    
    {4.5f, 2.0f, 0.75f, STONE1.TLu, STONE1.TLv}, //76 (R)
    {4.5f, 2.0f, 1.25f, STONE3.TRu, STONE3.TRv},
    {5.0f, 2.0f, 1.25f, STONE3.TRu, STONE3.TRv},
    {5.0f, 2.0f, 0.75f, STONE1.TLu, STONE1.TLv},

    //Front (Texture)
    {0.0f, 1.0f, 2.0f, STONE1.TLu, STONE1.TLv}, //80
    {0.0f, 0.0f, 2.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},

    {1.0f, 1.0f, 2.0f, STONE3.TLu, STONE3.TLv}, //84
    {1.0f, 0.0f, 2.0f, STONE3.BLu, STONE3.BLv},
    {2.0f, 0.0f, 2.0f, STONE3.BRu, STONE3.BRv},
    {2.0f, 1.0f, 2.0f, STONE3.TRu, STONE3.TRv},

    {2.0f, 1.0f, 2.0f, STONE2.TLu, STONE2.TLv}, //88
    {2.0f, 0.0f, 2.0f, STONE2.BLu, STONE2.BLv},
    {3.0f, 0.0f, 2.0f, STONE2.BRu, STONE2.BRv},
    {3.0f, 1.0f, 2.0f, STONE2.TRu, STONE2.TRv},

    {3.0f, 1.0f, 2.0f, STONE3.TLu, STONE3.TLv}, //92
    {3.0f, 0.0f, 2.0f, STONE3.BLu, STONE3.BLv},
    {4.0f, 0.0f, 2.0f, STONE3.BRu, STONE3.BRv},
    {4.0f, 1.0f, 2.0f, STONE3.TRu, STONE3.TRv},

    {4.0f, 1.0f, 2.0f, STONE1.TLu, STONE1.TLv}, //96
    {4.0f, 0.0f, 2.0f, STONE1.BLu, STONE1.BLv},
    {5.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {5.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},

    //Back (Texture)
    {0.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //100
    {0.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 0.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 1.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    {1.0f, 1.0f, 0.0f, STONE3.TLu, STONE3.TLv}, //104
    {1.0f, 0.0f, 0.0f, STONE3.BLu, STONE3.BLv},
    {2.0f, 0.0f, 0.0f, STONE3.BRu, STONE3.BRv},
    {2.0f, 1.0f, 0.0f, STONE3.TRu, STONE3.TRv},

    {2.0f, 1.0f, 0.0f, STONE3.TLu, STONE3.TLv}, //108
    {2.0f, 0.0f, 0.0f, STONE3.BLu, STONE3.BLv},
    {3.0f, 0.0f, 0.0f, STONE3.BRu, STONE3.BRv},
    {3.0f, 1.0f, 0.0f, STONE3.TRu, STONE3.TRv},

    {3.0f, 1.0f, 0.0f, STONE3.TLu, STONE3.TLv}, //112
    {3.0f, 0.0f, 0.0f, STONE3.BLu, STONE3.BLv},
    {4.0f, 0.0f, 0.0f, STONE3.BRu, STONE3.BRv},
    {4.0f, 1.0f, 0.0f, STONE3.TRu, STONE3.TRv},

    {4.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //116
    {4.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {5.0f, 0.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {5.0f, 1.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    //First Layer (L)
    {0.0f, 1.25f, 2.0f, STONE1.TLu, STONE1.TLv}, //120
    {0.0f, 1.0f, 2.0f, STONE1.TLu, STONE1.TLv + 0.1f},
    {0.5f, 1.0f, 2.0f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {0.5f, 1.25f, 2.0f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    {0.0f, 1.25f, 0.0f, STONE1.TLu, STONE1.TLv}, //124
    {0.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv + 0.1f},
    {0.5f, 1.0f, 0.0f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {0.5f, 1.25f, 0.0f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    //Second Layer
    {0.0f, 1.5f, 1.75f, STONE1.TLu, STONE1.TLv}, //128
    {0.0f, 1.25f, 1.75f, STONE1.TLu, STONE1.TLv + 0.1f},
    {0.5f, 1.25f, 1.75f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {0.5f, 1.5f, 1.75f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    {0.0f, 1.5f, 0.25f, STONE1.TLu, STONE1.TLv}, //132
    {0.0f, 1.25f, 0.25f, STONE1.TLu, STONE1.TLv + 0.1f},
    {0.5f, 1.25f, 0.25f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {0.5f, 1.5f, 0.25f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    //Third
    {0.0f, 1.75f, 1.5f, STONE1.TLu, STONE1.TLv}, //136
    {0.0f, 1.5f, 1.5f, STONE1.TLu, STONE1.TLv + 0.1f},
    {0.5f, 1.5f, 1.5f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {0.5f, 1.75f, 1.5f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    {0.0f, 1.75f, 0.5f, STONE1.TLu, STONE1.TLv}, //140
    {0.0f, 1.5f, 0.5f, STONE1.TLu, STONE1.TLv + 0.1f},
    {0.5f, 1.5f, 0.5f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {0.5f, 1.75f, 0.5f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    //Fourth
    {0.0f, 2.0f, 1.25f, STONE1.TLu, STONE1.TLv}, //144
    {0.0f, 1.75f, 1.25f, STONE1.TLu, STONE1.TLv + 0.1f},
    {0.5f, 1.75f, 1.25f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {0.5f, 2.0f, 1.25f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    {0.0f, 2.0f, 0.75f, STONE1.TLu, STONE1.TLv}, //148
    {0.0f, 1.75f, 0.75f, STONE1.TLu, STONE1.TLv + 0.1f},
    {0.5f, 1.75f, 0.75f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {0.5f, 2.0f, 0.75f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    //First layer top
    {0.0f, 1.25f, 0.0f, 0.72f, 0.140f}, //152
    {0.0f, 1.25f, 2.0f, 0.72f, 0.141f},
    {0.5f, 1.25f, 2.0f, 0.68f, 0.141f},
    {0.5f, 1.25f, 0.0f, 0.68f, 0.140f},

    //Second layer top
    {0.0f, 1.5f, 0.25f, 0.72f, 0.140f}, //156
    {0.0f, 1.5f, 1.75f, 0.72f, 0.141f},
    {0.5f, 1.5f, 1.75f, 0.68f, 0.141f},
    {0.5f, 1.5f, 0.25f, 0.68f, 0.140f},

    //Third layer top
    {0.0f, 1.75f, 0.5f, 0.72f, 0.140f}, //160
    {0.0f, 1.75f, 1.5f, 0.72f, 0.141f},
    {0.5f, 1.75f, 1.5f, 0.68f, 0.141f},
    {0.5f, 1.75f, 0.5f, 0.68f, 0.140f},

    //Fourth layer top
    {0.0f, 2.0f, 0.75f, 0.72f, 0.140f}, //164
    {0.0f, 2.0f, 1.25f, 0.72f, 0.141f},
    {0.5f, 2.0f, 1.25f, 0.68f, 0.141f},
    {0.5f, 2.0f, 0.75f, 0.68f, 0.140f},

    //First Layer (R)
    {4.5f, 1.25f, 2.0f, STONE1.TLu, STONE1.TLv}, //168
    {4.5f, 1.0f, 2.0f, STONE1.TLu, STONE1.TLv + 0.1f},
    {5.0f, 1.0f, 2.0f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {5.0f, 1.25f, 2.0f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    {4.5f, 1.25f, 0.0f, STONE1.TLu, STONE1.TLv}, //172
    {4.5f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv + 0.1f},
    {5.0f, 1.0f, 0.0f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {5.0f, 1.25f, 0.0f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    //Second Layer
    {4.5f, 1.5f, 1.75f, STONE1.TLu, STONE1.TLv}, //176
    {4.5f, 1.25f, 1.75f, STONE1.TLu, STONE1.TLv + 0.1f},
    {5.0f, 1.25f, 1.75f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {5.0f, 1.5f, 1.75f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    {4.5f, 1.5f, 0.25f, STONE1.TLu, STONE1.TLv}, //180
    {4.5f, 1.25f, 0.25f, STONE1.TLu, STONE1.TLv + 0.1f},
    {5.0f, 1.25f, 0.25f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {5.0f, 1.5f, 0.25f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    //Third
    {4.5f, 1.75f, 1.5f, STONE1.TLu, STONE1.TLv}, //184
    {4.5f, 1.5f, 1.5f, STONE1.TLu, STONE1.TLv + 0.1f},
    {5.0f, 1.5f, 1.5f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {5.0f, 1.75f, 1.5f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    {4.5f, 1.75f, 0.5f, STONE1.TLu, STONE1.TLv}, //188
    {4.5f, 1.5f, 0.5f, STONE1.TLu, STONE1.TLv + 0.1f},
    {5.0f, 1.5f, 0.5f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {5.0f, 1.75f, 0.5f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    //Fourth
    {4.5f, 2.0f, 1.25f, STONE1.TLu, STONE1.TLv}, //192
    {4.5f, 1.75f, 1.25f, STONE1.TLu, STONE1.TLv + 0.1f},
    {5.0f, 1.75f, 1.25f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {5.0f, 2.0f, 1.25f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    {4.5f, 2.0f, 0.75f, STONE1.TLu, STONE1.TLv}, //196
    {4.5f, 1.75f, 0.75f, STONE1.TLu, STONE1.TLv + 0.1f},
    {5.0f, 1.75f, 0.75f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv + 0.1f},
    {5.0f, 2.0f, 0.75f, (STONE1.TRu + STONE1.TRu) / 2, STONE1.TRv},

    //First layer top
    {4.5f, 1.25f, 0.0f, 0.72f, 0.140f}, //200
    {4.5f, 1.25f, 2.0f, 0.72f, 0.141f},
    {5.0f, 1.25f, 2.0f, 0.68f, 0.141f},
    {5.0f, 1.25f, 0.0f, 0.68f, 0.140f},

    //Second layer top
    {4.5f, 1.5f, 0.25f, 0.72f, 0.140f}, //204
    {4.5f, 1.5f, 1.75f, 0.72f, 0.141f},
    {5.0f, 1.5f, 1.75f, 0.68f, 0.141f},
    {5.0f, 1.5f, 0.25f, 0.68f, 0.140f},

    //Third layer top
    {4.5f, 1.75f, 0.5f, 0.72f, 0.140f}, //208
    {4.5f, 1.75f, 1.5f, 0.72f, 0.141f},
    {5.0f, 1.75f, 1.5f, 0.68f, 0.141f},
    {5.0f, 1.75f, 0.5f, 0.68f, 0.140f},

    //Fourth layer top
    {4.5f, 2.0f, 0.75f, 0.72f, 0.140f}, //212
    {4.5f, 2.0f, 1.25f, 0.72f, 0.141f},
    {5.0f, 2.0f, 1.25f, 0.68f, 0.141f},
    {5.0f, 2.0f, 0.75f, 0.68f, 0.140f},
};

static const int LongStoneIndices[] = {
    //Base
    0, 1, 2, //Bottom
    2, 3, 0,
    4, 0, 1, //Left
    1, 5, 4,
    80, 81, 82, //Front
    82, 83, 80,
    84, 85, 86,
    86, 87, 84,
    88, 89, 90,
    90, 91, 88,
    92, 93, 94,
    94, 95, 92,
    96, 97, 98,
    98, 99, 96,
    6, 2, 3, //Right
    3, 7, 6,
    100, 101, 102, //Back
    102, 103, 100,
    104, 105, 106,
    106, 107, 104,
    108, 109, 110,
    110, 111, 108,
    112, 113, 114,
    114, 115, 112,
    116, 117, 118,
    118, 119, 116,

    //Roof
    8, 9, 10,
    10, 11, 8,

    12, 13, 14,
    14, 15, 12,

    //First layer (L)
    16, 17, 18, //Bottom
    18, 19, 16,
    20, 16, 17, //Left
    17, 21, 20,
    120, 121, 122, //Front
    122, 123, 120,
    22, 18, 19, //Right
    19, 23, 22,
    124, 125, 126, //Back
    126, 127, 124,
    152, 153, 154, //Top
    154, 155, 152,

    //Second Layer (L)
    24, 25, 26, //Bottom
    26, 27, 24,
    28, 24, 25, //Left
    25, 29, 28,
    128, 129, 130, //Front
    130, 131, 128,
    30, 26, 27, //Right
    27, 31, 30,
    132, 133, 134, //Back
    134, 135, 132,
    156, 157, 158, //Top
    158, 159, 156,

    //Third Layer (L)
    32, 33, 34, //Bottom
    34, 35, 32,
    36, 32, 33, //Left
    33, 37, 36,
    136, 137, 138, //Front
    138, 139, 136,
    38, 34, 35, //Right
    35, 39, 38,
    140, 141, 142, //Back
    142, 143, 140,
    160, 161, 162, //Top
    162, 163, 160,

    //First Layer (R)
    40, 41, 42, //Bottom
    42, 43, 40,
    44, 40, 41, //Left
    41, 45, 44,
    168, 169, 170, //Front
    170, 171, 168,
    46, 42, 43, //Right
    43, 47, 46,
    172, 173, 174, //Back
    174, 175, 172,
    200, 201, 202, //Top
    202, 203, 200,

    //Second Layer (R)
    48, 49, 50, //Bottom
    50, 51, 48,
    52, 48, 49, //Left
    49, 53, 52,
    176, 177, 178, //Front
    178, 179, 176,
    54, 50, 51, //Right
    51, 55, 54,
    180, 181, 182, //Back
    182, 183, 180,
    204, 205, 206, //Top
    206, 207, 204,

    //Third Layer (R)
    56, 57, 58, //Bottom
    58, 59, 56,
    60, 56, 57, //Left
    57, 61, 60,
    184, 185, 186, //Front
    186, 187, 184,
    62, 58, 59, //Right
    59, 63, 62,
    188, 189, 190, //Front
    190, 191, 188,
    208, 209, 210, //Top
    210, 211, 208,

    //Fourth Layers
    64, 65, 66, //Bottom
    66, 67, 64,
    68, 64, 65, //Left
    65, 69, 68,
    144, 145, 146, //Front
    146, 147, 144,
    70, 66, 67, //Right
    67, 71, 70,
    148, 149, 150, //Back
    150, 151, 148,
    164, 165, 166, //Top
    166, 167, 164,

    72, 73, 74, //Bottom
    74, 75, 72,
    76, 72, 73, //Left
    73, 77, 76,
    192, 193, 194, //Front
    194, 195, 192,
    78, 74, 75, //Right
    75, 79, 78,
    196, 197, 198, //Back
    198, 199, 196,
    212, 213, 214, //Top
    214, 215, 212,
};

//9
static const Vertex HalfHouseVertices[] = {
    //Tower base
    {0.0f, 0.0f, 0.0f, STONE1.TLu, STONE1.TLv},
    {0.0f, 0.0f, 3.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 0.0f, 3.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 0.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 3.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //4
    {0.0f, 3.0f, 3.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 3.0f, 3.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 3.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    //House base
    {2.0f, 0.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv}, //8
    {2.0f, 0.0f, 3.0f, PLASTER2.BRu, PLASTER2.BRv},
    {4.0f, 0.0f, 3.0f, PLASTER2.BRu, PLASTER2.BRv},
    {4.0f, 0.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv},

    {2.0f, 1.0f, 0.0f, PLASTER2.TLu, PLASTER2.TLv}, //12
    {2.0f, 1.0f, 3.0f, PLASTER2.TRu, PLASTER2.TRv},
    {4.0f, 1.0f, 3.0f, PLASTER2.TRu, PLASTER2.TRv},
    {4.0f, 1.0f, 0.0f, PLASTER2.TLu, PLASTER2.TLv},

    //Crenellation
    //Merlon 1
    {0.0f, 3.0f, 0.0f, 0.72f, 0.140f}, //16
    {0.0f, 3.0f, 0.2f, 0.72f, 0.141f},
    {0.2f, 3.0f, 0.2f, 0.68f, 0.141f},
    {0.2f, 3.0f, 0.0f, 0.68f, 0.140f},

    {0.0f, 3.25f, 0.0f, 0.72f, 0.140f}, //20
    {0.0f, 3.25f, 0.2f, 0.72f, 0.141f},
    {0.2f, 3.25f, 0.2f, 0.68f, 0.141f},
    {0.2f, 3.25f, 0.0f, 0.68f, 0.140f},
    
    //Merlon 2
    {0.4f, 3.0f, 0.0f, 0.72f, 0.140f}, //24
    {0.4f, 3.0f, 0.2f, 0.72f, 0.141f},
    {0.6f, 3.0f, 0.2f, 0.68f, 0.141f},
    {0.6f, 3.0f, 0.0f, 0.68f, 0.140f},

    {0.4f, 3.25f, 0.0f, 0.72f, 0.140f}, //28
    {0.4f, 3.25f, 0.2f, 0.72f, 0.141f},
    {0.6f, 3.25f, 0.2f, 0.68f, 0.141f},
    {0.6f, 3.25f, 0.0f, 0.68f, 0.140f},

    //Merlon 3
    {0.8f, 3.0f, 0.0f, 0.72f, 0.140f}, //32
    {0.8f, 3.0f, 0.2f, 0.72f, 0.141f},
    {1.2f, 3.0f, 0.2f, 0.68f, 0.141f},
    {1.2f, 3.0f, 0.0f, 0.68f, 0.140f},

    {0.8f, 3.25f, 0.0f, 0.72f, 0.140f}, //36
    {0.8f, 3.25f, 0.2f, 0.72f, 0.141f},
    {1.2f, 3.25f, 0.2f, 0.68f, 0.141f},
    {1.2f, 3.25f, 0.0f, 0.68f, 0.140f},

    //Merlon 4
    {1.4f, 3.0f, 0.0f, 0.72f, 0.140f}, //40
    {1.4f, 3.0f, 0.2f, 0.72f, 0.141f},
    {1.6f, 3.0f, 0.2f, 0.68f, 0.141f},
    {1.6f, 3.0f, 0.0f, 0.68f, 0.140f},

    {1.4f, 3.25f, 0.0f, 0.72f, 0.140f}, //44
    {1.4f, 3.25f, 0.2f, 0.72f, 0.141f},
    {1.6f, 3.25f, 0.2f, 0.68f, 0.141f},
    {1.6f, 3.25f, 0.0f, 0.68f, 0.140f},

    //Merlon 5
    {1.8f, 3.0f, 0.0f, 0.72f, 0.140f}, //48
    {1.8f, 3.0f, 0.2f, 0.72f, 0.141f},
    {2.0f, 3.0f, 0.2f, 0.68f, 0.141f},
    {2.0f, 3.0f, 0.0f, 0.68f, 0.140f},

    {1.8f, 3.25f, 0.0f, 0.72f, 0.140f}, //52
    {1.8f, 3.25f, 0.2f, 0.72f, 0.141f},
    {2.0f, 3.25f, 0.2f, 0.68f, 0.141f},
    {2.0f, 3.25f, 0.0f, 0.68f, 0.140f},

    //Merlon 6
    {1.8f, 3.0f, 0.4f, 0.72f, 0.140f}, //56
    {1.8f, 3.0f, 0.6f, 0.72f, 0.141f},
    {2.0f, 3.0f, 0.6f, 0.68f, 0.141f},
    {2.0f, 3.0f, 0.4f, 0.68f, 0.140f},

    {1.8f, 3.25f, 0.4f, 0.72f, 0.140f}, //60
    {1.8f, 3.25f, 0.6f, 0.72f, 0.141f},
    {2.0f, 3.25f, 0.6f, 0.68f, 0.141f},
    {2.0f, 3.25f, 0.4f, 0.68f, 0.140f},

    //Merlon 7
    {1.8f, 3.0f, 0.8f, 0.72f, 0.140f}, //64
    {1.8f, 3.0f, 1.0f, 0.72f, 0.141f},
    {2.0f, 3.0f, 1.0f, 0.68f, 0.141f},
    {2.0f, 3.0f, 0.8f, 0.68f, 0.140f},

    {1.8f, 3.25f, 0.8f, 0.72f, 0.140f}, //68
    {1.8f, 3.25f, 1.0f, 0.72f, 0.141f},
    {2.0f, 3.25f, 1.0f, 0.68f, 0.141f},
    {2.0f, 3.25f, 0.8f, 0.68f, 0.140f},

    //Merlon 8
    {1.8f, 3.0f, 1.2f, 0.72f, 0.140f}, //72
    {1.8f, 3.0f, 1.4f, 0.72f, 0.141f},
    {2.0f, 3.0f, 1.4f, 0.68f, 0.141f},
    {2.0f, 3.0f, 1.2f, 0.68f, 0.140f},

    {1.8f, 3.25f, 1.2f, 0.72f, 0.140f}, //76
    {1.8f, 3.25f, 1.4f, 0.72f, 0.141f},
    {2.0f, 3.25f, 1.4f, 0.68f, 0.141f},
    {2.0f, 3.25f, 1.2f, 0.68f, 0.140f},
    
    //Merlon 9
    {1.8f, 3.0f, 1.6f, 0.72f, 0.140f}, //80
    {1.8f, 3.0f, 1.8f, 0.72f, 0.141f},
    {2.0f, 3.0f, 1.8f, 0.68f, 0.141f},
    {2.0f, 3.0f, 1.6f, 0.68f, 0.140f},

    {1.8f, 3.25f, 1.6f, 0.72f, 0.140f}, //84
    {1.8f, 3.25f, 1.8f, 0.72f, 0.141f},
    {2.0f, 3.25f, 1.8f, 0.68f, 0.141f},
    {2.0f, 3.25f, 1.6f, 0.68f, 0.140f},

    //Merlon 10
    {1.8f, 3.0f, 2.0f, 0.72f, 0.140f}, //88
    {1.8f, 3.0f, 2.2f, 0.72f, 0.141f},
    {2.0f, 3.0f, 2.2f, 0.68f, 0.141f},
    {2.0f, 3.0f, 2.0f, 0.68f, 0.140f},

    {1.8f, 3.25f, 2.0f, 0.72f, 0.140f}, //92
    {1.8f, 3.25f, 2.2f, 0.72f, 0.141f},
    {2.0f, 3.25f, 2.2f, 0.68f, 0.141f},
    {2.0f, 3.25f, 2.0f, 0.68f, 0.140f},

    //Merlon 11
    {1.8f, 3.0f, 2.4f, 0.72f, 0.140f}, //96
    {1.8f, 3.0f, 2.6f, 0.72f, 0.141f},
    {2.0f, 3.0f, 2.6f, 0.68f, 0.141f},
    {2.0f, 3.0f, 2.4f, 0.68f, 0.140f},

    {1.8f, 3.25f, 2.4f, 0.72f, 0.140f}, //100
    {1.8f, 3.25f, 2.6f, 0.72f, 0.141f},
    {2.0f, 3.25f, 2.6f, 0.68f, 0.141f},
    {2.0f, 3.25f, 2.4f, 0.68f, 0.140f},

    //Merlon 12
    {1.8f, 3.0f, 2.8f, 0.72f, 0.140f}, //104
    {1.8f, 3.0f, 3.0f, 0.72f, 0.141f},
    {2.0f, 3.0f, 3.0f, 0.68f, 0.141f},
    {2.0f, 3.0f, 2.8f, 0.68f, 0.140f},

    {1.8f, 3.25f, 2.8f, 0.72f, 0.140f}, //108
    {1.8f, 3.25f, 3.0f, 0.72f, 0.141f},
    {2.0f, 3.25f, 3.0f, 0.68f, 0.141f},
    {2.0f, 3.25f, 2.8f, 0.68f, 0.140f},
    
    //Merlon 13
    {1.4f, 3.0f, 2.8f, 0.72f, 0.140f}, //112
    {1.4f, 3.0f, 3.0f, 0.72f, 0.141f},
    {1.6f, 3.0f, 3.0f, 0.68f, 0.141f},
    {1.6f, 3.0f, 2.8f, 0.68f, 0.140f},

    {1.4f, 3.25f, 2.8f, 0.72f, 0.140f}, //116
    {1.4f, 3.25f, 3.0f, 0.72f, 0.141f},
    {1.6f, 3.25f, 3.0f, 0.68f, 0.141f},
    {1.6f, 3.25f, 2.8f, 0.68f, 0.140f},

    //Merlon 14
    {1.0f, 3.0f, 2.8f, 0.72f, 0.140f}, //120
    {1.0f, 3.0f, 3.0f, 0.72f, 0.141f},
    {1.2f, 3.0f, 3.0f, 0.68f, 0.141f},
    {1.2f, 3.0f, 2.8f, 0.68f, 0.140f},

    {1.0f, 3.25f, 2.8f, 0.72f, 0.140f}, //124
    {1.0f, 3.25f, 3.0f, 0.72f, 0.141f},
    {1.2f, 3.25f, 3.0f, 0.68f, 0.141f},
    {1.2f, 3.25f, 2.8f, 0.68f, 0.140f},

    //Merlon 15
    {0.8f, 3.0f, 2.8f, 0.72f, 0.140f}, //128
    {0.8f, 3.0f, 3.0f, 0.72f, 0.141f},
    {1.2f, 3.0f, 3.0f, 0.68f, 0.141f},
    {1.2f, 3.0f, 2.8f, 0.68f, 0.140f},

    {0.8f, 3.25f, 2.8f, 0.72f, 0.140f}, //132
    {0.8f, 3.25f, 3.0f, 0.72f, 0.141f},
    {1.2f, 3.25f, 3.0f, 0.68f, 0.141f},
    {1.2f, 3.25f, 2.8f, 0.68f, 0.140f},

    //Merlon 16
    {0.4f, 3.0f, 2.8f, 0.72f, 0.140f}, //136
    {0.4f, 3.0f, 3.0f, 0.72f, 0.141f},
    {0.6f, 3.0f, 3.0f, 0.68f, 0.141f},
    {0.6f, 3.0f, 2.8f, 0.68f, 0.140f},

    {0.4f, 3.25f, 2.8f, 0.72f, 0.140f}, //140
    {0.4f, 3.25f, 3.0f, 0.72f, 0.141f},
    {0.6f, 3.25f, 3.0f, 0.68f, 0.141f},
    {0.6f, 3.25f, 2.8f, 0.68f, 0.140f},

    //Merlon 17
    {0.0f, 3.0f, 2.8f, 0.72f, 0.140f}, //144
    {0.0f, 3.0f, 3.0f, 0.72f, 0.141f},
    {0.2f, 3.0f, 3.0f, 0.68f, 0.141f},
    {0.2f, 3.0f, 2.8f, 0.68f, 0.140f},

    {0.0f, 3.25f, 2.8f, 0.72f, 0.140f}, //148
    {0.0f, 3.25f, 3.0f, 0.72f, 0.141f},
    {0.2f, 3.25f, 3.0f, 0.68f, 0.141f},
    {0.2f, 3.25f, 2.8f, 0.68f, 0.140f},

    //Merlon 18
    {0.0f, 3.0f, 0.4f, 0.72f, 0.140f}, //152
    {0.0f, 3.0f, 0.6f, 0.72f, 0.141f},
    {0.2f, 3.0f, 0.6f, 0.68f, 0.141f},
    {0.2f, 3.0f, 0.4f, 0.68f, 0.140f},

    {0.0f, 3.25f, 0.4f, 0.72f, 0.140f}, //156
    {0.0f, 3.25f, 0.6f, 0.72f, 0.141f},
    {0.2f, 3.25f, 0.6f, 0.68f, 0.141f},
    {0.2f, 3.25f, 0.4f, 0.68f, 0.140f},

    //Merlon 19
    {0.0f, 3.0f, 0.8f, 0.72f, 0.140f}, //160
    {0.0f, 3.0f, 1.0f, 0.72f, 0.141f},
    {0.2f, 3.0f, 1.0f, 0.68f, 0.141f},
    {0.2f, 3.0f, 0.8f, 0.68f, 0.140f},

    {0.0f, 3.25f, 0.8f, 0.72f, 0.140f}, //164
    {0.0f, 3.25f, 1.0f, 0.72f, 0.141f},
    {0.2f, 3.25f, 1.0f, 0.68f, 0.141f},
    {0.2f, 3.25f, 0.8f, 0.68f, 0.140f},

    //Merlon 20
    {0.0f, 3.0f, 1.2f, 0.72f, 0.140f}, //168
    {0.0f, 3.0f, 1.4f, 0.72f, 0.141f},
    {0.2f, 3.0f, 1.4f, 0.68f, 0.141f},
    {0.2f, 3.0f, 1.2f, 0.68f, 0.140f},

    {0.0f, 3.25f, 1.2f, 0.72f, 0.140f}, //172
    {0.0f, 3.25f, 1.4f, 0.72f, 0.141f},
    {0.2f, 3.25f, 1.4f, 0.68f, 0.141f},
    {0.2f, 3.25f, 1.2f, 0.68f, 0.140f},
    
    //Merlon 21
    {0.0f, 3.0f, 1.6f, 0.72f, 0.140f}, //176
    {0.0f, 3.0f, 1.8f, 0.72f, 0.141f},
    {0.2f, 3.0f, 1.8f, 0.68f, 0.141f},
    {0.2f, 3.0f, 1.6f, 0.68f, 0.140f},

    {0.0f, 3.25f, 1.6f, 0.72f, 0.140f}, //180
    {0.0f, 3.25f, 1.8f, 0.72f, 0.141f},
    {0.2f, 3.25f, 1.8f, 0.68f, 0.141f},
    {0.2f, 3.25f, 1.6f, 0.68f, 0.140f},

    //Merlon 22
    {0.0f, 3.0f, 2.0f, 0.72f, 0.140f}, //184
    {0.0f, 3.0f, 2.2f, 0.72f, 0.141f},
    {0.2f, 3.0f, 2.2f, 0.68f, 0.141f},
    {0.2f, 3.0f, 2.0f, 0.68f, 0.140f},

    {0.0f, 3.25f, 2.0f, 0.72f, 0.140f}, //188
    {0.0f, 3.25f, 2.2f, 0.72f, 0.141f},
    {0.2f, 3.25f, 2.2f, 0.68f, 0.141f},
    {0.2f, 3.25f, 2.0f, 0.68f, 0.140f},

    //Merlon 23
    {0.0f, 3.0f, 2.4f, 0.72f, 0.140f}, //192
    {0.0f, 3.0f, 2.6f, 0.72f, 0.141f},
    {0.2f, 3.0f, 2.6f, 0.68f, 0.141f},
    {0.2f, 3.0f, 2.4f, 0.68f, 0.140f},

    {0.0f, 3.25f, 2.4f, 0.72f, 0.140f}, //196
    {0.0f, 3.25f, 2.6f, 0.72f, 0.141f},
    {0.2f, 3.25f, 2.6f, 0.68f, 0.141f},
    {0.2f, 3.25f, 2.4f, 0.68f, 0.140f},

    //Roof Back
    {2.0f, 1.99f, 0.0f, PLASTER2.TRu, PLASTER2.TRv}, //200
    {2.0f, 1.0f, 0.0f, PLASTER2.BRu, PLASTER2.BRv},
    {4.0f, 1.0f, 0.0f, PLASTER2.TLu, PLASTER2.TLv},

    //Roof Front
    {2.0f, 1.99f, 3.0f, PLASTER2.TLu, PLASTER2.TLv}, //203
    {2.0f, 1.0f, 3.0f, PLASTER2.BLu, PLASTER2.BLv},
    {4.0f, 1.0f, 3.0f, PLASTER2.BRu, PLASTER2.BRv},

    //Roof Top
    {2.0f, 2.0f, 0.0f, ROOF1.TRu, ROOF1.TRv}, //206
    {2.0f, 2.0f, 3.0f, ROOF1.BRu, ROOF1.BRv},
    {4.0f, 1.0f, 3.0f, ROOF1.BLu, ROOF1.BLv},
    {4.0f, 1.0f, 0.0f, ROOF1.TLu, ROOF1.TLv},

    //Tower Front (Textures)
    {0.0f, 1.0f, 3.0f, STONE1.TLu, STONE1.TLv}, //210
    {0.0f, 0.0f, 3.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 0.0f, 3.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 1.0f, 3.0f, STONE1.TRu, STONE1.TRv},

    {1.0f, 1.0f, 3.0f, STONE1.TLu, STONE1.TLv}, //214
    {1.0f, 0.0f, 3.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 0.0f, 3.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 1.0f, 3.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 2.0f, 3.0f, STONE1.TLu, STONE1.TLv}, //218
    {0.0f, 1.0f, 3.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 1.0f, 3.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 2.0f, 3.0f, STONE1.TRu, STONE1.TRv},

    {1.0f, 2.0f, 3.0f, STONE1.TLu, STONE1.TLv}, //222
    {1.0f, 1.0f, 3.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 1.0f, 3.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 2.0f, 3.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 3.0f, 3.0f, STONE3.TLu, STONE3.TLv}, //226
    {0.0f, 2.0f, 3.0f, STONE3.BLu, STONE3.BLv},
    {1.0f, 2.0f, 3.0f, STONE3.BRu, STONE3.BRv},
    {1.0f, 3.0f, 3.0f, STONE3.TRu, STONE3.TRv},

    {1.0f, 3.0f, 3.0f, STONE3.TLu, STONE3.TLv}, //230
    {1.0f, 2.0f, 3.0f, STONE3.BLu, STONE3.BLv},
    {2.0f, 2.0f, 3.0f, STONE3.BRu, STONE3.BRv},
    {2.0f, 3.0f, 3.0f, STONE3.TRu, STONE3.TRv},

    //Back (Textures)
    {0.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //234
    {0.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 0.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 1.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    {1.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //238
    {1.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 0.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 1.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 2.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //242
    {0.0f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {1.0f, 1.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {1.0f, 2.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    {1.0f, 2.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //246
    {1.0f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 1.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 2.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 3.0f, 0.0f, STONE3.TLu, STONE3.TLv}, //250
    {0.0f, 2.0f, 0.0f, STONE3.BLu, STONE3.BLv},
    {1.0f, 2.0f, 0.0f, STONE3.BRu, STONE3.BRv},
    {1.0f, 3.0f, 0.0f, STONE3.TRu, STONE3.TRv},

    {1.0f, 3.0f, 0.0f, STONE3.TLu, STONE3.TLv}, //254
    {1.0f, 2.0f, 0.0f, STONE3.BLu, STONE3.BLv},
    {2.0f, 2.0f, 0.0f, STONE3.BRu, STONE3.BRv},
    {2.0f, 3.0f, 0.0f, STONE3.TRu, STONE3.TRv},

    //Tower Left (Textures)
    {0.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //258
    {0.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {0.0f, 0.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {0.0f, 1.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 1.0f, 1.0f, STONE1.TLu, STONE1.TLv}, //262
    {0.0f, 0.0f, 1.0f, STONE1.BLu, STONE1.BLv},
    {0.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {0.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 1.0f, 2.0f, STONE1.TLu, STONE1.TLv}, //266
    {0.0f, 0.0f, 2.0f, STONE1.BLu, STONE1.BLv},
    {0.0f, 0.0f, 3.0f, STONE1.BRu, STONE1.BRv},
    {0.0f, 1.0f, 3.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 2.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //270
    {0.0f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {0.0f, 1.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {0.0f, 2.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 2.0f, 1.0f, STONE1.TLu, STONE1.TLv}, //274
    {0.0f, 1.0f, 1.0f, STONE1.BLu, STONE1.BLv},
    {0.0f, 1.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {0.0f, 2.0f, 2.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 2.0f, 2.0f, STONE1.TLu, STONE1.TLv}, //278
    {0.0f, 1.0f, 2.0f, STONE1.BLu, STONE1.BLv},
    {0.0f, 1.0f, 3.0f, STONE1.BRu, STONE1.BRv},
    {0.0f, 2.0f, 3.0f, STONE1.TRu, STONE1.TRv},

    {0.0f, 3.0f, 0.0f, STONE3.TLu, STONE3.TLv}, //282
    {0.0f, 2.0f, 0.0f, STONE3.BLu, STONE3.BLv},
    {0.0f, 2.0f, 1.0f, STONE3.BRu, STONE3.BRv},
    {0.0f, 3.0f, 1.0f, STONE3.TRu, STONE3.TRv},

    {0.0f, 3.0f, 1.0f, STONE3.TLu, STONE3.TLv}, //286
    {0.0f, 2.0f, 1.0f, STONE3.BLu, STONE3.BLv},
    {0.0f, 2.0f, 2.0f, STONE3.BRu, STONE3.BRv},
    {0.0f, 3.0f, 2.0f, STONE3.TRu, STONE3.TRv},

    {0.0f, 3.0f, 2.0f, STONE3.TLu, STONE3.TLv}, //290
    {0.0f, 2.0f, 2.0f, STONE3.BLu, STONE3.BLv},
    {0.0f, 2.0f, 3.0f, STONE3.BRu, STONE3.BRv},
    {0.0f, 3.0f, 3.0f, STONE3.TRu, STONE3.TRv},

    //Tower Right (Textures)
    {2.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //294
    {2.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 0.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 1.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 1.0f, 1.0f, STONE1.TLu, STONE1.TLv}, //298
    {2.0f, 0.0f, 1.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 1.0f, 2.0f, STONE1.TLu, STONE1.TLv}, //302
    {2.0f, 0.0f, 2.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 0.0f, 3.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 1.0f, 3.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 2.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //306
    {2.0f, 1.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 1.0f, 1.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 2.0f, 1.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 2.0f, 1.0f, STONE1.TLu, STONE1.TLv}, //310
    {2.0f, 1.0f, 1.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 1.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 2.0f, 2.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 2.0f, 2.0f, STONE1.TLu, STONE1.TLv}, //314
    {2.0f, 1.0f, 2.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 1.0f, 3.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 2.0f, 3.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 3.0f, 0.0f, STONE3.TLu, STONE3.TLv}, //318
    {2.0f, 2.0f, 0.0f, STONE3.BLu, STONE3.BLv},
    {2.0f, 2.0f, 1.0f, STONE3.BRu, STONE3.BRv},
    {2.0f, 3.0f, 1.0f, STONE3.TRu, STONE3.TRv},

    {2.0f, 3.0f, 1.0f, STONE3.TLu, STONE3.TLv}, //322
    {2.0f, 2.0f, 1.0f, STONE3.BLu, STONE3.BLv},
    {2.0f, 2.0f, 2.0f, STONE3.BRu, STONE3.BRv},
    {2.0f, 3.0f, 2.0f, STONE3.TRu, STONE3.TRv},

    {2.0f, 3.0f, 2.0f, STONE3.TLu, STONE3.TLv}, //326
    {2.0f, 2.0f, 2.0f, STONE3.BLu, STONE3.BLv},
    {2.0f, 2.0f, 3.0f, STONE3.BRu, STONE3.BRv},
    {2.0f, 3.0f, 3.0f, STONE3.TRu, STONE3.TRv},

    //House Front (Textures)
    {2.0f, 1.0f, 3.0f, PLASTER2.TLu, PLASTER2.TLv}, //330
    {2.0f, 0.0f, 3.0f, PLASTER2.BLu, PLASTER2.BLv},
    {4.0f, 0.0f, 3.0f, PLASTER2.BRu, PLASTER2.BRv},
    {4.0f, 1.0f, 3.0f, PLASTER2.TRu, PLASTER2.TRv},

    {2.0f, 1.0f, 0.0f, PLASTER2.TLu, PLASTER2.TLv}, //334
    {2.0f, 0.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv},
    {4.0f, 0.0f, 0.0f, PLASTER2.BRu, PLASTER2.BRv},
    {4.0f, 1.0f, 0.0f, PLASTER2.TRu, PLASTER2.TRv},
};

static const int HalfHouseIndices[] = {
    //Tower Base
    0, 1, 2, //Bottom
    2, 3, 0,

    258, 259, 260, //Left
    260, 261, 258,
    262, 263, 264,
    264, 265, 262,
    266, 267, 268,
    268, 269, 266,
    270, 271, 272,
    272, 273, 270,
    274, 275, 276,
    276, 277, 274,
    278, 279, 280,
    280, 281, 278,
    282, 283, 284,
    284, 285, 282,
    286, 287, 288,
    288, 289, 286,
    290, 291, 292,
    292, 293, 290,

    210, 211, 212, //Front
    212, 213, 210,
    214, 215, 216,
    216, 217, 214,
    218, 219, 220,
    220, 221, 218,
    222, 223, 224,
    224, 225, 222,
    226, 227, 228,
    228, 229, 226,
    230, 231, 232,
    232, 233, 230,

    294, 295, 296, //Right
    296, 297, 294,
    298, 299, 300,
    300, 301, 298,
    302, 303, 304,
    304, 305, 302,
    306, 307, 308,
    308, 309, 306,
    310, 311, 312,
    312, 313, 310,
    314, 315, 316,
    316, 317, 314,
    318, 319, 320,
    320, 321, 318,
    322, 323, 324,
    324, 325, 322,
    326, 327, 328,
    328, 329, 326,

    234, 235, 236, //Front
    236, 237, 234,
    238, 239, 240,
    240, 241, 238,
    242, 243, 244,
    244, 245, 242,
    246, 247, 248,
    248, 249, 246,
    250, 251, 252,
    252, 253, 250,
    254, 255, 256,
    256, 257, 254,

    4, 5, 6, //Top
    6, 7, 4,

    //House Base
    8, 9, 10, //Bottom
    10, 11, 8,
    330, 331, 332, //Front
    332, 333, 330,
    14, 10, 11, //Right
    11, 15, 14,
    334, 335, 336, //Back
    336, 337, 334,

    //Merlon 1
    16, 17, 18, //Bottom
    18, 19, 16,
    20, 16, 17, //Left
    17, 21, 20,
    21, 17, 18, //Front
    18, 22, 21,
    22, 18, 19, //Right
    19, 23, 22,
    23, 19, 16, //Back
    16, 20, 23,
    20, 21, 22, //Top
    22, 23, 20,

    //Merlon 2
    24, 25, 26, //Bottom
    26, 27, 24,
    28, 24, 25, //Left
    25, 29, 28,
    29, 25, 26, //Front
    26, 30, 29,
    30, 26, 27, //Right
    27, 31, 30,
    31, 27, 24, //Back
    24, 28, 31,
    28, 29, 30, //Top
    30, 31, 28,

    //Merlon 3
    32, 33, 34, //Bottom
    34, 35, 32,
    36, 32, 33, //Left
    33, 37, 36,
    37, 33, 34, //Front
    34, 38, 37,
    38, 34, 35, //Right
    35, 39, 38,
    39, 35, 32, //Back
    32, 36, 39,
    36, 37, 38, //Top
    38, 39, 36,

    //Merlon 4
    40, 41, 42, //Bottom
    42, 43, 40,
    44, 40, 41, //Left
    41, 45, 44,
    45, 41, 42, //Front
    42, 46, 45,
    46, 42, 43, //Right
    43, 47, 46,
    47, 43, 40, //Back
    40, 44, 47,
    44, 45, 46, //Top
    46, 47, 44,

    //Merlon 5
    48, 49, 50, //Bottom
    50, 51, 48,
    52, 48, 49, //Left
    49, 53, 52,
    53, 49, 50, //Front
    50, 54, 53,
    54, 50, 51, //Right
    51, 55, 54,
    55, 51, 48, //Back
    48, 52, 55,
    52, 53, 54, //Top
    54, 55, 52,

    //Merlon 6
    56, 57, 58, //Bottom
    58, 59, 56,
    60, 56, 57, //Left
    57, 61, 60,
    61, 57, 58, //Front
    58, 62, 61,
    62, 58, 59, //Right
    59, 63, 62,
    63, 59, 56, //Back
    56, 60, 63,
    60, 61, 62, //Top
    62, 63, 60,

    //Merlon 7
    64, 65, 66, //Bottom
    66, 67, 64,
    68, 64, 65, //Left
    65, 69, 68,
    69, 65, 66, //Front
    66, 70, 69,
    70, 66, 67, //Right
    67, 71, 70,
    71, 67, 64, //Back
    64, 68, 71,
    68, 69, 70, //Top
    70, 71, 68,

    //Merlon 8
    72, 73, 74, //Bottom
    74, 75, 72,
    76, 72, 73, //Left
    73, 77, 76,
    77, 73, 74, //Front
    74, 78, 77,
    78, 74, 75, //Right
    75, 79, 78,
    79, 75, 72, //Back
    72, 76, 79,
    76, 77, 78, //Top
    78, 79, 76,

    //Merlon 9
    80, 81, 82, //Bottom
    82, 83, 80,
    84, 80, 81, //Left
    81, 85, 84,
    85, 81, 82, //Front
    82, 86, 85,
    86, 82, 83, //Right
    83, 87, 86,
    87, 83, 80, //Back
    80, 84, 87,
    84, 85, 86, //Top
    86, 87, 84,

    //Merlon 10
    88, 89, 90, //Bottom
    90, 91, 88,
    92, 88, 89, //Left
    89, 93, 92,
    93, 89, 90, //Front
    90, 94, 93,
    94, 90, 91, //Right
    91, 95, 94,
    95, 91, 88, //Back
    88, 92, 95,
    92, 93, 94, //Top
    94, 95, 92,

    //Merlon 11
    96, 97, 98, //Bottom
    98, 99, 96,
    100, 96, 97, //Left
    97, 101, 100,
    101, 97, 98, //Front
    98, 102, 101,
    102, 98, 99, //Right
    99, 103, 102,
    103, 99, 96, //Back
    96, 100, 103,
    100, 101, 102, //Top
    102, 103, 100,

    //Merlon 12
    104, 105, 106, //Bottom
    106, 107, 104,
    108, 104, 105, //Left
    105, 109, 108,
    109, 105, 106, //Front
    106, 110, 109,
    110, 106, 107, //Right
    107, 111, 110,
    111, 107, 104, //Back
    104, 108, 111,
    108, 109, 110, //Top
    110, 111, 108,

    //Merlon 13
    112, 113, 114, //Bottom
    114, 115, 112,
    116, 112, 113, //Left
    113, 117, 116,
    117, 113, 114, //Front
    114, 118, 117,
    118, 114, 115, //Right
    115, 119, 118,
    119, 115, 112, //Back
    112, 116, 119,
    116, 117, 118, //Top
    118, 119, 116,

    //Merlon 14
    120, 121, 122, //Bottom
    122, 123, 120,
    124, 120, 121, //Left
    121, 125, 124,
    125, 121, 122, //Front
    122, 126, 125,
    126, 122, 123, //Right
    123, 127, 126,
    127, 123, 120, //Back
    120, 124, 127,
    124, 125, 126, //Top
    126, 127, 124,

    //Merlon 15
    128, 129, 130, //Bottom
    130, 131, 128,
    132, 128, 129, //Left
    129, 133, 132,
    133, 129, 130, //Front
    130, 134, 133,
    134, 130, 131, //Right
    131, 135, 134,
    135, 131, 128, //Back
    128, 132, 135,
    132, 133, 134, //Top
    134, 135, 132,

    //Merlon 16,
    136, 137, 138, //Bottom
    138, 139, 136,
    140, 136, 137, //Left
    137, 141, 140,
    141, 137, 138, //Front
    138, 142, 141,
    142, 138, 139, //Right
    139, 143, 142,
    143, 139, 136, //Back
    136, 140, 143,
    140, 141, 142, //Top
    142, 143, 140,

    //Merlon 17
    144, 145, 146, //Bottom
    146, 147, 144,
    148, 144, 145, //Left
    145, 149, 148,
    149, 146, 145, //Front
    145, 150, 149,
    150, 146, 147, //Right
    147, 151, 150,
    151, 147, 144, //Back
    144, 148, 151,
    148, 149, 150, //Top
    150, 151, 148,

    //Merlon 18
    152, 153, 154, //Bottom
    154, 155, 152,
    156, 152, 153, //Left
    153, 157, 156,
    157, 153, 154, //Front
    154, 158, 157,
    158, 154, 155, //Right
    155, 159, 158,
    159, 155, 152, //Back
    152, 156, 159,
    156, 157, 158, //Top
    158, 159, 156,
    
    //Merlon 19
    160, 161, 162, //Bottom
    162, 163, 160,
    164, 160, 161, //Left
    161, 165, 164,
    165, 161, 162, //Front
    162, 166, 165,
    166, 162, 163, //Right
    163, 167, 166,
    167, 163, 160, //Back
    160, 164, 167,
    164, 165, 166, //Top
    166, 167, 164,

    //Merlon 20
    168, 169, 170, //Bottom
    170, 171, 168,
    172, 168, 169, //Left
    169, 173, 172,
    173, 169, 170, //Front
    170, 174, 173, 
    174, 170, 171, //Right
    171, 175, 174,
    175, 171, 168, //Back
    168, 172, 175,
    172, 173, 174, //Top
    174, 175, 172,

    //Merlon 21
    176, 177, 178, //Bottom
    178, 179, 176,
    180, 176, 177, //Left
    177, 181, 180,
    181, 177, 178, //Front
    178, 182, 181,
    182, 178, 179, //Right
    179, 183, 182,
    183, 179, 176, //Back
    176, 180, 183,
    180, 181, 182, //Top
    182, 183, 180,

    //Merlon 22
    184, 185, 186, //Bottom
    186, 187, 184,
    188, 184, 185, //Left
    185, 189, 188,
    189, 185, 186, //Front
    186, 190, 189,
    190, 186, 187, //Right
    187, 191, 190,
    191, 187, 184, //Back
    184, 188, 191,
    188, 189, 190, //Top
    190, 191, 188,

    //Merlon 23
    192, 193, 194, //Bottom
    194, 195, 192,
    196, 192, 193, //Left
    193, 197, 196,
    197, 193, 194, //Front
    194, 198, 197,
    198, 194, 195, //Right
    195, 199, 198,
    199, 195, 192, //Back
    192, 196, 199,
    196, 197, 198, //Top
    198, 199, 196,

    //Roof
    200, 201, 202,
    203, 204, 205,
    206, 207, 208,
    208, 209, 206,
};

//10
static const Vertex StairHouseVertices[] = {
    //Pole 1
    {0.0f, 0.0f, 0.0f, WOOD.BLu, WOOD.BLv},
    {0.0f, 0.0f, 0.1f, WOOD.BRu, WOOD.BRv},
    {0.1f, 0.0f, 0.1f, WOOD.BRu, WOOD.BRv},
    {0.1f, 0.0f, 0.0f, WOOD.BLu, WOOD.BLv},
 
    {0.0f, 1.0f, 0.0f, WOOD.TLu, WOOD.TLv}, //4
    {0.0f, 1.0f, 0.1f, WOOD.TRu, WOOD.TRv},
    {0.1f, 1.0f, 0.1f, WOOD.TRu, WOOD.TRv},
    {0.1f, 1.0f, 0.0f, WOOD.TLu, WOOD.TLv},

    //Pole 2
    {0.0f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv}, //8
    {0.0f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {0.1f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {0.1f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv},

    {0.0f, 1.0f, 0.9f, WOOD.TLu, WOOD.TLv}, //12
    {0.0f, 1.0f, 1.0f, WOOD.TRu, WOOD.TRv},
    {0.1f, 1.0f, 1.0f, WOOD.TRu, WOOD.TRv},
    {0.1f, 1.0f, 0.9f, WOOD.TLu, WOOD.TLv},

    //Pole 3
    {0.9f, 0.0f, 0.0f, WOOD.BLu, WOOD.BLv}, //16
    {0.9f, 0.0f, 0.1f, WOOD.BRu, WOOD.BRv},
    {1.0f, 0.0f, 0.1f, WOOD.BRu, WOOD.BRv},
    {1.0f, 0.0f, 0.0f, WOOD.BLu, WOOD.BLv},

    {0.9f, 1.0f, 0.0f, WOOD.TLu, WOOD.TLv}, //20
    {0.9f, 1.0f, 0.1f, WOOD.TRu, WOOD.TRv},
    {1.0f, 1.0f, 0.1f, WOOD.TRu, WOOD.TRv},
    {1.0f, 1.0f, 0.0f, WOOD.TLu, WOOD.TLv},

    //Landing
    {0.0f, 1.0f, 0.0f, WOOD.TLu, WOOD.TLv}, //24
    {0.0f, 1.0f, 1.0f, WOOD.BLu, WOOD.BLv},
    {1.0f, 1.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {1.0f, 1.0f, 0.0f, WOOD.TRu, WOOD.TRv},

    {0.0f, 1.0f, 1.0f, WOOD.TLu, WOOD.TLv}, //28
    {0.0f, 0.0f, 2.0f, WOOD.BLu, WOOD.BLv},
    {1.0f, 0.0f, 2.0f, WOOD.BRu, WOOD.BRv},
    {1.0f, 1.0f, 1.0f, WOOD.TRu, WOOD.TRv},

    //House Base
    {1.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv}, //32
    {1.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},

    {1.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //36
    {1.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},
    {3.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},
    {3.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv},

    //Upper floor
    {1.0f, 1.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv}, //40
    {1.0f, 1.0f, 2.0f, PLASTER1.BRu, PLASTER1.BRv},
    {3.0f, 1.0f, 2.0f, PLASTER1.BRu, PLASTER1.BRv},
    {3.0f, 1.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},

    {1.0f, 2.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //44
    {1.0f, 2.0f, 2.0f, PLASTER1.TRu, PLASTER1.TRv},
    {3.0f, 2.0f, 2.0f, PLASTER1.TRu, PLASTER1.TRv},
    {3.0f, 2.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv},

    //Roof Back
    {1.0f, 2.0f, 0.0f, PLASTER2.BLu, PLASTER2.BLv}, //48
    {3.0f, 2.0f, 0.0f, PLASTER2.BRu, PLASTER2.BRv},
    {2.0f, 2.99f, 0.0f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Roof Front
    {1.0f, 2.0f, 2.0f, PLASTER2.BLu, PLASTER2.BLv}, //51
    {3.0f, 2.0f, 2.0f, PLASTER2.BRu, PLASTER2.BRv},
    {2.0f, 2.99f, 2.0f, (PLASTER2.TLu + PLASTER2.TRu) / 2, PLASTER2.TLv},

    //Roof Left
    {2.0f, 3.0f, -0.05f, ROOF2.TLu, ROOF2.TLv}, //54
    {0.95f, 2.0f, -0.05f, ROOF2.BLu, ROOF2.BLv},
    {0.95f, 2.0f, 2.05f, ROOF2.BRu, ROOF2.BRv},
    {2.0f, 3.0f, 2.05f, ROOF2.TRu, ROOF2.TRv},

    //Roof Right
    {2.0f, 3.0f, 2.05f, ROOF2.TLu, ROOF2.TLv}, //58
    {3.05f, 2.0f, 2.05f, ROOF2.BLu, ROOF2.BLv},
    {3.05f, 2.0f, -0.05f, ROOF2.BRu, ROOF2.BRv},
    {2.0f, 3.0f, -0.05f, ROOF2.TRu, ROOF2.TRv},

    //Ground
    {0.0f, 0.001f, 2.0f, ROAD.TLu, ROAD.TLv}, //62
    {0.0f, 0.001f, 3.0f, ROAD.BLu, ROAD.BLv},
    {1.0f, 0.001f, 3.0f, ROAD.BRu, ROAD.BRv},
    {1.0f, 0.001f, 2.0f, ROAD.TRu, ROAD.TRv},

    {1.0f, 0.001f, 2.0f, ROAD.TLu, ROAD.TLv}, //66
    {1.0f, 0.001f, 3.0f, ROAD.BLu, ROAD.BLv},
    {2.0f, 0.001f, 3.0f, ROAD.BRu, ROAD.BRv},
    {2.0f, 0.001f, 2.0f, ROAD.TRu, ROAD.TRv},

    {2.0f, 0.001f, 2.0f, ROAD.TLu, ROAD.TLv}, //70
    {2.0f, 0.001f, 3.0f, ROAD.BLu, ROAD.BLv},
    {3.0f, 0.001f, 3.0f, ROAD.BRu, ROAD.BRv},
    {3.0f, 0.001f, 2.0f, ROAD.TRu, ROAD.TRv},

    {0.0f, 0.001f, 0.0f, ROAD.TLu, ROAD.TLv}, //74
    {0.0f, 0.001f, 1.0f, ROAD.BLu, ROAD.BLv},
    {1.0f, 0.001f, 1.0f, ROAD.BRu, ROAD.BRv},
    {1.0f, 0.001f, 0.0f, ROAD.TRu, ROAD.TRv},

    {0.0f, 0.001f, 1.0f, ROAD.TLu, ROAD.TLv}, //78
    {0.0f, 0.001f, 2.0f, ROAD.BLu, ROAD.BLv},
    {1.0f, 0.001f, 2.0f, ROAD.BRu, ROAD.BRv},
    {1.0f, 0.001f, 1.0f, ROAD.TRu, ROAD.TRv},

    //Building front (Texture)
    {1.0f, 1.0f, 2.0f, STONE2.TLu, STONE2.TLv}, //82
    {1.0f, 0.0f, 2.0f, STONE2.BLu, STONE2.BLv},
    {2.0f, 0.0f, 2.0f, STONE2.BRu, STONE2.BRv},
    {2.0f, 1.0f, 2.0f, STONE2.TRu, STONE2.TRv},

    {2.0f, 1.0f, 2.0f, STONE1.TLu, STONE1.TLv}, //86
    {2.0f, 0.0f, 2.0f, STONE1.BLu, STONE1.BLv},
    {3.0f, 0.0f, 2.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 1.0f, 2.0f, STONE1.TRu, STONE1.TRv},

    //Back (Texture)
    {1.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //90
    {1.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {2.0f, 0.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {2.0f, 1.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    {2.0f, 1.0f, 0.0f, STONE1.TLu, STONE1.TLv}, //94
    {2.0f, 0.0f, 0.0f, STONE1.BLu, STONE1.BLv},
    {3.0f, 0.0f, 0.0f, STONE1.BRu, STONE1.BRv},
    {3.0f, 1.0f, 0.0f, STONE1.TRu, STONE1.TRv},

    //Building front upper (Texture)
    {1.0f, 2.0f, 2.0f, PLASTER1.TLu, PLASTER1.TLv}, //98
    {1.0f, 1.0f, 2.0f, PLASTER1.BLu, PLASTER1.BLv},
    {2.0f, 1.0f, 2.0f, PLASTER1.BRu, PLASTER1.BRv},
    {2.0f, 2.0f, 2.0f, PLASTER1.TRu, PLASTER1.TRv},

    {2.0f, 2.0f, 2.0f, PLASTER1.TLu, PLASTER1.TLv}, //102
    {2.0f, 1.0f, 2.0f, PLASTER1.BLu, PLASTER1.BLv},
    {3.0f, 1.0f, 2.0f, PLASTER1.BRu, PLASTER1.BRv},
    {3.0f, 2.0f, 2.0f, PLASTER1.TRu, PLASTER1.TRv},

    //Back upper (Texture)
    {1.0f, 2.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //106
    {1.0f, 1.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},
    {2.0f, 1.0f, 0.0f, PLASTER1.BRu, PLASTER1.BRv},
    {2.0f, 2.0f, 0.0f, PLASTER1.TRu, PLASTER1.TRv},

    {2.0f, 2.0f, 0.0f, PLASTER1.TLu, PLASTER1.TLv}, //110
    {2.0f, 1.0f, 0.0f, PLASTER1.BLu, PLASTER1.BLv},
    {3.0f, 1.0f, 0.0f, PLASTER1.BRu, PLASTER1.BRv},
    {3.0f, 2.0f, 0.0f, PLASTER1.TRu, PLASTER1.TRv},

    //Pole 1 (Texture)
    {0.0f, 1.0f, 0.0f, WOOD.TLu, WOOD.TLv}, //114
    {0.0f, 0.0f, 0.0f, WOOD.BLu, WOOD.BLv},
    {0.1f, 0.0f, 0.0f, WOOD.BRu, WOOD.BRv},
    {0.1f, 1.0f, 0.0f, WOOD.TRu, WOOD.TRv},

    {0.0f, 1.0f, 0.1f, WOOD.TLu, WOOD.TLv}, //118
    {0.0f, 0.0f, 0.1f, WOOD.BLu, WOOD.BLv},
    {0.1f, 0.0f, 0.1f, WOOD.BRu, WOOD.BRv},
    {0.1f, 1.0f, 0.1f, WOOD.TRu, WOOD.TRv},

    //Pole 3 (Texture)
    {0.9f, 1.0f, 0.0f, WOOD.TLu, WOOD.TLv}, //122
    {0.9f, 0.0f, 0.0f, WOOD.BLu, WOOD.BLv},
    {1.0f, 0.0f, 0.0f, WOOD.BRu, WOOD.BRv},
    {1.0f, 1.0f, 0.0f, WOOD.TRu, WOOD.TRv},

    {0.9f, 1.0f, 0.1f, WOOD.TLu, WOOD.TLv}, //126
    {0.9f, 0.0f, 0.1f, WOOD.BLu, WOOD.BLv},
    {1.0f, 0.0f, 0.1f, WOOD.BRu, WOOD.BRv},
    {1.0f, 1.0f, 0.1f, WOOD.TRu, WOOD.TRv},

    //Pole 2 (Texture)
    {0.0f, 1.0f, 0.9f, WOOD.TLu, WOOD.TLv}, //130
    {0.0f, 0.0f, 0.9f, WOOD.BLu, WOOD.BLv},
    {0.1f, 0.0f, 0.9f, WOOD.BRu, WOOD.BRv},
    {0.1f, 1.0f, 0.9f, WOOD.TRu, WOOD.TRv},

    {0.0f, 1.0f, 1.0f, WOOD.TLu, WOOD.TLv}, //134
    {0.0f, 0.0f, 1.0f, WOOD.BLu, WOOD.BLv},
    {0.1f, 0.0f, 1.0f, WOOD.BRu, WOOD.BRv},
    {0.1f, 1.0f, 1.0f, WOOD.TRu, WOOD.TRv},

};

static const int StairHouseIndices[] = {
    //Pole 1
    0, 1, 2, //Bottom
    2, 3, 0,
    4, 0, 1, //Left
    1, 5, 4,
    114, 115, 116, //Front
    116, 117, 114,
    6, 2, 3, //Right
    3, 7, 6,
    118, 119, 120, //Back
    120, 121, 118,

    //Pole 2
    8, 9, 10, //Bottom
    10, 11, 8,
    12, 8, 9, //Left
    9, 13, 12,
    130, 131, 132, //Front
    132, 133, 130,
    14, 10, 11, //Right
    11, 15, 14,
    134, 135, 136, //Back
    136, 137, 134,

    //Pole 3
    16, 17, 18, //Bottom
    18, 19, 16,
    20, 16, 17, //Left
    17, 21, 20,
    122, 123, 124, //Front
    124, 125, 122,
    22, 18, 19, //Right
    19, 23, 22,
    126, 127, 128, //Back
    128, 129, 126,

    //Landing
    24, 25, 26, //Main
    26, 27, 24,
    28, 29, 30, //Stairs
    30, 31, 28,

    //House Base
    32, 33, 34, //Bottom
    34, 35, 32,
    36, 32, 33, //Left
    33, 37, 36,
    82, 83, 84,
    84, 85, 82,
    86, 87, 88,
    88, 89, 86,
    38, 34, 35, //Right
    35, 39, 38,
    90, 91, 92,
    92, 93, 90,
    94, 95, 96,
    96, 97, 94,
    36, 37, 38, //Top
    38, 39, 36,

    //Upper Floor
    40, 41, 42, //Bottom
    42, 43, 40,
    44, 40, 41, //Left
    41, 45, 44,
    98, 99, 100,
    100, 101, 98,
    102, 103, 104,
    104, 105, 102,
    46, 42, 43, //Right
    43, 47, 46,
    106, 107, 108,
    108, 109, 106,
    110, 111, 112,
    112, 113, 110,
    44, 45, 46, //Top
    46, 47, 44,

    //Roof
    48, 49, 50, //Back
    51, 52, 53, //Front
    54, 55, 56, //Left
    56, 57, 54,
    58, 59, 60, //Right
    60, 61, 58,

    //Ground
    62, 63, 64, //Front
    64, 65, 62,
    66, 67, 68,
    68, 69, 66,
    70, 71, 72,
    72, 73, 70,
    74, 75, 76,
    76, 77, 74,
    78, 79, 80,
    80, 81, 78
};