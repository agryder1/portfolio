#pragma once
#include "Baseplate.h"

//You told us to make something cool that we're proud of. I did that.
Baseplate::Baseplate(const int length, const int numBuildings) : TOTAL_TILES(length * length), NUM_VERTICES((length + 1) * (length + 1)) {
    std::vector<Tile> workingTiles(TOTAL_TILES); //Reserve enough space to build our world
    
    //Allocate 40% of the buildings to large/complex shapes and the other 60% to little guys
    int numBigBuildings = (int)(numBuildings * 0.40f);
    int numLittleBuildings = (int)(numBuildings * 0.60f);
    std::vector<Tile> pathStarts(numBuildings); //Each building will have a driveway that we need to reserve space for
    
    int index = 0;
    //Create our tiles and vertices
    for (int i = 0; i < length; i++) {
        for (int k = 0; k < length; k++) {
            Vertex origin = {i * 1.0f, 0.0f, k * -1.0f};
 
            //Figure out where the tile is in terms of border/non-border
            //(Is the tile a corner piece, along the top row, along the right column, etc.)
            TileGridPlacement placement;

            if (index < length) { //Along top row
                if (index % length == 0) { //Along left column
                    placement = TOP_LEFT;
                }
                else if ((index + 1) % length == 0) { //Along right column
                    placement = TOP_RIGHT;
                }
                else {
                    placement = TOP_MIDDLE; //In the middle of the row
                }
            }
            else if (index >= TOTAL_TILES - length) { //Along bottom row
                if (index % length == 0) { //Along left column
                    placement = BOTTOM_LEFT;
                }
                else if ((index + 1) % length == 0) { //Along right column
                    placement = BOTTOM_RIGHT;
                }
                else {
                    placement = BOTTOM_MIDDLE; //In the middle of the row
                }
            }
            else { //Along one of the middle rows
                if (index % length == 0) { //Along the left column
                    placement = MIDDLE_LEFT;
                }
                else if ((index + 1) % length == 0) { //Along the right column
                    placement = MIDDLE_RIGHT;
                }
                else {
                    placement = MIDDLE; //In the middle of the row
                }
            } 
            //Add the tile to our working array
            workingTiles[index] = Tile(origin, placement, index); 
            index++;      
        }
    }

    //Now we need to place our buildings
    bool acceptTile = false;
    int randIndex = 0;
    int pathOffset = 0;

    //We place the large/complex buildings first because they will take up the most space
    for (int q = 0; q < numBigBuildings; q++) { //Until we have placed every building
        do {
            acceptTile = false;

            //Pick a random size within our geometry
            int buildingLength = genRandom(2, 3);
            int buildingWidth = genRandom(3, 5);

            //Set up an array to store the possible driveway locations / compass directions
            int possiblePathStarts[4];
            Direction possibleFacing[4];
            int numPossibleStarts = 0;

            //Pick a random tile that is not along the border or close enough to the edge that the building would not be able to place
            //(Skip array bounds checking, essentially)
            randIndex = genRandom(1, length - (buildingLength + 1)) + (genRandom(1, length - (buildingWidth + 1)) * length);
            Tile* possibleOrigin = &workingTiles[randIndex];

            //If we haven't already tried this tile, it's open, and it's not too close to anything we've already placed
            if (possibleOrigin->getChecked() == false && possibleOrigin->getType() == OPEN && CheckTileNeighbors(&workingTiles, length, randIndex)) {
                std::vector<int> confirmedIndices; //Prepare to place it
                confirmedIndices.push_back(randIndex);

                //There's still a chance that we won't be able to place the building because so far we've only checked the origin
                bool allGood = true;
                for (int k = 0; k < buildingLength; k++) {
                    if (!allGood) {
                        break; //Looks like this building doesn't fit.
                    }
                    for (int p = 0; p < buildingWidth; p++) {
                        int relativeIndex = randIndex + k + (p * length); //Figure out the tile's index relative to the building's origin

                        //If the tile is open and we haven't already checked it
                        if (workingTiles[relativeIndex].getType() == OPEN && workingTiles[relativeIndex].getChecked() == false) {
                            //Start checking its neighbors
                            if (k == 0 && workingTiles[relativeIndex - 1].getType() != OPEN) { //If tile to the left is full
                                workingTiles[relativeIndex - 1].setChecked(true);
                                allGood = false;
                                break;
                            }
                            else if (k == 0 && p == buildingWidth/2) { //If tile to the left is open and we're in the center of the width
                                //We might want to put a driveway there
                                possiblePathStarts[numPossibleStarts] = relativeIndex - 1;
                                possibleFacing[numPossibleStarts] = WEST;
                                numPossibleStarts++;
                            }

                            if (k == buildingLength - 1 && workingTiles[relativeIndex + 1].getType() != OPEN) { //If the tile to the right is full
                                workingTiles[relativeIndex + 1].setChecked(true);
                                allGood = false;
                                break;
                            }
                            else if (k == buildingLength - 1 && p == buildingWidth/2) { //If the tile to the right is open and we're in the center of the width
                                //Could be a good driveway
                                possiblePathStarts[numPossibleStarts] = relativeIndex + 1;
                                possibleFacing[numPossibleStarts] = EAST;
                                numPossibleStarts++;
                            }

                            if (p == 0 && workingTiles[relativeIndex - length].getType() != OPEN) { //If the tile below us is full
                                workingTiles[relativeIndex - length].setChecked(true);
                                allGood = false;
                                break;
                            }
                            else if (p == 0 && k == buildingLength/2) { //If the tile below us is open and we're in the middle of the length
                                //Prime driveway material
                                possiblePathStarts[numPossibleStarts] = relativeIndex - length;
                                possibleFacing[numPossibleStarts] = NORTH;
                                numPossibleStarts++;
                            }

                            if (p == buildingWidth - 1 && workingTiles[relativeIndex + length].getType() != OPEN) { //If the tile abous us is full
                                workingTiles[relativeIndex + length].setChecked(true);
                                allGood = false;
                                break;
                            }
                            else if (p == buildingWidth - 1 && k == buildingLength/2) { //If the tile above us is open and we're in the middle of the length
                                //Great driveway space
                                possiblePathStarts[numPossibleStarts] = relativeIndex + length;
                                possibleFacing[numPossibleStarts] = SOUTH;
                                numPossibleStarts++;
                            }

                            //If we got here, the tile is 100% open, so we pop it into our array and mark that we've checked it
                            confirmedIndices.push_back(relativeIndex);
                            workingTiles[relativeIndex].setChecked(true);
                        }
                        else {
                            //Otherwise we need to try a new origin.
                            allGood = false;
                        }
                    }
                }

                if (allGood) { //If we've successfully placed the building
                    TileType bigType; //We need to match it to its geometry
                    /*Big buildings have an offset because I built the geometry and then realized that
                      the buildings are gonna have to rotate and I built them all from (0,0,0) rather than
                      centered around it -->*/
                    if (buildingLength == 2 && buildingWidth == 3) {
                        workingTiles[confirmedIndices[0]].setOffset(0.0f, 1.0f); //<---
                        bigType = LHOUSE;
                    }
                    else if (buildingLength == 3 && buildingWidth == 2) {
                        workingTiles[confirmedIndices[0]].setOffset(1.0f, 0.0f);
                        bigType = LHOUSE;
                    }
                    else if (buildingLength == 3 && buildingWidth == 3) {
                        workingTiles[confirmedIndices[0]].setOffset(0.0f, 2.0f);
                        bigType = STAIR_HOUSE;
                    }
                    else if (buildingLength == 2 && buildingWidth == 4) {
                        workingTiles[confirmedIndices[0]].setOffset(0.0f, 1.0f);
                        bigType = COMMUNITY_HOUSE;
                    }
                    else if (buildingLength == 4 && buildingWidth == 2) {
                        workingTiles[confirmedIndices[0]].setOffset(1.0f, 0.0f);
                        bigType = COMMUNITY_HOUSE;
                    }
                    else if (buildingLength == 2 && buildingWidth == 5) {
                        workingTiles[confirmedIndices[0]].setOffset(0.0f, 1.0f);
                        bigType = LONG_STONE;
                    }
                    else if (buildingLength == 5 && buildingWidth == 2) {
                        workingTiles[confirmedIndices[0]].setOffset(1.0f, 0.0f);
                        bigType = LONG_STONE;
                    }
                    else if (buildingLength == 3 && buildingWidth == 4) {
                        workingTiles[confirmedIndices[0]].setOffset(0.0f, 2.0f);
                        bigType = HALF_HOUSE;
                    }
                    else if (buildingLength == 4 && buildingWidth == 3) {
                        workingTiles[confirmedIndices[0]].setOffset(2.0f, 0.0f);
                        bigType = HALF_HOUSE;
                    }
                    else if (buildingLength == 3 && buildingWidth == 5) {
                        bigType = CHURCH;
                    }
                    else if (buildingLength == 5 && buildingWidth == 3) {
                        bigType = CHURCH;
                    }

                    //Now we pick a driveway
                    randIndex = genRandom(0, numPossibleStarts - 1);
                    int drivewayIndex = possiblePathStarts[randIndex];
                    workingTiles[drivewayIndex].setChecked(true);
                    workingTiles[drivewayIndex].setType(PATH_START);

                    //We also need to let pathStarts know that we've placed a driveway tile
                    pathStarts[pathOffset] = workingTiles[drivewayIndex];
                    pathOffset++; //And we need to count how many we've placed so that later we can place more at the index immediately after

                    //Go through all the tiles associated with our building
                    for (int index : confirmedIndices) {
                        //Reserve them as building pieces and mark them as checked
                        workingTiles[index].setType(BIG_BUILDING_PIECE, possibleFacing[randIndex], buildingLength, buildingWidth);
                        workingTiles[index].setChecked(true);
                    }
                    
                    //We just copied over our origin which is BAD, so we change its type again
                    workingTiles[confirmedIndices[0]].setType(bigType, possibleFacing[randIndex], buildingLength, buildingWidth);
                    acceptTile = true;
                }
            }
        } while (acceptTile == false); //Keep trying to place a building until it fits
        //NOTE: THIS WILL CAUSE AN INFINTE LOOP IF THERE IS NOT ENOUGH TILES TO FIT ALL THE BUILDINGS
    }

    //Now place our little houses with a similar algorithm
    for (int p = 0; p < numLittleBuildings; p++) {
        do { //Until we find a good place to put the tile
            acceptTile = false;
            randIndex = genRandom(0, TOTAL_TILES - 1);
            Tile* possibleTile = &workingTiles[randIndex];

            //Check if the randomly selected tile and its 4 adjacent tiles are open
            if (possibleTile->getChecked() == false && possibleTile->getType() == OPEN && CheckTileNeighbors(&workingTiles, length, randIndex) && randIndex != ((TOTAL_TILES - 1) / 2)) {
                //Place a building
                acceptTile = true;
                possibleTile->setType(BUILDING);

                //Place the house's "driveway"
                switch(possibleTile->getFacing()) {
                    case (NORTH):
                        workingTiles[randIndex - length].setType(PATH_START);
                        pathStarts[p + pathOffset] = workingTiles[randIndex - length];
                    break;

                    case (EAST):
                        workingTiles[randIndex + 1].setType(PATH_START);
                        pathStarts[p + pathOffset] = workingTiles[randIndex + 1];
                    break;

                    case (SOUTH):
                        workingTiles[randIndex + length].setType(PATH_START);
                        pathStarts[p + pathOffset] = workingTiles[randIndex + length];
                    break;

                    case (WEST):
                        workingTiles[randIndex - 1].setType(PATH_START);
                        pathStarts[p + pathOffset] = workingTiles[randIndex - 1];
                    break;

                    default:
                        printf("Problem finding driveway... %d\n", possibleTile->getGridPlacement());
                    break;
                }
            }

            //Mark that we've tried this tile so that we don't waste time checking it again later
            possibleTile->setChecked(true);
        } while (acceptTile == false);
    }

    //Now we need to connect our driveways to form our roads

    Tile* source; //Where we are right now
    Vertex destOrigin; //Where we want to be
    int srcIndex;
    int choiceIndex;
    int lastSrcIndex = pathStarts[1].getIndex(); //Where we just were
    bool needToStop = false;
    std::set<int> currentPath; //Set to keep track of the current path we're tracing

    //For every driveway after the first
    for (int j = 1; j < numBuildings; j++) {
        //Figure out where it is on the grid
        destOrigin = pathStarts[j - 1].getTileOrigin();
        source = &pathStarts[j];
        needToStop = false; //If we reach something that stops/obstructs our road placement then we just give up on the path

        currentPath.clear(); //We clear currentPath everytime we choose a new path start because we're allowed to have roads overlap

        //And while the distance between where we currently are (last path we placed) and our destination is not 0
        while (source->DistanceToPoint(destOrigin) != 0 && needToStop == false) {
            srcIndex = source->getIndex(); //Figure out where we are in the working array
            needToStop = false;
            choiceIndex = 0;

            currentPath.insert(srcIndex); //Add our current tile to the places we've been

            //Figure out our tile placement so that we don't check non-existant tiles
            switch(source->getGridPlacement()) {
                //Any tile that is open and is horizontally or vertically adjacent to this tile could become a path tile 
                case(TOP_LEFT):
                    choiceIndex = FindBestPath(&workingTiles, length, "ES", srcIndex, lastSrcIndex, destOrigin);
                break;

                case (TOP_MIDDLE):
                    choiceIndex = FindBestPath(&workingTiles, length, "ESW", srcIndex, lastSrcIndex, destOrigin);
                break;

                case(TOP_RIGHT):
                    choiceIndex = FindBestPath(&workingTiles, length, "SW", srcIndex, lastSrcIndex, destOrigin);
                break;

                case(MIDDLE_LEFT):
                    choiceIndex = FindBestPath(&workingTiles, length, "NES", srcIndex, lastSrcIndex, destOrigin);
                break;

                case(MIDDLE):
                    choiceIndex = FindBestPath(&workingTiles, length, "NESW", srcIndex, lastSrcIndex, destOrigin);
                break;

                case(MIDDLE_RIGHT):
                    choiceIndex = FindBestPath(&workingTiles, length, "NSW", srcIndex, lastSrcIndex, destOrigin);
                break;

                case(BOTTOM_LEFT):
                    choiceIndex = FindBestPath(&workingTiles, length, "NE", srcIndex, lastSrcIndex, destOrigin);
                break;

                case(BOTTOM_MIDDLE):
                    choiceIndex = FindBestPath(&workingTiles, length, "NEW", srcIndex, lastSrcIndex, destOrigin);
                break;

                case(BOTTOM_RIGHT):
                    choiceIndex = FindBestPath(&workingTiles, length, "NW", srcIndex, lastSrcIndex, destOrigin);
                break;

                default:
                    printf("Problem making roads");
                break;
            }

            //If we ran into a problem placing the roads then we need to stop this path
            if (choiceIndex == -1 || choiceIndex == -2) {
                needToStop = true;
            }
            else if (currentPath.find(choiceIndex) != currentPath.end()) { //If we've already been here
                needToStop = true; //Don't start an infinite loop
            }
            else {
                //If nothing went wrong, we place a road and move onto the next driveway
                workingTiles[choiceIndex].setType(PATH);
                source = &workingTiles[choiceIndex];
                lastSrcIndex = srcIndex;
            }
        }
    }

    //Now that we know where everything will go we're ready to sort the tiles
    std::vector<Vertex> buildingVertices;
    std::vector<int> buildingIndices;
    
    std::vector<Vertex> grassNPathVertices;
    std::vector<int> grassNPathIndices;

    std::vector<Vertex> bigBuildingVertices;
    std::vector<int> bigBuildingIndices;
    
    //These variables keep track of how many vertices we've created so far
    int buildNextIndex = 0;
    int grassNPathNextIndex = 0;
    int bigNextIndex = 0;

    //These variables keep track of how many indices we've created so far
    int numBuildIndices = 0;
    int numGrassNPathIndices = 0;
    int numBigIndices = 0;

    //Go through our working array and divide the tiles into smaller arrays that we can pass to our shaders
    for (int r = 0; r < TOTAL_TILES; r++) {
        Tile tileToSort = workingTiles[r];
        Vertex tileOrigin = tileToSort.getTileOrigin();
        int vCount = 0;
        
        //We do this by checking the building's type and then iterating through the associated geometry
        if (tileToSort.getType() == BUILDING) {
             //Buildings can rotate so we need to handle that
            float angle;
            switch (tileToSort.getFacing()) {
                case(NORTH):
                    angle = -90.0f;
                break;

                case(EAST):
                    angle = -180.0f;
                break;

                case(SOUTH):
                    angle = 90.0f;
                break;

                case(WEST):
                    angle = 0.0f;
                break;
            }

            int coinFlip = genRandom(0, 1); //Little houses have two possible geometry sets

            if (coinFlip == 0) {
                for (Vertex vertex : House1Vertices) {
                    //Make sure we're rotating around the center
                    glm::vec4 point = glm::vec4(vertex.x - 0.5f, vertex.y, vertex.z - 0.5f, 1.0f);
                    //Make a rotation matrix
                    glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
                    glm::vec4 rotatedPoint = rotation * point; //Rotate our vertex
                    //Line our vertex up with where it is supposed to be on the tile and add it to the array that we pass to the shader later
                    buildingVertices.push_back({rotatedPoint.x + 0.5f + tileOrigin.x, rotatedPoint.y + tileOrigin.y, rotatedPoint.z + 0.5f + tileOrigin.z, vertex.u, vertex.v});
                    vCount++; //Keep track of how many vertices were just added
                }
                
                //Iterate through our indices and add them to our array
                for (int index : House1Indices) {
                    buildingIndices.push_back(index + buildNextIndex);
                    numBuildIndices++;
                }
            }
            else { //Exact same process
                for (Vertex vertex : House2Vertices) {
                    glm::vec4 point = glm::vec4(vertex.x - 0.5f, vertex.y, vertex.z - 0.5f, 1.0f);
                    glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
                    glm::vec4 rotatedPoint = rotation * point;
                    buildingVertices.push_back({rotatedPoint.x + 0.5f + tileOrigin.x, rotatedPoint.y + tileOrigin.y, rotatedPoint.z + 0.5f + tileOrigin.z, vertex.u, vertex.v});
                    vCount++;
                }
                
                for (int index : House2Indices) {
                    buildingIndices.push_back(index + buildNextIndex);
                    numBuildIndices++;
                }
            }

            buildNextIndex += vCount; //Update our vertex count
        }
        else if (tileToSort.getType() == LHOUSE) {
            //BIG Buildings
            glm::vec2 offset = tileToSort.getOffset();

            for (Vertex vertex : LHouseVertices) {
                bigBuildingVertices.push_back({vertex.x + tileOrigin.x - offset.x, vertex.y, vertex.z + tileOrigin.z - offset.y, vertex.u, vertex.v});
                vCount++;
            }

            for (int index : LHouseIndices) {
                bigBuildingIndices.push_back(index + bigNextIndex);
                numBigIndices++;
            }

            bigNextIndex += vCount;
        }
        else if (tileToSort.getType() == COMMUNITY_HOUSE) {
            //BIG Buildings
            glm::vec2 offset = tileToSort.getOffset();

            for (Vertex vertex : CommunityHouseVertices) {
                bigBuildingVertices.push_back({vertex.x + tileOrigin.x - offset.x, vertex.y, vertex.z + tileOrigin.z - offset.y, vertex.u, vertex.v});
                vCount++;
            }

            for (int index : CommunityHouseIndices) {
                bigBuildingIndices.push_back(index + bigNextIndex);
                numBigIndices++;
            }

            bigNextIndex += vCount;
        }
        else if (tileToSort.getType() == STAIR_HOUSE) {
            //BIG Buildings
            glm::vec2 offset = tileToSort.getOffset();

            for (Vertex vertex : StairHouseVertices) {
                bigBuildingVertices.push_back({vertex.x + tileOrigin.x - offset.x, vertex.y, vertex.z + tileOrigin.z - offset.y, vertex.u, vertex.v});
                vCount++;
            }

            for (int index : StairHouseIndices) {
                bigBuildingIndices.push_back(index + bigNextIndex);
                numBigIndices++;
            }

            bigNextIndex += vCount;
        }
        else if (tileToSort.getType() == LONG_STONE) {
            //BIG Buildings
            glm::vec2 offset = tileToSort.getOffset();

            for (Vertex vertex : LongStoneVertices) {
                bigBuildingVertices.push_back({vertex.x + tileOrigin.x - offset.x, vertex.y, vertex.z + tileOrigin.z - offset.y, vertex.u, vertex.v});
                vCount++;
            }

            for (int index : LongStoneIndices) {
                bigBuildingIndices.push_back(index + bigNextIndex);
                numBigIndices++;
            }

            bigNextIndex += vCount;
        }
        else if (tileToSort.getType() == HALF_HOUSE) {
            //BIG Buildings
            glm::vec2 offset = tileToSort.getOffset();

            for (Vertex vertex : HalfHouseVertices) {
                bigBuildingVertices.push_back({vertex.x + tileOrigin.x - offset.x, vertex.y, vertex.z + tileOrigin.z - offset.y, vertex.u, vertex.v});
                vCount++;
            }

            for (int index : HalfHouseIndices) {
                bigBuildingIndices.push_back(index + bigNextIndex);
                numBigIndices++;
            }

            bigNextIndex += vCount;
        }
        else if (tileToSort.getType() == CHURCH) {
            //BIG Buildings

            for (Vertex vertex : ChurchVertices) {
                glm::vec4 point = glm::vec4(vertex.x - 0.5f, vertex.y, vertex.z - 0.5f, 1.0f);
                glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
                glm::vec4 rotatedPoint = rotation * point;
                bigBuildingVertices.push_back({rotatedPoint.x + 0.5f + tileOrigin.x, rotatedPoint.y + tileOrigin.y, rotatedPoint.z + 0.5f + tileOrigin.z, vertex.u, vertex.v});
                vCount++;
            }

            for (int index : ChurchIndices) {
                bigBuildingIndices.push_back(index + bigNextIndex);
                numBigIndices++;
            }

            bigNextIndex += vCount;
        }
        else if (tileToSort.getType() == BIG_BUILDING_PIECE) {
            //Big building pieces don't actually have geometry because they sit directly below a building, so we just skip 'em
            continue;
        }
        else if (tileToSort.getType() == PATH || tileToSort.getType() == PATH_START) {
            //Paths
            for (Vertex vertex : PathVertices) {
                grassNPathVertices.push_back({vertex.x + tileOrigin.x, vertex.y + tileOrigin.y, vertex.z + tileOrigin.z, vertex.u, vertex.v});
                vCount++;
            }

            for (int index : PathIndices) {
                grassNPathIndices.push_back(index + grassNPathNextIndex);
                numGrassNPathIndices++;
            }

            grassNPathNextIndex += vCount;
        }
        else {
            //Grass has three different geometry sets it can choose from
            int flipATriange = genRandom(0, 2);

            if (flipATriange == 0) {
                for (Vertex vertex : Grass1Vertices) {
                    grassNPathVertices.push_back({vertex.x + tileOrigin.x, vertex.y + tileOrigin.y, vertex.z + tileOrigin.z, vertex.u, vertex.v});
                    vCount++;
                }
            }
            else if (flipATriange == 1) {
                for (Vertex vertex : Grass2Vertices) {
                    grassNPathVertices.push_back({vertex.x + tileOrigin.x, vertex.y + tileOrigin.y, vertex.z + tileOrigin.z, vertex.u, vertex.v});
                    vCount++;
                }
            }
            else {
                for (Vertex vertex : Grass3Vertices) {
                    grassNPathVertices.push_back({vertex.x + tileOrigin.x, vertex.y + tileOrigin.y, vertex.z + tileOrigin.z, vertex.u, vertex.v});
                    vCount++;
                }
            }

            for (int index : GrassIndices) {
                grassNPathIndices.push_back(index + grassNPathNextIndex);
                numGrassNPathIndices++;
            }

            grassNPathNextIndex += vCount;
        }
    }

    //Update our number variables so that we know how many indices we've passed each element array buffer
    this->numBuildIndices = numBuildIndices;
    this->numBigIndices = numBigIndices;
    this->numGrassNPathIndices = numGrassNPathIndices;

    //Create our texture
    this->allTextures = wolf::TextureManager::CreateTexture("data/Textures.png");
    this->allTextures->SetFilterMode(wolf::Texture::FM_LinearMipmap, wolf::Texture::FM_Linear);
    this->allTextures->SetWrapMode(wolf::Texture::WM_Repeat);
    this->allTextures->Bind(0); //We only have the one, so we only need to bind it once
    
    //Create our shaders
    this->buildingShader = wolf::LoadShaders("data/building.vsh", "data/building.fsh");

    //Building
    glGenVertexArrays(1, &this->B_VAO);
    glGenBuffers(1, &this->B_VBO);
    glGenBuffers(1, &this->B_EBO);

    glBindVertexArray(this->B_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, this->B_VBO);
    glBufferData(GL_ARRAY_BUFFER, buildingVertices.size() * sizeof(Vertex), buildingVertices.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->B_EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, buildingIndices.size() * sizeof(GLuint), buildingIndices.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_TRUE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    this->bigShader = wolf::LoadShaders("data/big.vsh", "data/big.fsh");

    //Big Buildings
    glGenVertexArrays(1, &this->BB_VAO);
    glGenBuffers(1, &this->BB_VBO);
    glGenBuffers(1, &this->BB_EBO);

    glBindVertexArray(this->BB_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, this->BB_VBO);
    glBufferData(GL_ARRAY_BUFFER, bigBuildingVertices.size() * sizeof(Vertex), bigBuildingVertices.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->BB_EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, bigBuildingIndices.size() * sizeof(GLuint), bigBuildingIndices.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_TRUE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    this->grassNPathShader = wolf::LoadShaders("data/grassNpath.vsh", "data/grassNpath.fsh");

    //Grass and Paths
    glGenVertexArrays(1, &this->GNP_VAO);
    glGenBuffers(1, &this->GNP_VBO);
    glGenBuffers(1, &this->GNP_EBO);

    glBindVertexArray(this->GNP_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, this->GNP_VBO);
    glBufferData(GL_ARRAY_BUFFER, grassNPathVertices.size() * sizeof(Vertex), grassNPathVertices.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->GNP_EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, grassNPathIndices.size() * sizeof(GLuint), grassNPathIndices.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    glEnableVertexAttribArray(0);
    
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_TRUE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

}

//This method chooses which of our neighboring tiles will take us closer to our destination
int Baseplate::FindBestPath(std::vector<Tile>* tileset, const int length, const char* possibleLocs, const int srcIndex, const int lastSrcIndex, const Vertex destOrigin) {

    TileType tileType;
    int bestOption = -3;
    int min = INT_MAX;
    int distance;
    int checkIndex;
    bool foundATile = false;
    bool reachedTheEnd = false;

    //We check each compass direction (all adjacent tiles)
    /*Depending on a tile's gridplacement, it can only travel in
      certain compass directions, so rather than making a big switch
      case with every gridplacement possibility (again) I use a string
      to 'flag' which directions are possible*/

    //north
    if (strchr(possibleLocs, 'N') != NULL) {
        checkIndex = srcIndex - length; //Check directly above
        tileType = (*tileset)[checkIndex].getType(); //Is it open?
        distance = (*tileset)[checkIndex].DistanceToPoint(destOrigin); //How far away from the destination is it?

        //If the tile is open (or we're overlapping a road), it's the best option so far, and it isn't the last tile we visited
        if ((tileType == OPEN || tileType == PATH) && distance < min && lastSrcIndex != checkIndex) {
            bestOption = checkIndex; //Then this might be the tile we choose!
            min = distance;
            foundATile = true;
        }
        else if (tileType == PATH_START && lastSrcIndex != checkIndex) { //If we've gone in a circle and ended up back where we started
            reachedTheEnd = true; //Then it doesn't matter if there's another option -- it will never lead anywhere.
        }
    }
    
    //Same algorithm as North
    //east
    if (strchr(possibleLocs, 'E') != NULL) {
        checkIndex = srcIndex + 1;
        tileType = (*tileset)[checkIndex].getType();
        distance = (*tileset)[checkIndex].DistanceToPoint(destOrigin);

        if ((tileType == OPEN || tileType == PATH) && distance < min && lastSrcIndex != checkIndex) {
            bestOption = checkIndex;
            min = distance;
            foundATile = true;
        }
        else if (tileType == PATH_START && lastSrcIndex != checkIndex) {
            reachedTheEnd = true;
        }
    }

    //south
    if (strchr(possibleLocs, 'S') != NULL) {
        checkIndex = srcIndex + length;
        tileType = (*tileset)[checkIndex].getType();
        distance = (*tileset)[checkIndex].DistanceToPoint(destOrigin);

        if ((tileType == OPEN || tileType == PATH) && distance < min && lastSrcIndex != checkIndex) {
            bestOption = checkIndex;
            min = distance;
            foundATile = true;
        }
        else if (tileType == PATH_START && lastSrcIndex != checkIndex) {
            reachedTheEnd = true;
        }
    }

    //west
    if (strchr(possibleLocs, 'W') != NULL) {
        checkIndex = srcIndex - 1;
        tileType = (*tileset)[checkIndex].getType();
        distance = (*tileset)[checkIndex].DistanceToPoint(destOrigin);

        if ((tileType == OPEN || tileType == PATH) && distance < min && lastSrcIndex != checkIndex) {
            bestOption = checkIndex;
            min = distance;
            foundATile = true;
        }
        else if (tileType == PATH_START && lastSrcIndex != checkIndex) {
            reachedTheEnd = true;
        }
    }

    //NOTE: There are two different error codes because I'm hoping to work on this again in the future and
    //find a way to eliminate the circles
    if (reachedTheEnd) {
        //If we've gone in a circle
        return -1;
    }
    else if (foundATile) {
        //Return the best tile we can
        return bestOption;
    }
    else {
        //If we can't find an open tile
        return -2;
    }
}

//This method checks if the adjacent tiles are open
bool Baseplate::CheckTileNeighbors(std::vector<Tile>* tileset, const int length, const int index) {
    TileGridPlacement gridPlacement = (*tileset)[index].getGridPlacement();

    //We have to know the tile's gridplacemet to make sure that we don't try to check a tile that doesn't exist (go beyond the array's bounds)
    switch(gridPlacement) {
        case(TOP_LEFT): //0
            if (((*tileset)[index + 1].getType() == OPEN || (*tileset)[index + 1].getType() == PATH_START) &&
                ((*tileset)[index + length].getType() == OPEN || (*tileset)[index + length].getType() == PATH_START)) {
                return true;
            }
        break;

        case (TOP_MIDDLE): //1
            if (((*tileset)[index - 1].getType() == OPEN || (*tileset)[index - 1].getType() == PATH_START) && 
                ((*tileset)[index + 1].getType() == OPEN || (*tileset)[index + 1].getType() == PATH_START) &&
                ((*tileset)[index + length].getType() == OPEN || (*tileset)[index + length].getType() == PATH_START)) {
                return true;
            }
        break;

        case(TOP_RIGHT): //2
            if (((*tileset)[index - 1].getType() == OPEN || (*tileset)[index - 1].getType() == PATH_START) &&
                ((*tileset)[index + length].getType() == OPEN || (*tileset)[index + length].getType() == PATH_START)) {
                return true;
            }
        break;

        case(MIDDLE_LEFT): //3
            if (((*tileset)[index + 1].getType() == OPEN || (*tileset)[index + 1].getType() == PATH_START) &&
                ((*tileset)[index - length].getType() == OPEN || (*tileset)[index - length].getType() == PATH_START) &&
                ((*tileset)[index + length].getType() == OPEN || (*tileset)[index + length].getType() == PATH_START)) {
                return true;
            }
        break;

        case(MIDDLE): //4
            if (((*tileset)[index - 1].getType() == OPEN || (*tileset)[index - 1].getType() == PATH_START) &&
                ((*tileset)[index + 1].getType() == OPEN || (*tileset)[index + 1].getType() == PATH_START) &&
                ((*tileset)[index - length].getType() == OPEN || (*tileset)[index - length].getType() == PATH_START) && 
                ((*tileset)[index + length].getType() == OPEN || (*tileset)[index + length].getType() == OPEN)) {
                return true;
            }
        break;

        case(MIDDLE_RIGHT): //5
            if (((*tileset)[index - 1].getType() == OPEN || (*tileset)[index - 1].getType() == PATH_START) &&
                ((*tileset)[index - length].getType() == OPEN || (*tileset)[index - length].getType() == PATH_START) &&
                ((*tileset)[index + length].getType() == OPEN || (*tileset)[index + length].getType() == PATH_START)) {
                return true;
            }
        break;

        case(BOTTOM_LEFT): //6
            if (((*tileset)[index + 1].getType() == OPEN || (*tileset)[index + 1].getType() == PATH_START) &&
                ((*tileset)[index - length].getType() == OPEN || (*tileset)[index - length].getType() == PATH_START)) {
                return true;
            }
        break;

        case(BOTTOM_MIDDLE): //7
            if (((*tileset)[index - 1].getType() == OPEN || (*tileset)[index - 1].getType() == PATH_START) &&
                ((*tileset)[index + 1].getType() == OPEN || (*tileset)[index + 1].getType() == PATH_START) &&
                ((*tileset)[index - length].getType() == OPEN || (*tileset)[index - length].getType() == PATH_START)) {
                return true; 
            }
        break;

        case(BOTTOM_RIGHT): //8
            if (((*tileset)[index - 1].getType() == OPEN || (*tileset)[index - 1].getType() == PATH_START) &&
                ((*tileset)[index - length].getType() == OPEN || (*tileset)[index - length].getType() == PATH_START)) {
                return true;
            }
        break;

        default:
            printf("Problem in CheckTileNeighbors switch/case.");
        break;
    }

    //If we ran into an issue, than this tile isn't a viable option
    return false;
}

//Render everything!
void Baseplate::Render(const glm::mat4 projection, const glm::mat4 view, const glm::mat4 world) {
    //Grass and Paths
    //Get our shader ready
    glUseProgram(this->grassNPathShader);

    //Set uniforms
    glUniformMatrix4fv(glGetUniformLocation(this->grassNPathShader, "u_projection"), 1, GL_FALSE, glm::value_ptr(projection));
    glUniformMatrix4fv(glGetUniformLocation(this->grassNPathShader, "u_view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(this->grassNPathShader, "u_world"), 1, GL_FALSE, glm::value_ptr(world));
    glUniform1i(glGetUniformLocation(this->grassNPathShader, "theClaw"), 0);
    
    //Bind VAO
    glBindVertexArray(this->GNP_VAO);
    
    //Draw!
    glDrawElements(GL_TRIANGLES, this->numGrassNPathIndices, GL_UNSIGNED_INT, 0);

    //Buildings
    //Get our shader ready
    glUseProgram(this->buildingShader);

    //Set uniforms
    glUniformMatrix4fv(glGetUniformLocation(this->buildingShader, "u_projection"), 1, GL_FALSE, glm::value_ptr(projection));
    glUniformMatrix4fv(glGetUniformLocation(this->buildingShader, "u_view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(this->buildingShader, "u_world"), 1, GL_FALSE, glm::value_ptr(world));
    glUniform1i(glGetUniformLocation(this->buildingShader, "theClaw"), 0);
    
    //Bind VAO
    glBindVertexArray(this->B_VAO);
    
    //Draw!
    glDrawElements(GL_TRIANGLES, this->numBuildIndices, GL_UNSIGNED_INT, 0);

    //Big buildings
    //Get shader ready
    glUseProgram(this->bigShader);

    //Set uniforms
    glUniformMatrix4fv(glGetUniformLocation(this->bigShader, "u_projection"), 1, GL_FALSE, glm::value_ptr(projection));
    glUniformMatrix4fv(glGetUniformLocation(this->bigShader, "u_view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(this->bigShader, "u_world"), 1, GL_FALSE, glm::value_ptr(world));
    glUniform1i(glGetUniformLocation(this->bigShader, "theClaw"), 0);
    
    //Bind VAO
    glBindVertexArray(this->BB_VAO);
    
    //Draw!
    glDrawElements(GL_TRIANGLES, this->numBigIndices, GL_UNSIGNED_INT, 0);

};

//Clean up!
Baseplate::~Baseplate() {
    glDeleteVertexArrays(1, &B_VAO);
    glDeleteBuffers(1, &B_VBO);
    glDeleteBuffers(1, &B_EBO);

    glDeleteVertexArrays(1, &BB_VAO);
    glDeleteBuffers(1, &BB_VBO);
    glDeleteBuffers(1, &BB_EBO);

    glDeleteVertexArrays(1, &GNP_VAO);
    glDeleteBuffers(1, &GNP_VBO);
    glDeleteBuffers(1, &GNP_EBO);

    wolf::TextureManager::DestroyTexture(this->allTextures);
};