#pragma once
#include "Tile.h"

Tile::Tile() {/*Cry*/};

//Create a tile!
Tile::Tile(const Vertex tileOrigin, const TileGridPlacement placement, const int index) {
    this->origin = tileOrigin;
    this->gridPlacement = placement;
    this->tilesetIndex = index;
    this->tileType = OPEN;
};

void Tile::setType(const TileType newType) {
    this->tileType = newType;

    //If we've just placed a building then we're gonna want to figure out what direction it will face
    if (newType == BUILDING) {
        //NORTH = 0
        //EAST = 1
        //SOUTH = 2
        //WEST = 3
        Direction NSW[3] = {NORTH, SOUTH, WEST};
        Direction NEW[3] = {NORTH, EAST, WEST};
        Direction NW[2] = {NORTH, WEST};
        
        //Based on where the building is on the grid, some compass directions may/may not be possible
        switch(gridPlacement) {
            case(TOP_LEFT):
                this->facing = genRandom(EAST, SOUTH);
            break;

            case (TOP_MIDDLE):
                this->facing = genRandom(EAST, WEST);
            break;

            case(TOP_RIGHT):
                this->facing = genRandom(SOUTH, WEST);
            break;

            case(MIDDLE_LEFT):
                this->facing = genRandom(NORTH, SOUTH);
            break;

            case(MIDDLE):
                this->facing = genRandom(NORTH, WEST);
            break;

            case(MIDDLE_RIGHT):
                this->facing = pickDirection(NSW, 3);
            break;

            case(BOTTOM_LEFT):
                this->facing = genRandom(NORTH, EAST);
            break;

            case(BOTTOM_MIDDLE):
                this->facing = pickDirection(NEW, 3);
            break;

            case(BOTTOM_RIGHT):
                this->facing = pickDirection(NW, 2);
            break;

            default:
                printf("Problem in Tile::Tile switch/case.");
            break;
        }
    }
};

//We have a secondary setType method for when we're creating a big building
void Tile::setType(const TileType newType, const Direction newFacing, const int length, const int width) {
    //We already have the facing figured out
    this->tileType = newType;
    this->facing = newFacing;
    //We need to know how big this building is
    this->buildingLength = length;
    this->buildingWidth = width;
};

