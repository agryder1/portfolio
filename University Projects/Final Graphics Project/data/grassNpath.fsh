uniform sampler2D theClaw;
in vec2 texCoords;

out vec4 PixelColor;

void main()
{
    PixelColor = texture(theClaw, texCoords);
}
