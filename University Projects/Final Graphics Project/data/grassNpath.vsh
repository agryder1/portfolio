uniform mat4 u_projection;
uniform mat4 u_view;
uniform mat4 u_world;

in vec3 position;
in vec2 v_uv;

out vec2 texCoords;

void main()
{
    gl_Position = u_projection * u_view * u_world * vec4(position, 1);
    texCoords = v_uv;
}