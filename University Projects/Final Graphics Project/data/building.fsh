out vec4 PixelColor;
in vec2 texCoords;
uniform sampler2D theClaw;

void main()
{
    PixelColor = texture(theClaw, texCoords);
}