uniform sampler2D sampler;

in vec4 v_color;
in vec2 v_uv;

out vec4 PixelColor;

void main()
{
    PixelColor = texture(sampler, v_uv) * v_color;
}
