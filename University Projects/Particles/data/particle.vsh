uniform mat4 projection;
uniform mat4 view;
uniform mat4 world;

layout(location = 0) in vec3 a_vertices;
layout(location = 1) in vec3 a_center;
layout(location = 2) in vec3 a_size;
layout(location = 3) in vec4 a_color;
layout(location = 4) in vec2 a_uv;

out vec4 v_color;
out vec2 v_uv;

void main()
{
    vec3 cameraRightWorld = {view[0][0], view[1][0], view[2][0]};
    vec3 cameraUpWorld = {view[0][1], view[1][1], view[2][1]};
    vec3 particlePos = a_center + cameraRightWorld * a_vertices.x * a_size.x + cameraUpWorld * a_vertices.y * a_size.y;
    gl_Position = projection * view * world * vec4(particlePos, 1.0);
	v_color = a_color;
    v_uv = a_uv;
}
