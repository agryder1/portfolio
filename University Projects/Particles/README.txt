WASD and mouse - moves the camera
Q - switches between effects (stops the active effect and plays the next one)
I (i) - restarts the active effect
O (oh) - stops the active effect
P - pauses/plays the active effect