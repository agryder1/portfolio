#pragma once
#include "Effect.h"

// Parent class of all Affectors
class Affector {
public:
    Affector() {};
    virtual ~Affector() {};
    virtual void Apply(float p_fDelta, Particle* p_pParticle) = 0;
};