#pragma once
#include "general.h"
#include "Emitter.h"
#include "tinyxml/tinyxml.h"
#include "tinyxml/tinystr.h"

// This class operates as a holder and general controller for a set of Emitters
class Effect {
private:
    // Vector to hold a set of emitters
    std::vector<Emitter*> m_vEmitters;

    // Effect name
    std::string m_strName;

    // Transform
    glm::vec3 m_vScale = glm::vec3(1.0f, 1.0f, 1.0f);
    glm::vec3 m_vRotation = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 m_vPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::mat4 m_m4Transform = glm::mat4(1.0f);
    bool dirty = false; //Flag to prevent unnecessary transform recalculations

public:
    // Constructor and Destructor
    Effect(const std::string& p_strName);
    ~Effect();
    
    // Update, render, and transform manipulation
    void Update(float deltaTime);
    void Render(const glm::mat4& p_mProjection, const glm::mat4& p_mView);
    void SetTransform(const glm::mat4& p_m4Transform) { m_m4Transform = p_m4Transform; };

    // Setter/getter for the effect's name
    void SetName(const std::string& p_strNewName) {m_strName = p_strNewName;};
    std::string GetName() {return m_strName;};

    // Creates an effect using the passed in XML file
    static Effect* CreateWithXMLFile(const std::string& p_strFilePath);

    // Adds an emitter to the list
    bool AddEmitter(Emitter* p_pEmitter) {
        // Check if we have this emitter already
        std::vector<Emitter*>::iterator it = std::find(m_vEmitters.begin(), m_vEmitters.end(), p_pEmitter);
        if (it == m_vEmitters.end()) { // And only add it it we don't
            m_vEmitters.push_back(p_pEmitter);
            return true;
        }

        // Otherwise, we already have it
        return false;
    };

    // Removes an emitter from the list
    bool RemoveEmitter(Emitter* p_pEmitter) {
        // Check if we have the emitter
        std::vector<Emitter*>::iterator it = std::find(m_vEmitters.begin(), m_vEmitters.end(), p_pEmitter);
        if (it != m_vEmitters.end()) { // And if we do, delete it
            m_vEmitters.erase(it);
            return true;
        }

        // Otherwise, don't worry about it
        return false;
    };

    // Methods to control attached Emitters
    void Play();
    void Pause();
    void Stop();
    void Restart();
    
    // Transform Manipulation
    //Set our transform variables directly (overwrites old transform)
    void SetScale(const glm::vec3& newScale) {m_vScale = newScale; dirty = true;};
    void SetRotation(const glm::vec3& newRotation) {m_vRotation = newRotation; dirty = true;};
    void SetPosition(const glm::vec3& newPosition) {m_vPosition = newPosition; dirty = true;};

    //Perform a transformation based on our current transform (adjusts current transform)
    void Scale(const glm::vec3& scale) {m_vScale *= scale; dirty = true;};
    void Rotation(const glm::vec3& rotate) {m_vRotation += rotate; dirty = true;};
    void Translate(const glm::vec3& position) {m_vPosition += position; dirty = true;};

    // Get our transform
    glm::mat4 GetTransform() {
        if (dirty) { //Only recalculate if necessary
            m_m4Transform = glm::translate(glm::mat4(1.0f), m_vPosition);
            m_m4Transform = m_m4Transform * glm::mat4_cast(glm::quat(glm::vec3(glm::radians(m_vRotation.x), glm::radians(m_vRotation.y), glm::radians(m_vRotation.z))));
            m_m4Transform = glm::scale(m_m4Transform, m_vScale);

            dirty = false;
        }
        return m_m4Transform;
    }

};