#pragma once
#include "..\thirdparty\glew\include\GL\glew.h"
#include "..\thirdparty\glfw\include\GLFW\glfw3.h"
#include "..\thirdparty\glfw\include\GLFW\glfw3native.h"
#include "..\thirdparty\glm\glm\glm.hpp"
#include "..\wolf\wolf.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Array to hold commonly-used colors (I just like having them on-hand)
const glm::vec4 colors[10] = {
	{1.0f, 0.0f, 0.0f, 1.0f},	 // 0: red
	{1.0f, 0.65f, 0.0f, 1.0f},	 // 1: orange
	{1.0f, 1.0f, 0.0f, 1.0f},	 // 2: yellow
	{0.0f, 1.0f, 0.0f, 1.0f},	 // 3: green
	{0.0f, 1.0f, 1.0f, 1.0f},	 // 4: cyan
	{0.0f, 0.0f, 1.0f, 1.0f},	 // 5: blue
	{0.63f, 0.13f, 0.94f, 1.0f}, // 6: purple
	{1.0f, 0.0f, 0.50f, 1.0f},	 // 7: pink
	{1.0f, 1.0f, 1.0f, 1.0f},	 // 8: white
	{0.0f, 0.0f, 0.0f, 1.0f},	 // 9: black
};

struct Vertex {
    GLfloat x, y, z;
};

struct Plane {
    GLfloat a, b, c, d;
    void Normalize() { //Planes need to be normalized!
        float length = glm::length(glm::vec3(a, b, c));
        a = a/length;
        b = b/length;
        c = c/length;
        d = d/length;
    }
};

//Used to hold Camera frustum(s)
struct Frustum {
    Plane planes[6];
};

//Pseudo random number generators
int genRandomInt(int lower, int upper);
float genRandomFloat(float lower, float upper);
glm::vec2 genRandomVec2(const glm::vec2& min, const glm::vec2& max);
glm::vec3 genRandomVec3(const glm::vec3& min, const glm::vec3& max);
glm::vec4 genRandomVec4(const glm::vec4& min, const glm::vec4& max);
