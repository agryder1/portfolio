#pragma once
#include "Affector.h"
#include "general.h"

// Enum for the different types of fading this affector supports
enum FadeMode {
    CONSTANT_FADE,
    RANDOM_FADE,
    FADE_OUT_OVER_LIFETIME,
    FADE_IN_OVER_LIFETIME
};

// This class applies a fade effect to a particle
class FadeAffector : public Affector {
private:
    FadeMode m_enMode;          // Wjat fade mode should it use?
    float m_fFadeMin = 0.0f;    // Minimum if using a range
    float m_fFadeMax = 0.0f;    // Maximum if using a range
    float m_fFadeAmt = 0.0f;    // Fade amount if using a constant fade

public:
    // Contructors that correspond to the required information for each mode
    // Note that any mode can be used with any constructor, the required parameters just need to
    // be set using the corresponding getters, later
    FadeAffector(FadeMode p_enFadeMode) : m_enMode(p_enFadeMode), Affector() {};
    FadeAffector(FadeMode p_enFadeMode, float p_fFadeAmt) : m_enMode(p_enFadeMode), m_fFadeAmt(p_fFadeAmt), Affector() {};
    FadeAffector(FadeMode p_enFadeMode, float p_fFadeMin, float p_fFadeMax) : m_enMode(p_enFadeMode), m_fFadeMin(p_fFadeMin), m_fFadeMax(p_fFadeMax), Affector() {};

    virtual ~FadeAffector() {};

    // Apply the chose fade mode to a particle
    virtual void Apply(float p_fDelta, Particle* p_pParticle) {
        switch (m_enMode) {
            case CONSTANT_FADE : {
                // Constant amount just continuously applies the fade
                p_pParticle->fade += m_fFadeAmt * p_fDelta;
            }
            break;

            case RANDOM_FADE : {
                // Random applies a random fade amount each time this is called
                p_pParticle->fade += genRandomFloat(m_fFadeMin, m_fFadeMax) * p_fDelta;
            }
            break;

            case FADE_OUT_OVER_LIFETIME : {
                // Fade out over lifetime applies more of a fade as the particle gets older
                p_pParticle->fade = 1.0f - p_pParticle->age / p_pParticle->lifetime;
            }
            break;

            case FADE_IN_OVER_LIFETIME : {
                // Fade in over lifetime applies less of a fade as the particle gets older
                p_pParticle->fade = 0.0f + p_pParticle->age / p_pParticle->lifetime;
            }
        }
    };

    // Setters for each of the Affector's attributes
    void SetFadeMode(FadeMode p_enFadeMode) {m_enMode = p_enFadeMode;};
    void SetFadeAmt(float p_fAmt) {m_fFadeAmt = p_fAmt;};
    void SetFadeRange(float p_fMin, float p_fMax) {m_fFadeMin = p_fMin; m_fFadeMax = p_fMax;};
};