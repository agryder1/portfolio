#pragma once
#include "Affector.h"

// This class applies a particle's velocity to it over time
class AddVelAffector : public Affector {
public:
    AddVelAffector() : Affector() {};
    virtual ~AddVelAffector() {};
    virtual void Apply(float p_fDelta, Particle* p_pParticle) {
        p_pParticle->position += p_pParticle->velocity * p_fDelta;
    };
};