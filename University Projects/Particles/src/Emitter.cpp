#include "Emitter.h"
#include "Affector.h"
#include "AddVelAffector.h"
#include "FadeAffector.h"
#include "ScaleAffector.h"
#include "OscillatingVelAffector.h"

// Particle Geometry
const Vertex Emitter::m_fParticleGeometry[] = {
	{-0.5f, 0.5f, 0.0f},	// Top Left
    {-0.5f, -0.5f, 0.0f}, 	// Bottom Left
    {0.5f, -0.5f, 0.0f}, 	// Bottom Right
    {0.5f, -0.5f, 0.0f}, 	// Bottom Right
    {0.5f, 0.5f, 0.0f}, 	// Top Right
    {-0.5f, 0.5f, 0.0f} 	// Top Left
};

// Texture coordinates
const glm::vec2 Emitter::m_fTextureCoords[] = {
	{0.0f, 0.0f},	// Top Left
	{0.0f, 1.0f}, 	// Bottom Left
	{1.0f, 1.0f}, 	// Bottom Right
	{1.0f, 1.0f}, 	// Bottom Right
	{1.0f, 0.0f}, 	// Top Right
	{0.0f, 0.0f}	// Top Left
};

// Emitter ID
int Emitter::nextEmitterID = 0;

// Constructor with a set duration
Emitter::Emitter(const std::string& p_strName, const std::string& p_strTexturePath, EmissionMode p_enMode, BlendMode p_enBlendMode, int p_iMaxParticles, float p_fDuration) 
: m_iID(nextEmitterID), m_strName(p_strName), m_strTexturePath(p_strTexturePath), m_enMode(p_enMode), m_enBlendMode(p_enBlendMode), m_iMaxParticles(p_iMaxParticles), m_fDuration(p_fDuration)
{
	m_bRandomDuration = false;
	InitEmitter();
}

// Constructor with a duration range
Emitter::Emitter(const std::string& p_strName, const std::string& p_strTexturePath, EmissionMode p_enMode, BlendMode p_enBlendMode, int p_iMaxParticles, float p_fMinDuration, float p_fMaxDuration) :
m_iID(nextEmitterID), m_strName(p_strName), m_strTexturePath(p_strTexturePath), m_enMode(p_enMode), m_enBlendMode(p_enBlendMode), m_iMaxParticles(p_iMaxParticles), m_fDurationMin(p_fMinDuration), m_fDurationMax(p_fMaxDuration)
{
	m_bRandomDuration = true;
	m_fDuration = genRandomFloat(p_fMinDuration, p_fMaxDuration);
	InitEmitter();
}

// Initalize the emitter
void Emitter::InitEmitter() {
	// If we don't have a shader program
    if (!m_pProgram) {
        // Create one
		this->m_pProgram = wolf::LoadShaders("data/particle.vsh", "data/particle.fsh");

		glGenVertexArrays(1, &this->m_pVAO);
		glGenBuffers(1, &this->m_pVBO);

		glBindVertexArray(this->m_pVAO);
		glBindBuffer(GL_ARRAY_BUFFER, this->m_pVBO);
		glBufferData(GL_ARRAY_BUFFER, SIZE_OF_ONE_PARTICLE_DATASET * m_iMaxParticles, NULL, GL_DYNAMIC_DRAW);

		// Position
		glVertexAttribPointer(0,       						// Attribute location
							3,              				// Number of components
							GL_FLOAT,       				// Type of each component
							GL_FALSE,      					// Normalize?
							SIZE_OF_ONE_PARTICLE_VERTEX, 	// Stride
							0);             				// Offset
		glEnableVertexAttribArray(0);

		// Center
		glVertexAttribPointer(1,       						// Attribute location
							3,              				// Number of components
							GL_FLOAT,       				// Type of each component
							GL_FALSE,       				// Normalize?
							SIZE_OF_ONE_PARTICLE_VERTEX, 	// Stride
							(void*)(3 * sizeof(GLfloat)));	// Offset
		glEnableVertexAttribArray(1);

		// Size / Scale
		glVertexAttribPointer(2,       								// Attribute location
							3,              						// Number of components
							GL_FLOAT,       						// Type of each component
							GL_FALSE,       						// Normalize?
							SIZE_OF_ONE_PARTICLE_VERTEX, 			// Stride
							(void*)(VERTEX_SIZE + CENTER_SIZE));	// Offset
		glEnableVertexAttribArray(2);

		// Color
		glVertexAttribPointer(3,       											// Attribute location
							4,              									// Number of components
							GL_FLOAT,       									// Type of each component
							GL_FALSE,       									// Normalize?
							SIZE_OF_ONE_PARTICLE_VERTEX, 						// Stride
							(void*)(VERTEX_SIZE + CENTER_SIZE + SCALE_SIZE));	// Offset
		glEnableVertexAttribArray(3);

		// Texture Coordinates
		glVertexAttribPointer(4,       														// Attribute location
							2,              												// Number of components
							GL_FLOAT,       												// Type of each component
							GL_FALSE,       												// Normalize?
							SIZE_OF_ONE_PARTICLE_VERTEX, 									// Stride
							(void*)(VERTEX_SIZE + CENTER_SIZE + SCALE_SIZE + COLOR_SIZE));	// Offset
		glEnableVertexAttribArray(4);

		// Find our uniforms
		this->m_uProjection = glGetUniformLocation(this->m_pProgram, "projection");
		this->m_uView = glGetUniformLocation(this->m_pProgram, "view");
		this->m_uWorld = glGetUniformLocation(this->m_pProgram, "world");
		this->m_uSampler = glGetUniformLocation(this->m_pProgram, "sampler");

		// Create our texture
		this->m_pTexture = wolf::TextureManager::CreateTexture(this->m_strTexturePath);
		this->m_pTexture->SetFilterMode(wolf::Texture::FM_LinearMipmap, wolf::Texture::FM_Linear);
		this->m_pTexture->SetWrapMode(wolf::Texture::WM_Clamp);
    }

	// Fill our pool
	for (int i=0; i < m_iMaxParticles; i++) {
		Particle* freshParticle = new Particle();
		AddToPool(freshParticle);
	}

	// If we're a burst emitter, calculate time to next burst
	if (m_enMode == BURST) {
		CalcBurstTime();
	}

	// Update the emitter id and number of emitters
    nextEmitterID++;
}

// Destructor
Emitter::~Emitter() {
	// Delete shader program
    glDeleteVertexArrays(1, &this->m_pVAO);
    glDeleteBuffers(1, &this->m_pVBO);
    glDeleteProgram(this->m_pProgram);

	// Delete everything in the active list
	Particle* activeCurrent = m_pActiveHead;
	while (activeCurrent != NULL) {
		if (activeCurrent->next != NULL) {
			activeCurrent = activeCurrent->next;
			delete(activeCurrent->previous);
		}
		else {
			delete(activeCurrent);
			break;
		}
	}

	// Delete everything in the free list
	Particle* freeCurrent = m_pFreeHead;
	while (freeCurrent != NULL) {
		if (freeCurrent->next != NULL) {
			freeCurrent = freeCurrent->next;
			delete(freeCurrent->previous);
		}
		else {
			delete(freeCurrent);
			break;
		}
	}
}

// Add a particle to the free list
void Emitter::AddToPool(Particle* p_pParticle) {
	// This is the new free head, so it shouldn't have a previous
	p_pParticle->previous = 0;
	p_pParticle->next = m_pFreeHead;

	// If the pool wasn't empty beforehand
	if (m_pFreeHead != NULL) {
		// Then the last free head needs to know that we're taking its place
		m_pFreeHead->previous = p_pParticle;
	}

	// Set this particle to be the new free head
	m_pFreeHead = p_pParticle;
}

// Take a particle out of the free pool
Particle* Emitter::TakeFromPool() {
	// Get the free head
	Particle* freshParticle = m_pFreeHead;

	// If it's null, then the pool is empty
	if (freshParticle == NULL) {
		// So we recycle the tail of the active list
		freshParticle = m_pActiveTail;
		RemoveFromActiveList(m_pActiveTail);
		return freshParticle;
	}
	else {
		// Otherwise, we set the free head to be the next particle in the pool
		m_pFreeHead = m_pFreeHead->next;

		// and let the new free head know that there's nothing before it anymore
		if (m_pFreeHead != NULL) {
			m_pFreeHead->previous = 0;
		}

		// Then we separate this particle from the free list
		freshParticle->previous = 0;
		freshParticle->next = 0;

		// And return it
		return freshParticle;
	}
}

// Add a particle to the active list
void Emitter::AddToActiveList(Particle* p_pParticle) {
	// This is the new active head so there should be nothing before it
	p_pParticle->previous = 0;
	p_pParticle->next = m_pActiveHead;

	// If the previous free head exists
	if (m_pActiveHead != NULL) {
		// Then we let it know that its been replaced
		m_pActiveHead->previous = p_pParticle;
	}
	else {
		// Otherwise, this particle also becomes the new tail of the active list
		m_pActiveTail = p_pParticle;
	}

	// Set this particle to be the new active head
	m_pActiveHead = p_pParticle;
}

// Take a particle off of the active list
void Emitter::RemoveFromActiveList(Particle* p_pParticle) {
	// Start at the head of the Active list
	Particle* current = m_pActiveHead;

	// Iterate through until we hit the end
	while (current != NULL) {
		// Or find what we're looking for
		if (current == p_pParticle) {
			if (current != m_pActiveHead && current != m_pActiveTail) {
				// Set the previous Particle's next to whatever comes after this one
				// And set the next Particle's previous to whatever came before this one
				current->previous->next = current->next;
				current->next->previous = current->previous;
			}

			// If this was the head of the list
			if (current == m_pActiveHead) {
				// Then the Particle after us becomes the new head
				if (current->next != NULL) {
					current->next->previous = 0;
				}

				m_pActiveHead = current->next;
			}

			// If this was the tail of the list
			if (current == m_pActiveTail) {
				// Then the Particle before us becomes the new tail
				if (current->previous != NULL) {
					current->previous->next = 0;
				}

				m_pActiveTail = current->previous;
			}

			// Then disconnect this one from the list
			current->next = 0;
			current->previous = 0;

			return;
		}

		// This isn't what we want, so we try the next one
		current = current->next;
	}
}

// Spawn a particle (take from the pool or recycle and give the particle its inital attributes)
void Emitter::SpawnParticle() {
	// Get a new particle
	Particle* freshParticle = TakeFromPool();

	// Set its position randomly
	if (m_bRandomInitPosition) {
		freshParticle->position = genRandomVec3(m_vInitPosMin, m_vInitPosMax);
	}
	else {
		// Or specifically
		freshParticle->position = m_vInitPosition;
	}

	// Do the same for scale
	if (m_bRandomInitScale) {
		freshParticle->scale = genRandomVec3(m_vInitScaleMin, m_vInitScaleMax);
	}
	else {
		freshParticle->scale = m_vInitScale;
	}

	// And for velocity
	if (m_bRandomInitVelocity) {
		freshParticle->velocity = genRandomVec3(m_vInitVelMin, m_vInitVelMax);
	}
	else {
		freshParticle->velocity = m_vInitVelocity;
	}

	// And for color
	if (m_bRandomInitColor) {
		freshParticle->color = genRandomVec4(m_vInitColorMin, m_vInitColorMax);
	}
	else {
		freshParticle->color = m_vInitColor;
	}

	// Also for fade
	if (m_bRandomInitFade) {
		freshParticle->fade = genRandomFloat(m_fInitFadeMin, m_fInitFadeMax);
	}
	else {
		freshParticle->fade = m_fInitFade;
	}

	// And for fade
	if (m_bRandomInitLifetime) {
		freshParticle->lifetime = genRandomFloat(m_fInitLifeMin, m_fInitLifeMax);
	}
	else {
		freshParticle->lifetime = m_fInitLifetime;
	}

	// Then add this particle to the active list
	freshParticle->age = 0.0f;
	AddToActiveList(freshParticle);
}

// Kill a particle by taking it off the active list and sending it back to the pool
void Emitter::KillParticle(Particle* p_pParticle) {
	RemoveFromActiveList(p_pParticle);
	AddToPool(p_pParticle);
}

// Calculate the time to burst for burst emitters
void Emitter::CalcBurstTime() {
	if (m_bRandomBurstTime) {	// Can either be random
		m_fTimeToBurst = genRandomFloat(m_fBurstTimeMin, m_fBurstTimeMax);
	}
	else {	// Or specific
		m_fTimeToBurst = m_fBurstTime;
	}
}

// Restart the emitter by sending all its particles back into the pool and reinitalizing them
void Emitter::Restart() {
	// Kill all the particles
	Particle* current = m_pActiveHead;
	while (current != NULL) {
		KillParticle(current);
		current = m_pActiveHead;
	}

	// Figure out our new duration (if it can be randomly decided)
	if (m_bRandomDuration) {
		m_fDuration = genRandomFloat(m_fDurationMin, m_fDurationMax);
	}

	// Figure out our time to burst
	if (m_enMode == BURST) {
		CalcBurstTime();
		m_fTimeSinceLastEmission = 0.0f;
	}

	// Restart ourlifetime
	m_fLifetime = 0.0f;

	// And send 'er
	m_bPaused = false;
	m_bStopped = false;
}

// Unhinged method used to set all the initalization properties in one fell swoop
void Emitter::SetInitProperties(const glm::vec3& p_vInitPosition, const glm::vec3& p_vInitScale, const glm::vec3& p_vInitVelocity, const glm::vec4& p_vInitColor, float p_fInitFade, float p_fInitLifetime) {
	SetInitPosition(p_vInitPosition);
	SetInitScale(p_vInitScale);
	SetInitVelocity(p_vInitVelocity);
	SetInitColor(p_vInitColor);
	SetInitFade(p_fInitFade);
	SetInitLifetime(p_fInitLifetime);
}

// Update all active particles and "create" new ones
void Emitter::Update(float deltaTime) {
	// If we're not paused
	if (!m_bPaused) {
		// Keep track of how long we've been alive
		m_fLifetime += deltaTime;

		// If our duration hasn't expired and we're not stopped
		if ((m_fDuration < 0 || m_fLifetime < m_fDuration) && !m_bStopped) {
			// If we're spawning particles continuously
			if (m_enMode == CONTINUOUS) {
				// Figure out our birthrate (random each frame or constant)
				int birthrate;
				if (m_bRandomBirthrate) {
					birthrate = genRandomInt(m_iBirthrateMin, m_iBirthrateMax);
				}
				else {
					birthrate = m_iBirthrate;
				}

				// Figure out how many particles we should spawn this frame to meet said birthrate
				m_fTimeSinceLastEmission += birthrate * deltaTime;
				int num_spawns = int(m_fTimeSinceLastEmission);
				m_fTimeSinceLastEmission -= num_spawns;

				// And spawn that many
				for (int i = num_spawns; i != 0; i--) {
					SpawnParticle();
				}
			}
			else if (m_enMode == BURST) { // If we're a burst emitter
				// Keep track of how long it has been since our last burst
				m_fTimeToBurst -= deltaTime;

				// If it's time to burst
				if (m_fTimeToBurst <= 0) {
					// Figure out how many particles to spawn (random or constant)
					int num_spawns = 0;
					if (m_bRandomBurstSpawn) {
						num_spawns = genRandomInt(m_iBurstSpawnsMin, m_iBurstSpawnsMax);
					}
					else {
						num_spawns = m_iBurstSpawns;
					}

					// If we can burst repeatedly, figure out how long we should wait
					// until we burst again
					if (m_bRepeatBurst) {
						CalcBurstTime();
					}

					// Then spawn however many particles we decided on
					for (int i = num_spawns; i != 0; i--) {
						SpawnParticle();
					}
				}
			}
		}

		// Update all of the active particles by traversing the active list
		// and applying each of our affectors
		Particle* current = m_pActiveHead;
		while (current != NULL) {
			// Get the next active particle
			Particle* next = current->next;

			// Apply each of our affectors
			for (Affector* affector : m_pAffectors) {
				affector->Apply(deltaTime, current);
			}

			// Figure out if this one should be dead
			current->age += deltaTime;
			if (current->age >= current->lifetime) {
				KillParticle(current);	// Kill it if so
			}

			// And move onto the next one
			current = next;
		} 
	}
}

// Render all of our active particles in one big draw call
void Emitter::Render(const glm::mat4& p_mProjection, const glm::mat4& p_mView) {

	// We have to bind the buffer before early on to update its contents before we use the program
	glBindBuffer(GL_ARRAY_BUFFER, this->m_pVBO);

	// We're going to need to know how many particles are being rendered this frame
	int numToRender = 0;

	// And what offset we should apply to the VBO's memory addresses
	int offset = 0;

	// Go through the list of active particles
	Particle* current = m_pActiveHead;
	while (current != nullptr) {
		// Write each particle's data to the temporary array
		for (int i = 0; i < 6; i++) {
			int rowStart = i * ROW_SIZE;

			// Geometry
			temp_particleData[rowStart + 0] = m_fParticleGeometry[i].x;
			temp_particleData[rowStart + 1] = m_fParticleGeometry[i].y;
			temp_particleData[rowStart + 2] = m_fParticleGeometry[i].z;

			// Center (particle position)
			temp_particleData[rowStart + 3] = current->position.x;
			temp_particleData[rowStart + 4] = current->position.y;
			temp_particleData[rowStart + 5] = current->position.z;
			
			// Scale
			temp_particleData[rowStart + 6] = current->scale.x;
			temp_particleData[rowStart + 7] = current->scale.y;
			temp_particleData[rowStart + 8] = current->scale.z;

			// Color
			temp_particleData[rowStart + 9] = current->color.r;
			temp_particleData[rowStart + 10] = current->color.g;
			temp_particleData[rowStart + 11] = current->color.b;
			temp_particleData[rowStart + 12] = current->color.a * current->fade;

			// Texture Coordinates
			temp_particleData[rowStart + 13] = m_fTextureCoords[i].x;
			temp_particleData[rowStart + 14] = m_fTextureCoords[i].y;
		}

		// Then copy the contents of the temporary array to the VBO
		glBufferSubData(GL_ARRAY_BUFFER, offset * SIZE_OF_ONE_PARTICLE_DATASET, SIZE_OF_ONE_PARTICLE_DATASET, this->temp_particleData);

		// Advance to the next particle
		numToRender++;
		offset++;
		current = current->next;
	}

	// Bind the program
	glUseProgram(this->m_pProgram);

	// Figure out what blend mode we're using
	switch(this->m_enBlendMode) {
		case ADDITIVE:
			glBlendFunc(GL_ONE, GL_ONE);
		break;

		case MULTIPLICATIVE:
			glBlendFunc(GL_DST_COLOR, GL_ZERO);
		break;

		case INTERPOLATIVE:
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		break;
	}

	// Set up our uniforms
	glUniformMatrix4fv(this->m_uProjection, 1, GL_FALSE, glm::value_ptr(p_mProjection));
	glUniformMatrix4fv(this->m_uView, 1, GL_FALSE, glm::value_ptr(p_mView));
	glUniformMatrix4fv(this->m_uWorld, 1, GL_FALSE, glm::value_ptr(GetWorldTransform()));
	glUniform1i(this->m_uSampler, 0);

	// Bind our VAO
	glBindVertexArray(this->m_pVAO);

	// Bind our texture
	m_pTexture->Bind();

	//Render our however many particles we updated the data for
	glDrawArrays(GL_TRIANGLES, 0, numToRender * 6);
}


// This monster of a method parses an XML file and uses it to create an Emitter
Emitter* Emitter::CreateWithXML(const std::string& p_strFilePath) {
	// Try to parse the XML file
	TiXmlDocument doc(p_strFilePath.c_str());

	if (doc.LoadFile() == false) {
		printf("!--- Emitter XML error: \"%s\" ---!\n", doc.ErrorDesc());
		return nullptr;
	}
	
	// The constructor arguments should all be in the first node
	TiXmlNode* pRootNode = doc.FirstChild();
	const char* szRootName = pRootNode->Value();
	TiXmlElement* pRootElement = pRootNode->ToElement();

	// So check that the node has the right name
	if (strcmp(szRootName, "Emitter") != 0) {
		return nullptr;
	}

	// Then look for a name,
	const char* szEmitterName = pRootElement->Attribute("name");
	if (szEmitterName == nullptr) {
		return nullptr;
	}

	// texture path,
	const char* szTexturePath = pRootElement->Attribute("texture_path");
	if (szTexturePath == nullptr) {
		return nullptr;
	}

	// emission mode,
	const char* szType = pRootElement->Attribute("type");
	if (szType == nullptr) {
		return nullptr;
	}

	// (which can be either continuous or burst and should be converted to an enum)
	EmissionMode enMode;
	if (strcmp(szType, "continuous") == 0) {
		enMode = CONTINUOUS;
	}
	else if (strcmp(szType, "burst") == 0) {
		enMode = BURST;
	}
	else {
		return nullptr;
	}

	// blend mode
	const char* szBlendMode = pRootElement->Attribute("blend_mode");
	if (szBlendMode == nullptr) {
		return nullptr;
	}

	// (can be additive, multiplicative, or interpolative and should be converted to an enum)
	BlendMode enBlendMode;
	if (strcmp(szBlendMode, "additive") == 0) {
		enBlendMode = ADDITIVE;
	}
	else if (strcmp(szBlendMode, "multiplicative") == 0) {
		enBlendMode = MULTIPLICATIVE;
	}
	else if (strcmp(szBlendMode, "interpolative") == 0) {
		enBlendMode = INTERPOLATIVE;
	}
	else {
		return nullptr;
	}

	// the maximum number of particles,
	int iMax;
	if (pRootElement->QueryIntAttribute("max_particles", &iMax) != TIXML_SUCCESS) {
		return nullptr;
	}

	// the emitter duration (which can either be one number)
	float fDuration;
	if (pRootElement->QueryFloatAttribute("duration", &fDuration) != TIXML_SUCCESS) {
		// (or it can be a random value in a given range)
		float fDurationMin, fDurationMax;
		if (pRootElement->QueryFloatAttribute("duration_min", &fDurationMin) != TIXML_SUCCESS ||
			pRootElement->QueryFloatAttribute("duration_max", &fDurationMax) != TIXML_SUCCESS)
		{
			return nullptr;
		}

		// (found random duration, so we constuct with the range)
		Emitter* pCreatedEmitter = new Emitter(szEmitterName, szTexturePath, enMode, enBlendMode, iMax, fDurationMin, fDurationMax);
	}

	// If we got to this point then we have enough to construct an Emitter
	Emitter* pCreatedEmitter = new Emitter(szEmitterName, szTexturePath, enMode, enBlendMode, iMax, fDuration);

	// And now we can start looking for spawn properties and affectors
	TiXmlNode* pChildNode = pRootNode->FirstChild();
	while (pChildNode != nullptr) {
		const char* szName = pChildNode->Value();
		TiXmlElement* pElement = pChildNode->ToElement();

		// If this node describes a spawn property
		if (strcmp(szName, "spawn_property") == 0) {
			// Look for its name
			const char* szPropertyName = pElement->Attribute("name");
			if (szPropertyName == nullptr) {
				delete(pCreatedEmitter);
				return nullptr;
			}

			// And figure out if it is random or constant
			bool bIsRandom;
			if (pElement->QueryBoolAttribute("is_random", &bIsRandom) != TIXML_SUCCESS) {
				delete(pCreatedEmitter);
				return nullptr;
			}

			// If this is a position property
			if (strcmp(szPropertyName, "position") == 0) {
				// If it is random,
				if (bIsRandom) {
					// Look for the range
					float fMinPosX, fMaxPosX, fMinPosY, fMaxPosY, fMinPosZ, fMaxPosZ;
					if (pElement->QueryFloatAttribute("x_min", &fMinPosX) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("x_max", &fMaxPosX) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("y_min", &fMinPosY) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("y_max", &fMaxPosY) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("z_min", &fMinPosZ) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("z_max", &fMaxPosZ) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// And add it to our emitter
					pCreatedEmitter->SetRandomInitPositionRange(glm::vec3(fMinPosX, fMinPosY, fMinPosZ), glm::vec3(fMaxPosX, fMaxPosY, fMaxPosZ));
				}
				else {
					// If it isn't random, just look for the x and y
					float fXPos, fYPos, fZPos;
					if (pElement->QueryFloatAttribute("x", &fXPos) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("y", &fYPos) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("z", &fZPos) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// And add them to the emitter
					pCreatedEmitter->SetInitPosition(glm::vec3(fXPos, fYPos, fZPos));
				}
			}

			// Scale follows the same logic
			if (strcmp(szPropertyName, "scale") == 0) {
				// If it is random,
				if (bIsRandom) {
					// Look for the range
					float fMinScaleX, fMaxScaleX, fMinScaleY, fMaxScaleY, fMinScaleZ, fMaxScaleZ;
					if (pElement->QueryFloatAttribute("x_min", &fMinScaleX) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("x_max", &fMaxScaleX) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("y_min", &fMinScaleY) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("y_max", &fMaxScaleY) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("z_min", &fMinScaleZ) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("z_max", &fMaxScaleZ) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// And add it to our emitter
					pCreatedEmitter->SetRandomInitScaleRange(glm::vec3(fMinScaleX, fMinScaleY, fMinScaleZ), glm::vec3(fMaxScaleX, fMaxScaleY, fMaxScaleZ));
				}
				else {
					// If it isn't random, just look for the x and y
					float fXScale, fYScale, fZScale;
					if (pElement->QueryFloatAttribute("x", &fXScale) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("y", &fYScale) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("z", &fZScale) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// And add them to the emitter
					pCreatedEmitter->SetInitScale(glm::vec3(fXScale, fYScale, fZScale));
				}
			}

			// As does velocity
			if (strcmp(szPropertyName, "velocity") == 0) {
				// If it is random,
				if (bIsRandom) {
					// Look for the range
					float fMinVelX, fMaxVelX, fMinVelY, fMaxVelY, fMinVelZ, fMaxVelZ;
					if (pElement->QueryFloatAttribute("x_min", &fMinVelX) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("x_max", &fMaxVelX) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("y_min", &fMinVelY) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("y_max", &fMaxVelY) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("z_min", &fMinVelZ) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("z_max", &fMaxVelZ) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// And add it to our emitter
					pCreatedEmitter->SetRandomInitVelocityRange(glm::vec3(fMinVelX, fMinVelY, fMinVelZ), glm::vec3(fMaxVelX, fMaxVelY, fMaxVelZ));
				}
				else {
					// If it isn't random, just look for the x and y
					float fXVel, fYVel, fZVel;
					if (pElement->QueryFloatAttribute("x", &fXVel) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("y", &fYVel) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("z", &fZVel) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// And add them to the emitter
					pCreatedEmitter->SetInitVelocity(glm::vec3(fXVel, fYVel, fZVel));
				}
			}

			// Color is a bit more complex
			if (strcmp(szPropertyName, "color") == 0) {
				if (bIsRandom) {
					// Look for the min and max r, g, b, a values
					float fMinR, fMinG, fMinB, fMinA, fMaxR, fMaxG, fMaxB, fMaxA;
					if (pElement->QueryFloatAttribute("min_r", &fMinR) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("min_g", &fMinG) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("min_b", &fMinB) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("min_a", &fMinA) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max_r", &fMaxR) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max_g", &fMaxG) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max_b", &fMaxB) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max_a", &fMaxA) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// And if we find them all, use them to set the range
					pCreatedEmitter->SetRandomInitColorRange(glm::vec4(fMinR, fMinG, fMinB, fMinA), glm::vec4(fMaxR, fMaxG, fMaxB, fMaxA));
				}
				else {
					// Look for the r, g, b, a values
					float fR, fG, fB, fA;
					if (pElement->QueryFloatAttribute("r", &fR) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("g", &fG) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("b", &fB) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("a", &fA) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// And if we find them all, use them to set the color
					pCreatedEmitter->SetInitColor(glm::vec4(fR, fG, fB, fA));
				}
			}

			// Fade is simple!
			if (strcmp(szPropertyName, "fade") == 0) {
				if (bIsRandom) {
					// Look for the range
					float fMinFade, fMaxFade;
					if (pElement->QueryFloatAttribute("min", &fMinFade) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max", &fMaxFade) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// If we find it, use it
					pCreatedEmitter->SetRandomInitFadeRange(fMinFade, fMaxFade);
				}
				else {
					// Look for the value
					float fFadeValue;
					if (pElement->QueryFloatAttribute("value", &fFadeValue) != TIXML_SUCCESS) {
						delete(pCreatedEmitter);
						return nullptr;
					}

					// If we find it, set it
					pCreatedEmitter->SetInitFade(fFadeValue);
				}
			}

			// Same process for lifetime
			if (strcmp(szPropertyName, "lifetime") == 0) {
				if (bIsRandom) {
					// Look for the range
					float fMinLife, fMaxLife;
					if (pElement->QueryFloatAttribute("min", &fMinLife) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max", &fMaxLife) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// If we find it, use it
					pCreatedEmitter->SetRandomInitLifetimeRange(fMinLife, fMaxLife);
				}
				else {
					// Look for the value
					float fLifetime;
					if (pElement->QueryFloatAttribute("value", &fLifetime) != TIXML_SUCCESS) {
						delete(pCreatedEmitter);
						return nullptr;
					}

					// If we find it, set it
					pCreatedEmitter->SetInitLifetime(fLifetime);
				}
			}

			// And for birthrate
			if (strcmp(szPropertyName, "birthrate") == 0) {
				if (bIsRandom) {
					// Look for the range
				 	int fMinBirth, fMaxBirth;
					if (pElement->QueryIntAttribute("min", &fMinBirth) != TIXML_SUCCESS ||
						pElement->QueryIntAttribute("max", &fMaxBirth) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// If we find it, use it
					pCreatedEmitter->SetRandomBirthrateRange(fMinBirth, fMaxBirth);
				}
				else {
					// Look for the value
					int fBirthRate;
					if (pElement->QueryIntAttribute("value", &fBirthRate) != TIXML_SUCCESS) {
						delete(pCreatedEmitter);
						return nullptr;
					}

					// If we find it, set it
					pCreatedEmitter->SetBirthrate(fBirthRate);
				}
			}

			// And for burst spawn
			if (strcmp(szPropertyName, "num_burst_spawn") == 0) {
				if (bIsRandom) {
					// Look for the range
					int fMinBurst, fMaxBurst;
					if (pElement->QueryIntAttribute("min", &fMinBurst) != TIXML_SUCCESS ||
						pElement->QueryIntAttribute("max", &fMaxBurst) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// If we find it, use it
					pCreatedEmitter->SetRandomBurstAmountRange(fMinBurst, fMaxBurst);
				}
				else {
					// Look for the value
					int fBurstSpawn;
					if (pElement->QueryIntAttribute("value", &fBurstSpawn) != TIXML_SUCCESS) {
						delete(pCreatedEmitter);
						return nullptr;
					}

					// If we find it, set it
					pCreatedEmitter->SetBurstAmount(fBurstSpawn);
				}
			}

			// This one is the same, too
			if (strcmp(szPropertyName, "time_to_burst") == 0) {
				if (bIsRandom) {
					// Look for the range
					float fMinBurstTime, fMaxBurstTime;
					if (pElement->QueryFloatAttribute("min", &fMinBurstTime) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max", &fMaxBurstTime) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					// If we find it, use it
					pCreatedEmitter->SetRandomBurstTimeRange(fMinBurstTime, fMaxBurstTime);
				}
				else {
					// Look for the value
					float fBurstTime;
					if (pElement->QueryFloatAttribute("value", &fBurstTime) != TIXML_SUCCESS) {
						delete(pCreatedEmitter);
						return nullptr;
					}

					// If we find it, set it
					pCreatedEmitter->SetBurstTime(fBurstTime);
				}

				// But with this one we also check if this is repeatable
				bool bRepeats;
				if (pElement->QueryBoolAttribute("repeats", &bRepeats) != TIXML_SUCCESS) {
					delete(pCreatedEmitter);
					return nullptr;
				}
				pCreatedEmitter->ToggleRepeatBurst(bRepeats);
			}
		}

		// If this is an affector
		if (strcmp(szName, "affector") == 0) {
			// Figure out what type
			const char* szType = pElement->Attribute("type");
			if (szType == nullptr) {
				delete(pCreatedEmitter);
				return nullptr;
			}

			// If we're adding velocity
			if (strcmp(szType, "add_velocity") == 0) {
				// We just need to add the affector
				pCreatedEmitter->AddAffector(new AddVelAffector());
			}
			else if (strcmp(szType, "fade") == 0) { // If we're adding a fade affector
				// We need to know what fade mode it uses
				const char* szFadeMode = pElement->Attribute("mode");
				if (strcmp(szFadeMode, "constant") == 0) {
					// Constant has one value
					float fFadeAmount;
					if (pElement->QueryFloatAttribute("amount", &fFadeAmount) != TIXML_SUCCESS) {
						delete(pCreatedEmitter);
						return nullptr;
					}

					pCreatedEmitter->AddAffector(new FadeAffector(CONSTANT_FADE, fFadeAmount));
				}
				else if (strcmp(szFadeMode, "random") == 0) {
					// Random has a range
					float fMinFadeAmount, fMaxFadeAmount;
					if (pElement->QueryFloatAttribute("min_amount", &fMinFadeAmount) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max_amount", &fMaxFadeAmount) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					pCreatedEmitter->AddAffector(new FadeAffector(RANDOM_FADE, fMinFadeAmount, fMaxFadeAmount));
				}
				else if (strcmp(szFadeMode, "out_over_lifetime") == 0) {
					// Fading out can be added as an argument
					pCreatedEmitter->AddAffector(new FadeAffector(FADE_OUT_OVER_LIFETIME));
				}
				else if (strcmp(szFadeMode, "in_over_lifetime") == 0) {
					// And so can fading in
					pCreatedEmitter->AddAffector(new FadeAffector(FADE_IN_OVER_LIFETIME));
				}
				else {
					// Otherwise, this is an invalid fade mode
					delete(pCreatedEmitter);
					return nullptr;
				}
			}
			else if (strcmp(szType, "scale") == 0) {
				// Similarly, we need to know the scale mode
				const char* szScaleMode = pElement->Attribute("mode");
				if (szScaleMode == nullptr) {
					delete(pCreatedEmitter);
					return nullptr;
				}

				// Constant only has an X and Y
				if (strcmp(szScaleMode, "constant") == 0) {
					float fXAmt, fYAmt, fZAmt;
					if (pElement->QueryFloatAttribute("x", &fXAmt) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("y", &fYAmt) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("z", &fZAmt) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					pCreatedEmitter->AddAffector(new ScaleAffector(CONSTANT_SCALE, glm::vec3(fXAmt, fYAmt, fZAmt)));
				}
				else if (strcmp(szScaleMode, "random") == 0) {
					// Random takes an X and Y range
					float fXMin, fXMax, fYMin, fYMax, fZMin, fZMax;
					if (pElement->QueryFloatAttribute("min_x", &fXMin) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("min_y", &fYMin) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max_x", &fXMax) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max_y", &fYMax) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("min_z", &fZMin) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("max_z", &fZMax) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					pCreatedEmitter->AddAffector(new ScaleAffector(RANDOM_SCALE, glm::vec3(fXMin, fYMin, fZMin), glm::vec3(fXMax, fYMax, fZMax)));
				}
				else if (strcmp(szScaleMode, "over_lifetime") == 0) {
					// Over lifetime needs a start and end size
					float fStartX, fStartY, fStartZ, fEndX, fEndY, fEndZ;
					if (pElement->QueryFloatAttribute("start_x", &fStartX) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("start_y", &fStartY) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("start_z", &fStartZ) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("end_x", &fEndX) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("end_y", &fEndY) != TIXML_SUCCESS ||
						pElement->QueryFloatAttribute("end_z", &fEndZ) != TIXML_SUCCESS)
					{
						delete(pCreatedEmitter);
						return nullptr;
					}

					pCreatedEmitter->AddAffector(new ScaleAffector(SCALE_OVER_LIFETIME, glm::vec3(fStartX, fStartY, fStartZ), glm::vec3(fEndX, fEndY, fEndZ)));
				}
				else {
					// Otherwise, this is an invalid scale mode
					delete(pCreatedEmitter);
					return nullptr;
				}
			}
			else if (strcmp(szType, "oscillating") == 0) {
				float fInterval, fAmt;
				if (pElement->QueryFloatAttribute("interval", &fInterval) != TIXML_SUCCESS ||
					pElement->QueryFloatAttribute("amount", &fAmt) != TIXML_SUCCESS) {
					delete(pCreatedEmitter);
					return nullptr;
				}

				Axis enAxis;
				const char* szAxis = pElement->Attribute("axis");
				if (szAxis == nullptr) {
					delete(pCreatedEmitter);
					return nullptr;
				}

				if (strcmp(szAxis, "x") == 0) {
					enAxis = X;
				}
				else if (strcmp(szAxis, "y") == 0) {
					enAxis = Y;
				}
				else if (strcmp(szAxis, "z") == 0) {
					enAxis = Z;
				}
				else {
					delete(pCreatedEmitter);
					return nullptr;
				}

				pCreatedEmitter->AddAffector(new OscillatingVelAffector(fInterval, enAxis, fAmt));
			}
			else {
				// Otherwise, this is an invalid affector type
				delete(pCreatedEmitter);
				return nullptr;
			}
		}

		// Advance to the next node
		pChildNode = pChildNode->NextSibling();
	}

	// If we didn't run into any issues, we can return the emitter we created
	return pCreatedEmitter;
}