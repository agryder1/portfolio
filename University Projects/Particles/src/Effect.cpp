#include "Effect.h"

// Constructor
Effect::Effect(const std::string& p_strName) : m_strName(p_strName) {}

// Destructor
Effect::~Effect() {
	// Have to make sure that all of our emitters are destroyed
	for (Emitter* emitter : m_vEmitters) {
		emitter->~Emitter();
	}

	m_vEmitters.clear();
}

// Update each emitter
void Effect::Update(float deltaTime) {
	for (Emitter* emitter : m_vEmitters) {
		emitter->Update(deltaTime);
	}
}

// Render each emitter (and transform it if necessary)
void Effect::Render(const glm::mat4& p_mProjection, const glm::mat4& p_mView) {
	GetTransform();
	for (Emitter* emitter : m_vEmitters) {
		emitter->SetParentTransform(m_m4Transform);
		emitter->Render(p_mProjection, p_mView);
	}
}

// Play all emitters
void Effect::Play() {
	for (Emitter* emitter : m_vEmitters) {
		emitter->Play();
	}
};

// Pause all emitters
void Effect::Pause() {
	for (Emitter* emitter : m_vEmitters) {
		emitter->Pause();
	}
};

// Stop all emitters from emitting
void Effect::Stop() {
	for (Emitter* emitter : m_vEmitters) {
		emitter->Stop();
	}
};

// Restart the emitter (kill all active particles and reinitialize)
void Effect::Restart() {
	for (Emitter* emitter : m_vEmitters) {
		emitter->Restart();
	}
}

// This method parses an XML file and uses it to create an Effect and its Emitters
Effect* Effect::CreateWithXMLFile(const std::string& p_strFilePath) {
	// Try to parse the file
	TiXmlDocument doc(p_strFilePath.c_str());

	if (doc.LoadFile() == false) {
		printf("!--- Effect XML error: \"%s\" ---!\n", doc.ErrorDesc());
		return nullptr;
	}

	// The first node should be "Effect"
	TiXmlNode* pNode = doc.FirstChild("Effect");
	if (pNode == nullptr) {
		return nullptr;
	}

	// And it should have our name
	TiXmlElement* pElement = pNode->ToElement();
	const char* szEffectName = pElement->Attribute("name");
	if (szEffectName == nullptr) {
		delete(pElement);
		return nullptr;
	}

	// If it does, we can create an Effect!
	Effect* pCreatedEffect = new Effect(szEffectName);

	// Then we start iterating through the nodes
	TiXmlNode* pChildNode = pNode->FirstChild();
	while (pChildNode != nullptr) {
		const char* szChildName = pChildNode->Value();
		TiXmlElement* pChildElement = pChildNode->ToElement();

		// If we find an Emitter
		if (strcmp(szChildName, "Emitter") == 0) {
			// We look for its XML file
			const char* szFilePath = pChildElement->Attribute("file");
			if (szFilePath == nullptr) {
				delete(pCreatedEffect);
				return nullptr;
			}

			// And pass it on to the Emitter class' XML parser
			Emitter* pEmitter = Emitter::CreateWithXML(szFilePath);

			// If the parser successfully creates an Emitter
			if (pEmitter == nullptr) {
				delete(pCreatedEffect);
				return nullptr;
			}

			// We look for the offset attribute(s)
			float offsetX, offsetY, offsetZ;
			if (pChildElement->QueryFloatAttribute("offsetX", &offsetX) != TIXML_SUCCESS) { // X
				delete(pCreatedEffect);
				return nullptr;
			}

			if (pChildElement->QueryFloatAttribute("offsetY", &offsetY) != TIXML_SUCCESS) { // Y
				delete(pCreatedEffect);
				return nullptr;
			}

			if (pChildElement->QueryFloatAttribute("offsetZ", &offsetZ) != TIXML_SUCCESS) { // Z
				delete(pCreatedEffect);
				return nullptr;
			}

			// And use them to position our Emitter
			pEmitter->SetPosition(glm::vec3(offsetX, offsetY, offsetZ));

			// Then we add it to our created effect
			pCreatedEffect->AddEmitter(pEmitter);
		}

		// And move to the next node
		pChildNode = pChildNode->NextSibling();
	}

	// If we get here, then we didn't run into any issues, so we can pass back the effect
	return pCreatedEffect;
}