#pragma once
#include "Affector.h"

// Enum for each of the Affector's scale modes
enum ScaleMode {
    CONSTANT_SCALE,
    RANDOM_SCALE,
    SCALE_OVER_LIFETIME
};

// This class scales a particle according to the scale mode
class ScaleAffector : public Affector {
private:
    ScaleMode m_enMode; // Mode to use

    // Parameters for each scale mode
    glm::vec3 m_vScaleAmt, m_vScaleMin, m_vScaleMax, m_vStartSize, m_vEndSize;

public:
    // Each of these constructors corresponds to a different scale mode, but you can use
    // any constructor with any mode as long as you set the required parameters using
    // the corresponding setters, later
    ScaleAffector(ScaleMode p_enScaleMode) : m_enMode(p_enScaleMode), Affector() {} ;
    ScaleAffector(ScaleMode p_enScaleMode, const glm::vec3& p_vScaleAmt) : m_enMode(p_enScaleMode), m_vScaleAmt(p_vScaleAmt), Affector() {};
    ScaleAffector(ScaleMode p_enScaleMode, const glm::vec3& p_vMinOrStart, const glm::vec3& p_vMaxOrEnd) :
    m_enMode(p_enScaleMode), Affector() {
        if (p_enScaleMode == RANDOM_SCALE) {
            m_vScaleMin = p_vMinOrStart;
            m_vScaleMax = p_vMaxOrEnd;
        }
        else {
            m_vStartSize = p_vMinOrStart;
            m_vEndSize = p_vMaxOrEnd;
        }
    };

    virtual ~ScaleAffector() {};
    
    // Scales the particle according to the chosen scale mode
    virtual void Apply(float p_fDelta, Particle* p_pParticle) {
        switch (m_enMode) {
            case CONSTANT_SCALE : {
                // Constant continously scales the particle
                p_pParticle->scale += m_vScaleAmt * p_fDelta;
            }
            break;

            case RANDOM_SCALE : {
                // Random chooses a new scale from a given range each time this method is called
                p_pParticle->scale += genRandomVec3(m_vScaleMin, m_vScaleMax) * p_fDelta;
            }
            break;

            case SCALE_OVER_LIFETIME : {
                // Scale over lifetime scales the particle from its start size to the given end size over its lifetime
                p_pParticle->scale = m_vStartSize + p_pParticle->age * (m_vEndSize - m_vStartSize);
            }
            break;

        }

        // Make sure we don't end up with a negative scale
        if (p_pParticle->scale.x < 0) {
            p_pParticle->scale.x = 0;
        }

        if (p_pParticle->scale.y < 0) {
            p_pParticle->scale.y = 0;
        }

        if (p_pParticle->scale.z < 0) {
            p_pParticle->scale.z = 0;
        }
    };

    // Setters for each of the Affector's attributes
    void SetScaleMode(ScaleMode p_enMode) {m_enMode = p_enMode;};
    void SetScaleAmt(const glm::vec3& p_vAmt) {m_vScaleAmt = p_vAmt;};
    void SetScaleRange(const glm::vec3& p_vMin, const glm::vec3& p_vMax) {m_vScaleMin = p_vMin; m_vScaleMax = p_vMax;};
    void SetStartAndEndScale(const glm::vec3& p_vStartScale, const glm::vec3& p_vEndScale) {m_vStartSize = p_vStartScale; m_vEndSize = p_vEndScale;};
};