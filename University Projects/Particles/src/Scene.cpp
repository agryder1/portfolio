#include "Scene.h"

//Static variable to hold the Singleton instance
Scene* Scene::m_pSceneInstance;

bool Scene::AddEffect(Effect* p_pEffect) {
    // If we have effects already
    if (!m_pEffects.empty()) {
        // We need to check that this isn't a duplicate
        std::vector<Effect*>::iterator it = std::find(m_pEffects.begin(), m_pEffects.end(), p_pEffect);
        if (it != m_pEffects.end()) {
            return false; // Uh oh! It's a duplicate
        }
    }

    // No duplicate!
    m_pEffects.push_back(p_pEffect);
    return true;
}

bool Scene::RemoveEffect(Effect* p_pEffect) {
    // If we have effects
    if (!m_pEffects.empty()) {
        // Then we check if it's on the list
        std::vector<Effect*>::iterator it = std::find(m_pEffects.begin(), m_pEffects.end(), p_pEffect);
        if (it != m_pEffects.end()) {
            m_pEffects.erase(it); // If it's on the list we remove it
            return true;
        }
    }

    // Otherwise, we didn't find it or we had no effects
    return false;
}

//Update the scene
void Scene::Update(float deltatTime) {

    for (Effect* effect : m_pEffects) {
        effect->Update(deltatTime);
    }
}

//Render the scene
void Scene::Render() {
    //Can't render without a camera!
    if (m_pSceneCamera != nullptr) {
        //Get our projection and view matrices from the camera
        glm::mat4 projection = m_pSceneCamera->GetProjectionMatrix();
        glm::mat4 view = m_pSceneCamera->GetViewMatrix();

        // Iterate through all the attached effects and render 'em
        for (Effect* effect : m_pEffects) {
            effect->Render(projection, view);
        }
    }
}