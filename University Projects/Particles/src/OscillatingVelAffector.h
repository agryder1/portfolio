#pragma once
#include "Affector.h"

enum Axis {
    X,
    Y,
    Z
};

// This class applies a particle's velocity to it over time
class OscillatingVelAffector: public Affector {
private:
    float m_fInterval = 0.0f;
    float m_fTimer = 0.0f;
    float m_fAmt = 0.0f;
    Axis m_enAxis;

public:
    OscillatingVelAffector(float p_fInterval, Axis p_enAxis, float p_fAmt) : m_fInterval(p_fInterval), m_enAxis(p_enAxis), m_fAmt(p_fAmt), Affector() {};
    virtual ~OscillatingVelAffector() {};
    virtual void Apply(float p_fDelta, Particle* p_pParticle) {
        m_fTimer += p_fDelta;
        if (m_fTimer >= m_fInterval) {
            switch(m_enAxis) {
                case X:
                    p_pParticle->velocity.x = m_fAmt;
                break;
                case Y:
                    p_pParticle->velocity.y = m_fAmt;
                break;
                case Z:
                    p_pParticle->velocity.z = m_fAmt;
                break;
            }
            m_fAmt = -m_fAmt;
            m_fTimer = 0.0f;
        }
    };
};