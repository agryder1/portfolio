#pragma once
#include "general.h"
#include "Camera.h"
#include "Effect.h"

//Singleton Scene that holds all our nodes and our quadtree
class Scene {
public:
    //Create an instance
    static void CreateInstance() {
        assert(m_pSceneInstance == NULL); //As long as there isn't already one!
        m_pSceneInstance = new Scene();
    };

    //Destroy the instance
    static void DestroyInstance() {
        assert(m_pSceneInstance != NULL); //If it exists..
        delete(m_pSceneInstance);
        m_pSceneInstance = NULL;
    };

    //Get the instance
    static Scene* Instance() {
        assert(m_pSceneInstance); //If it exists..
        return m_pSceneInstance;
    };

    //Add/remove a camera
    void SetActiveCamera(Camera* p_pCamera) {m_pSceneCamera = p_pCamera;};
    bool RemoveActiveCamera() {
        if (m_pSceneCamera != nullptr) {
            free(m_pSceneCamera);
            return true;
        }
        return false;
    };

    //Update and render the scene's contents
    void Update(float deltaTime);
    void Render();

    // Need to be able to add and remove effects
    bool AddEffect(Effect* p_pEffect);
    bool RemoveEffect(Effect* p_pEffect);

private:
    //Private constructor and destructor 'cause we're a Singleton
    Scene() {};
    ~Scene() {
        //Don't forget to free our camera and quadtree
        free(m_pSceneCamera);
        
        if (!m_pEffects.empty()) {
            m_pEffects.clear();
        }

    };

    static Scene* m_pSceneInstance; //The instance variable
    std::vector<Effect*> m_pEffects;

    //Variables to hold our camera and quadtree
    Camera* m_pSceneCamera = nullptr;
};