#pragma once
#include "general.h"
#include <vector>

// Enum for the emitter's emission mode
enum EmissionMode {
    BURST,
    CONTINUOUS,
};

// Enum for the emitter's blend mode
enum BlendMode {
    ADDITIVE,
    MULTIPLICATIVE,
    INTERPOLATIVE,
};

// Forward definition of affector so we can define add/remove Affector
class Affector;

// Particle struct that holds all information for a single particle
struct Particle {
    glm::vec3 position, scale, velocity;    // Where is it, how big is it, what's its velocity?
    glm::vec4 color;                        // What color is it?
    float lifetime;                         // How long until it disappears on its own?
    float fade;                             // Fade to control alpha channel
    float age = 0.0f;                       // How long has this particle been alive?

    Particle* previous = 0;                 // Particle that came before us
    Particle* next = 0;                     // Particle that comes after us

    // Constructor with default values
    Particle() : position(0.0f), scale(1.0f), velocity(0.0f), color(colors[7]), lifetime(0.0f), fade(0.0f) {}
    
    // Constructor with manually-inputted values
    Particle(const glm::vec3& p_vPos, const glm::vec3& p_vScale, const glm::vec3& p_vVel, const glm::vec4& p_vColor, float p_vLife, float p_vFade) : 
    position(p_vPos), scale(p_vScale), velocity(p_vVel), color(p_vColor), lifetime(p_vLife), fade(p_vFade) {}
};

// This class defines an Emitter which spawns and controls particles from a finite pool.
// The Emitter renders all particles in a single draw call and uses a linked list structure
// to keep track of what particles are alive/dead
class Emitter {
private:
    // To render all particles at once, we need to be able to dynamically write to the VBO,
    // and to do that, we need to know how large the data for a single particle is.

    // Particles all have the following attributes; we need to know how big each one is.
    static const GLsizei VERTEX_SIZE = 3 * sizeof(GLfloat);
    static const GLsizei CENTER_SIZE = 3 * sizeof(GLfloat);
    static const GLsizei SCALE_SIZE = 3 * sizeof(GLfloat);
    static const GLsizei COLOR_SIZE = 4 * sizeof(GLfloat);
    static const GLsizei UV_SIZE = 2 * sizeof(GLfloat);

    // We then use those numbers to figure out how big one particle vertex is
    static const GLsizei SIZE_OF_ONE_PARTICLE_VERTEX = VERTEX_SIZE + CENTER_SIZE + SCALE_SIZE + COLOR_SIZE + UV_SIZE;

    // and use that to figure out how big a dataset with enough vertices to draw one particle is
    static const GLsizei SIZE_OF_ONE_PARTICLE_DATASET = SIZE_OF_ONE_PARTICLE_VERTEX * 6;

    // That number is then used to figure out our temporary array dimensions
    static const GLsizei TEMP_ARRAY_LENGTH = (SIZE_OF_ONE_PARTICLE_VERTEX / sizeof(GLfloat)) * 6;
    static const int ROW_SIZE = TEMP_ARRAY_LENGTH / 6;

    // So that we can reserve that much space in memory
    GLfloat temp_particleData[TEMP_ARRAY_LENGTH];

    // We're also going to need a place to put our particles' shared geometry as each particle
    // is the same shape and uses the same texture coordinates
    static const Vertex m_fParticleGeometry[];
    static const glm::vec2 m_fTextureCoords[];

    // We should also keep track of how many emitters have been made and
    // give them a corresponding ID so we can quickly reference them
    static int nextEmitterID;
    const int m_iID;

    //Shader variables
    GLuint m_pProgram = 0;
    GLuint m_pVBO = 0;
    GLuint m_pVAO = 0;

    GLint m_uProjection;
    GLint m_uView;
    GLint m_uWorld;
    GLint m_uSampler;

    wolf::Texture* m_pTexture;
    std::string m_strTexturePath;

    // Transform variables
    glm::vec3 m_vLocalScale = glm::vec3(1.0f, 1.0f, 1.0f);
    glm::vec3 m_vLocalRotation = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 m_vLocalPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::mat4 m_m4LocalTransform = glm::mat4(1.0f);
    glm::mat4 m_mParentTransform = glm::mat4(1.0f);
    bool dirty = false; //Flag to prevent unnecessary transform recalculations

    // Emitter properties
    std::string m_strName;
    EmissionMode m_enMode;

    std::vector<Affector*> m_pAffectors; // list of Affectors that will be applied to each Particle

    const int m_iMaxParticles;  // Size of the finite pool

    bool m_bPaused = true;
    bool m_bStopped = false;

    float m_fDuration = 0.0f;
    float m_fDurationMin = 0.0f;
    float m_fDurationMax = 0.0f;
    float m_fLifetime = 0.0f;

    BlendMode m_enBlendMode = INTERPOLATIVE;

    // Spawn properties
    int m_iBirthrate = 0;
    int m_iBirthrateMin = 0;
    int m_iBirthrateMax = 0;

    bool m_bRandomDuration = false;
    bool m_bRandomBirthrate = false;
    bool m_bRandomBurstSpawn = false;
    bool m_bRandomBurstTime = false;
    bool m_bRepeatBurst = false;

    int m_iBurstSpawns = 0;
    int m_iBurstSpawnsMin = 0;
    int m_iBurstSpawnsMax = 0;

    float m_fBurstTime = 0.0f;
    float m_fBurstTimeMin = 0.0f;
    float m_fBurstTimeMax = 0.0f;

    // Spawn control
    float m_fTimeSinceLastEmission = 0.0f;
    float m_fTimeToBurst = 0.0f;
    
    // Particle lists
    Particle* m_pFreeHead = 0;
    Particle* m_pActiveHead = 0;
    Particle* m_pActiveTail = 0;

    void AddToPool(Particle* p_pParticle);
    Particle* TakeFromPool();

    void AddToActiveList(Particle* p_pParticle);
    void RemoveFromActiveList(Particle* p_pParticle);

    // Particle Properties
    bool m_bRandomInitPosition;
    glm::vec3 m_vInitPosition, m_vInitPosMin, m_vInitPosMax;

    bool m_bRandomInitScale;
    glm::vec3 m_vInitScale, m_vInitScaleMin, m_vInitScaleMax;

    bool m_bRandomInitVelocity;
    glm::vec3 m_vInitVelocity, m_vInitVelMin, m_vInitVelMax;

    bool m_bRandomInitColor;
    glm::vec4 m_vInitColor, m_vInitColorMin, m_vInitColorMax;

    bool m_bRandomInitLifetime;
    float m_fInitLifetime, m_fInitLifeMin, m_fInitLifeMax;

    bool m_bRandomInitFade;
    float m_fInitFade, m_fInitFadeMin, m_fInitFadeMax;

    // Private method used to initialize the emitter
    void InitEmitter();

public:
    // Constructors (one with a set duration and the other with a duration range), and Destructor
    Emitter(const std::string& p_strName, const std::string& p_strTexturePath, EmissionMode p_enEmissionMode, BlendMode p_enBlendMode, int p_iMaxParticles, float p_fDuration);
    Emitter(const std::string& p_strName, const std::string& p_strTexturePath, EmissionMode p_enEmissionMode, BlendMode p_enBlendMode, int p_iMaxParticles, float p_fMinDuration, float p_fMaxDuration);
    ~Emitter();

    // Update and render
    void Update(float deltaTime);
    void Render(const glm::mat4& p_mProjection, const glm::mat4& p_mView);

    // Getter for the ID
    int GetID() { return m_iID; };

    // Static method that lets us create Emitters using an XML file
    static Emitter* CreateWithXML(const std::string& p_strFilePath);

    // --- Methods for controlling the Emitter ---
    void Pause() {m_bPaused = true;};
    void Play() {
        m_bPaused = false;
        m_bStopped = false;
    };
    void Stop() {m_bStopped = true;};
    void Restart();

    // Get particles out of the pool and put 'em back in
    void SpawnParticle();
    void KillParticle(Particle* p_pParticle);

    // Change the blend mode
    void SetBlendMode(BlendMode p_enBlendMode) {m_enBlendMode = p_enBlendMode;};
    BlendMode GetBlendMode() {return m_enBlendMode;};

    // Change the Emitter's name
    void SetName(const std::string& p_strName) {m_strName = p_strName;};
    std::string GetName() {return m_strName;};

    // Add an affector
    bool AddAffector(Affector* p_pAffector) {
        // If our list isn't empty,
        if (!m_pAffectors.empty()) {
            // Check if we have that particular affecor already
            std::vector<Affector*>::iterator it = std::find(m_pAffectors.begin(), m_pAffectors.end(), p_pAffector);
            if (it != m_pAffectors.end()) { // And don't add it if we do
                return false;
            }
        }

        // Otherwise, add the affector
        m_pAffectors.push_back(p_pAffector);
        return true;
    }

    // Remove an affector
    bool RemoveAffector(Affector* p_pAffector) {
        // If our list isn't empty
        if (!m_pAffectors.empty()) {
            // And we have the affector
            std::vector<Affector*>::iterator it = std::find(m_pAffectors.begin(), m_pAffectors.end(), p_pAffector);
            if (it != m_pAffectors.end()) {
                // Remove it
                m_pAffectors.erase(it);
                return true;
            }
        }

        // Otherwise, do nothing
        return false;
    }

    // --- Transform Manipulation ---
    //Set our local transform variables directly (overwrites old transform)
    void SetScale(const glm::vec3& newScale) {m_vLocalScale = newScale; dirty = true;};
    void SetRotation(const glm::vec3& newRotation) {m_vLocalRotation = newRotation; dirty = true;};
    void SetPosition(const glm::vec3& newPosition) {m_vLocalPosition = newPosition; dirty = true;};
    
    //Perform a transformation based on our current transform (adjusts current transform)
    void Scale(const glm::vec3& scale) {m_vLocalScale *= scale; dirty = true;};
    void Rotation(const glm::vec3& rotate) {m_vLocalRotation += rotate; dirty = true;};
    void Position(const glm::vec3& position) {m_vLocalPosition += position; dirty = true;};

    // Set this Emitter's parent transform
    void SetParentTransform(const glm::mat4& p_mParentTransform) {m_mParentTransform = p_mParentTransform;};

    //Get our local transform (relative to our origin)
    //(defined and declared here because its not likely to change)
    glm::mat4 GetLocalTransform() {
        if (dirty) { //Only recalculate if necessary
            m_m4LocalTransform = glm::translate(glm::mat4(1.0f), m_vLocalPosition);
            m_m4LocalTransform = m_m4LocalTransform * glm::mat4_cast(glm::quat(glm::vec3(glm::radians(m_vLocalRotation.x), glm::radians(m_vLocalRotation.y), glm::radians(m_vLocalRotation.z))));
            m_m4LocalTransform = glm::scale(m_m4LocalTransform, m_vLocalScale);

            dirty = false;
        }
        return m_m4LocalTransform;
    }

    //Get our global transform (relative to our parent's origin)
    //(defined and declared here because its not likely to change)
    glm::mat4 GetWorldTransform() {
        return GetLocalTransform() * m_mParentTransform; //Otherwise, just return our local transform
    }

    // --- Particle Properties ---

    // Gently cursed "Set All" method
    void SetInitProperties(const glm::vec3& p_vInitPosition, const glm::vec3& p_vInitScale, const glm::vec3& p_vInitVelocity, const glm::vec4& p_vInitColor, float fade, float lifetime);

    // Position
    void SetInitPosition(const glm::vec3& p_vInitPos) {
        m_bRandomInitPosition = false;
        m_vInitPosition = p_vInitPos;
    };

    void SetRandomInitPositionRange(const glm::vec3& p_vMin, const glm::vec3& p_vMax) {
        m_bRandomInitPosition = true;
        m_vInitPosMin = p_vMin;
        m_vInitPosMax = p_vMax;
    };

    // Scale
    void SetInitScale(const glm::vec3& p_vInitScale) {
        m_bRandomInitScale = false;
        m_vInitScale = p_vInitScale;
    };

    void SetRandomInitScaleRange(const glm::vec3& p_vMin, const glm::vec3& p_vMax) {
        m_bRandomInitScale = true;
        m_vInitScaleMin = p_vMin;
        m_vInitScaleMax = p_vMax;
    };

    // Velocity
    void SetInitVelocity(const glm::vec3& p_vInitVelocity) {
        m_bRandomInitVelocity = false;
        m_vInitVelocity = p_vInitVelocity;
    }

    void SetRandomInitVelocityRange(const glm::vec3& p_vMin, const glm::vec3& p_vMax) {
        m_bRandomInitVelocity = true;
        m_vInitVelMin = p_vMin;
        m_vInitVelMax = p_vMax;
    }

    // Color
    void SetInitColor(const glm::vec4& p_vColor) {
        m_bRandomInitColor = false;
        m_vInitColor = p_vColor;
    }

    void SetRandomInitColorRange(const glm::vec4& p_vMin, const glm::vec4& p_vMax) {
        m_bRandomInitColor = true;
        m_vInitColorMin = p_vMin;
        m_vInitColorMax = p_vMax;
    }

    // Fade
    void SetInitFade(float p_fFade) {
        m_bRandomInitFade = false;
        m_fInitFade = p_fFade;
    }

    void SetRandomInitFadeRange(float p_fMin, float p_fMax) {
        m_bRandomInitFade = true;
        m_fInitFadeMin = p_fMin;
        m_fInitFadeMax = p_fMax;
    }

    // Lifetime
    void SetInitLifetime(float p_fLifetime) {
        m_bRandomInitLifetime = false;
        m_fInitLifetime = p_fLifetime;
    }

    void SetRandomInitLifetimeRange(float p_fMin, float p_fMax) {
        m_bRandomInitLifetime = true;
        m_fInitLifeMin = p_fMin;
        m_fInitLifeMax = p_fMax;
    }

    // --- Continuous ---
    void SetBirthrate(int p_iBirthrate) {
        m_bRandomBirthrate = false;
        m_iBirthrate = p_iBirthrate;
    };

    void SetRandomBirthrateRange(int p_iMin, int p_iMax) {
        m_bRandomBirthrate = true;
        m_iBirthrateMin = p_iMin;
        m_iBirthrateMax = p_iMax;
    };

    // --- Burst ---
    void CalcBurstTime();

    void ToggleRepeatBurst(bool p_bRepeats) {m_bRepeatBurst = p_bRepeats;};

    void SetBurstTime(float p_fBurstTime) {
        m_bRandomBurstTime = false;
        m_fBurstTime = p_fBurstTime;
    };

    void SetRandomBurstTimeRange(float p_fMin, float p_fMax) {
        m_bRandomBurstTime = true;
        m_fBurstTimeMin = p_fMin;
        m_fBurstTimeMax = p_fMax;
    };

    void SetBurstAmount(int p_iBurstAmt) {
        m_bRandomBurstSpawn = false;
        m_iBurstSpawns = p_iBurstAmt;
    };
    
    void SetRandomBurstAmountRange(int p_iMin, int p_iMax) {
        m_bRandomBurstSpawn = true;
        m_iBurstSpawnsMin = p_iMin;
        m_iBurstSpawnsMax = p_iMax;
    };
};