#include "sample3D.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Scene.h"
#include "AddVelAffector.h"
#include "FadeAffector.h"
#include "ScaleAffector.h"

constexpr float MIN_DISTANCE = -100.0f;
constexpr float MAX_DISTANCE = 100.0f;
constexpr int MAX_PARENTS = 750;
constexpr int MAX_CHILDREN = 250;

//Clean up!
Sample3D::~Sample3D()
{
	printf("Destroying 3D Sample\n");
	wolf::BufferManager::DestroyBuffer(m_pVB);
	wolf::ProgramManager::DestroyProgram(m_pProgram);
}

void Sample3D::init()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	Scene::CreateInstance();
	m_pApp->disableCursor();
        
    // Only init if not already done
    if(!m_pProgram)
    {
		//Create our world and camera
		this->camera = new Camera(glm::vec3(0.0f, 1.0f, -10.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		this->camera->SetYaw(90.0f);
		Scene::Instance()->SetActiveCamera(this->camera);

		this->lastMousePos = m_pApp->getScreenSize() / 2.0f;

		//Create an effect using the FireEffect xml file
		Effect* fire = Effect::CreateWithXMLFile("./XML/FireEffect.xml");
		if (fire != nullptr) {
			Scene::Instance()->AddEffect(fire);
			m_pEffects.push_back(fire);
			fire->Play();
		}

		Effect* snow = Effect::CreateWithXMLFile("./XML/SnowEffect.xml");
		if (snow != nullptr) {
			Scene::Instance()->AddEffect(snow);
			m_pEffects.push_back(snow);
		}

		Effect* chickens = Effect::CreateWithXMLFile("./XML/ChickenEffect.xml");
		if (chickens != nullptr) {
			Scene::Instance()->AddEffect(chickens);
			m_pEffects.push_back(chickens);
		}
    }

    printf("Successfully initialized 3D Sample\n");
}

void Sample3D::update(float dt) 
{
	//Need a different version of deltatime because we need to count time since last frame not time elapsed
	currentTime = (float) glfwGetTime();
	m_deltaTime = currentTime - lastTime;
	lastTime = currentTime;

	//Process mouse movement
	mousePos = m_pApp->getMousePos();
	glm::vec2 mouseMovement = mousePos - lastMousePos;
	camera->ProcessMouseMovement(mouseMovement.x, -mouseMovement.y);
	lastMousePos = mousePos;

	if (m_pApp->isKeyJustDown('P')) {
		if (m_bIsPaused) {
			m_pEffects[m_iEffectIndex]->Play();
			m_bIsPaused = false;
		}
		else {
			m_pEffects[m_iEffectIndex]->Pause();
			m_bIsPaused = true;
		}
	}

	if (m_pApp->isKeyJustDown('O')) {
		m_pEffects[m_iEffectIndex]->Stop();
		m_bIsPaused = true;
	}

	if (m_pApp->isKeyJustDown('I')) {
		m_pEffects[m_iEffectIndex]->Restart();
		m_bIsPaused = false;
	}

	if (m_pApp->isKeyJustDown('Q')) {
		m_pEffects[m_iEffectIndex]->Stop();
		m_iEffectIndex++;
		if (m_iEffectIndex >= m_pEffects.size()) {
			m_iEffectIndex = 0;
		}
		m_pEffects[m_iEffectIndex]->Play();
	}

	//Tell the Scene to update
	Scene::Instance()->Update(dt);
}

void Sample3D::render(int width, int height)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Let the camera know how big our screen is currently
	camera->UpdateScreenSize((float) width, (float) height);

	//Process keyboard input to move the camera
	if (m_pApp->isKeyDown('W')) {
		camera->ProcessKeyboard(FORWARD, m_deltaTime);
	}

	if (m_pApp->isKeyDown('A')) {
		camera->ProcessKeyboard(LEFT, m_deltaTime);
	}

	if (m_pApp->isKeyDown('S')) {
		camera->ProcessKeyboard(BACKWARD, m_deltaTime);
	}

	if (m_pApp->isKeyDown('D')) {
		camera->ProcessKeyboard(RIGHT, m_deltaTime);
	}

	//Tell the Scene to render
	Scene::Instance()->Render();
}