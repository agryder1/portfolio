#pragma once
#include "general.h"

/*Pseudo random number generator that generates
  numbers within a specified range.
  lower: the lower bound
  upper: the upper bound
  returns: a random number*/
int genRandomInt(int lower, int upper) {
  return (rand() % (upper - lower + 1)) + lower;
}

float genRandomFloat(float lower, float upper) {
  return (rand() % (int)(upper - lower + 1)) + lower;
}

glm::vec2 genRandomVec2(const glm::vec2& min, const glm::vec2& max) {
  return glm::vec2(genRandomFloat(min.x, max.x), genRandomFloat(min.y, max.y));
}

glm::vec3 genRandomVec3(const glm::vec3& min, const glm::vec3& max) {
  return glm::vec3(genRandomFloat(min.x, max.x), genRandomFloat(min.y, max.y), genRandomFloat(min.z, max.z));
}

glm::vec4 genRandomVec4(const glm::vec4& min, const glm::vec4& max) {
  return glm::vec4(genRandomFloat(min.x, max.x), genRandomFloat(min.y, max.y), genRandomFloat(min.z, max.z), genRandomFloat(min.w, max.w));
}