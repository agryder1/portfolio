uniform mat4 projection;
uniform mat4 view;
uniform mat4 world;
uniform vec4 u_color;
in vec3 a_position;
out vec4 v_color;

void main()
{
    gl_Position = projection * view * world * vec4(a_position, 1.0f);
	v_color = u_color;
}
