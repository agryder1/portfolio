Q toggles the quadtree rendering!

To show off the hierarchy, parents are created with a color and randomly given children,
children have the same color as their parents and they are always close to their parents.
