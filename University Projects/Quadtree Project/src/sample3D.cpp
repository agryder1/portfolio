#include "sample3D.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Scene.h"
#include "QuadtreeBox.h"

constexpr float MIN_DISTANCE = -100.0f;
constexpr float MAX_DISTANCE = 100.0f;
constexpr int MAX_PARENTS = 750;
constexpr int MAX_CHILDREN = 250;

//Clean up!
Sample3D::~Sample3D()
{
	printf("Destroying 3D Sample\n");
	wolf::BufferManager::DestroyBuffer(m_pVB);
	wolf::ProgramManager::DestroyProgram(m_pProgram);
}

void Sample3D::init()
{
    glEnable(GL_DEPTH_TEST);
	Scene::CreateInstance();
	m_pApp->disableCursor();
        
    // Only init if not already done
    if(!m_pProgram)
    {
		//Create our world and camera
		this->camera = new Camera(glm::vec3(0.0f, MAX_DISTANCE + 50.0f, -10.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		this->camera->SetPitch(-45.0f);
		Scene::Instance()->SetActiveCamera(this->camera);

		this->lastMousePos = m_pApp->getScreenSize() / 2.0f;

		//Create some random nodes in a bounded area
		std::vector<Node*> nodes;
		for (int i = 0; i < MAX_PARENTS; i++) {
			DebugCube* cube = new DebugCube(glm::vec3(genRandom(MIN_DISTANCE, MAX_DISTANCE), MAX_DISTANCE - 1, genRandom(MIN_DISTANCE, MAX_DISTANCE)));
			cube->Scale(glm::vec3(genRandom(1, 2), genRandom(1, 2), genRandom(1,2)));

			cube->SetColor(colors[genRandom(0, 10)]); //We get a random color
			Scene::Instance()->AddNode(cube); //And we get added to the scene
			nodes.push_back(cube);
		}

		for (int k = 0; k < MAX_CHILDREN; k++) {
			DebugCube* sonCube = new DebugCube(glm::vec3(genRandom(-5, 5), genRandom(-5, 5), genRandom(-5, 5)));
			int index = genRandom(0, MAX_PARENTS - 1);
			DebugCube* parent = (DebugCube*)nodes.at(index);
			sonCube->SetColor(parent->GetColor());
			parent->AddChild(sonCube);
		}

		//Create a Quadtree that covers the whole bounded area
		Vertex quadtreeDimensions[8] = {
			//Min
			{MIN_DISTANCE, MIN_DISTANCE, MIN_DISTANCE},
			{MIN_DISTANCE, MIN_DISTANCE, MAX_DISTANCE},
			{MAX_DISTANCE, MIN_DISTANCE, MAX_DISTANCE},
			{MAX_DISTANCE, MIN_DISTANCE, MIN_DISTANCE},

			//Max
			{MIN_DISTANCE, MAX_DISTANCE, MIN_DISTANCE},
			{MIN_DISTANCE, MAX_DISTANCE, MAX_DISTANCE},
			{MAX_DISTANCE, MAX_DISTANCE, MAX_DISTANCE},
			{MAX_DISTANCE, MAX_DISTANCE, MIN_DISTANCE}		
		};
		
		QuadtreeBox* quadtree = new QuadtreeBox(quadtreeDimensions, nodes);
		quadtree->BuildQuadtree(); //And tell that tree to start building itself
		Scene::Instance()->SetQuadtree(quadtree); //Then attach the tree to the Scene
    }

    printf("Successfully initialized 3D Sample\n");
}

void Sample3D::update(float dt) 
{
	//Need a different version of deltatime because we need to count time since last frame not time elapsed
	currentTime = (float) glfwGetTime();
	m_deltaTime = currentTime - lastTime;
	lastTime = currentTime;

	//Process mouse movement
	mousePos = m_pApp->getMousePos();
	glm::vec2 mouseMovement = mousePos - lastMousePos;
	camera->ProcessMouseMovement(mouseMovement.x, -mouseMovement.y);
	lastMousePos = mousePos;

	//Toggle the quadtree's rendering
	if (m_pApp->isKeyJustDown('Q')) {
		Scene::Instance()->ToggleQuadtreeRendering();
	}

	//Tell the Scene to update
	Scene::Instance()->Update(dt);
}

void Sample3D::render(int width, int height)
{
	glClearColor(0.01f, 0.80f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Let the camera know how big our screen is currently
	camera->UpdateScreenSize((float) width, (float) height);

	//Process keyboard input to move the camera
	if (m_pApp->isKeyDown('W')) {
		camera->ProcessKeyboard(FORWARD, m_deltaTime);
	}

	if (m_pApp->isKeyDown('A')) {
		camera->ProcessKeyboard(LEFT, m_deltaTime);
	}

	if (m_pApp->isKeyDown('S')) {
		camera->ProcessKeyboard(BACKWARD, m_deltaTime);
	}

	if (m_pApp->isKeyDown('D')) {
		camera->ProcessKeyboard(RIGHT, m_deltaTime);
	}

	//Tell the Scene to render
	Scene::Instance()->Render();
}