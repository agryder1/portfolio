#pragma once
#include "general.h"
#include "Node.h"

class DebugCube : public Node { //Inherits from Node
private:
    //Shared geometry and cube counter
    static const Vertex m_cubeVertices[];
    static const unsigned int m_iCubeIndices[];
    static int m_iNumCubes;

    //Shared shader variables
    static GLuint m_pProgram;
    static GLuint m_pVBO;
    static GLuint m_pVAO;
    static GLuint m_pEBO;

    glm::vec4 m_vColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

public:
    DebugCube(const glm::vec3& position);
    ~DebugCube() override;

    //Should be able to have different colored cubes
    void SetColor(const glm::vec4& p_vColor) {m_vColor = p_vColor;};
    glm::vec4 GetColor() {return m_vColor;};

    void Update(float deltaTime);
    void Render(const glm::mat4& p_mProjection, const glm::mat4& p_mView);
};