#pragma once
#include "..\thirdparty\glew\include\GL\glew.h"
#include "..\thirdparty\glfw\include\GLFW\glfw3.h"
#include "..\thirdparty\glfw\include\GLFW\glfw3native.h"
#include "..\thirdparty\glm\glm\glm.hpp"
#include "..\wolf\wolf.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

const glm::vec4 colors[10] = {
	{1.0f, 0.0f, 0.0f, 1.0f},	 //red
	{1.0f, 0.65f, 0.0f, 1.0f},	 //orange
	{1.0f, 1.0f, 0.0f, 1.0f},	 //yellow
	{0.0f, 1.0f, 0.0f, 1.0f},	 //green
	{0.0f, 1.0f, 1.0f, 1.0f},	 //cyan
	{0.0f, 0.0f, 1.0f, 1.0f},	 //blue
	{0.63f, 0.13f, 0.94f, 1.0f}, //purple
	{1.0f, 0.0f, 0.50f, 1.0f},	 //pink
	{1.0f, 1.0f, 1.0f, 1.0f},	 //white
	{0.0f, 0.0f, 0.0f, 1.0f},	 //black
};

struct Vertex {
    GLfloat x, y, z;
};

struct Plane {
    GLfloat a, b, c, d;
    void Normalize() { //Planes need to be normalized!
        float length = glm::length(glm::vec3(a, b, c));
        a = a/length;
        b = b/length;
        c = c/length;
        d = d/length;
    }
};

//Used to hold Camera frustum(s)
struct Frustum {
    Plane planes[6];
};

//Used by DebugCubes
struct BoundingSphere {
    glm::vec3 center;
    float radius;
};

//Used by QuadtreeBoxes
struct AABB {
    Vertex minPoint;
    Vertex maxPoint;
};

bool SphereInFrustumCheck(const BoundingSphere* sphere, const Frustum* frustum);

//Pseudo random int number generator
int genRandom(int lower, int upper);
int genRandom(float lower, float upper);
