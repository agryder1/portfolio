#pragma once
#include "general.h"

/*Pseudo random number generator that generates
  numbers within a specified range.
  lower: the lower bound
  upper: the upper bound
  returns: a random number*/
int genRandom(int lower, int upper) {
  return (rand() % (upper - lower + 1)) + lower;
}

int genRandom(float lower, float upper) {
  return (rand() % (int)(upper - lower + 1)) + (int)lower;
}

//Sphere in Frustum check used to figure out what nodes in a quad should be rendered
//(This check is done *after* we check that the quad itself is in the frustum)
bool SphereInFrustumCheck(const BoundingSphere* sphere, const Frustum* frustum) {
    for (Plane p : frustum->planes) { //For each of the planes on the frustum
        //The distance between the center of the sphere and the plane
        float d = (sphere->center.x * p.a) + (sphere->center.y * p.b) + (sphere->center.z * p.c) + p.d;
        
        //Must be inside the sphere's radius
        if (d < -sphere->radius) {
          return false; //If it isn't then we're not intersecting
        }
        else if (abs(d) < sphere->radius) {
          return true; //If it is then we should render
        }
    }
    return true;
}