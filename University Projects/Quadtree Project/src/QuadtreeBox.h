#pragma once
#include "general.h"
#include "Node.h"

class QuadtreeBox {
public:
    //There are two constructors, one is public and creates a root,
    //the other is private and creates a branch or leaf
    QuadtreeBox(const Vertex minMaxArray[8], const std::vector<Node*> nodes);
    ~QuadtreeBox(); //Destructor
    
    void BuildQuadtree(); //Recursively build our tree
    void Render(const glm::mat4& projection, const glm::mat4& view); //Render the quadtree
    std::vector<Node*> RenderCheck(const Frustum* frustum); //Recursively check what nodes should be rendered

private:
    //Quadtree bounds (can't have too many levels or too many nodes in a quad)
    static const int maxLevel = 10;
    static const int maxNodes = 10;

    //Shared shader and vertex data for rendering the tree
    static std::vector<Vertex> lineData;
    static GLuint m_pProgram;
    static GLuint m_pVBO;
    static GLuint m_pVAO;

    //Private constructor we mentioned earlier
    QuadtreeBox(const Vertex minMaxArray[8], int p_iLevel);

    //Method that divides a QuadtreeBox into four smaller QuadtreeBoxes
    bool Subdivide();

    //Intersection tests
    bool SphereInBoxCheck(const QuadtreeBox* quad, Node* node);
    bool BoxInFrustumCheck(const QuadtreeBox* quad, const Frustum* frustum);

    //Recursive method used to collect the vertex data for rendering the tree once it has been built
    void FindLineData();
    
    Vertex points[8]; //The points that define this quad (4 for the bottom and 4 for the top)
    QuadtreeBox* quads[4] {nullptr}; //Array to hold our subdivisions (if we have any!)

    bool hasSubdivisions = false; //Bool used to check if we can/should subdivide in Subdivide()

protected:
    //These variables can be accessed by other QuadtreeBoxes because we
    //want to be able have the branches and root(s) access them when
    //they create new leaves or branches

    std::vector<Node*> interested; //These are the nodes this QuadtreeBox has access to
    AABB BoundingAABB; //The AABB that defines this quad's bounds
    int level; //What level of the tree we're on

};