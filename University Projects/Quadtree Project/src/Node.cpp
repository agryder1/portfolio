#include "Node.h"

//Inside the sphere == ||point - center|| <= radius
//On the surface of the sphere == ||point - center|| = radius

BoundingSphere Node::GetBounds() {
    if (dirty) { //Dirty check in case we check the bounds without calling LocalTransform()
        bounds.center = localPosition;
        bounds.radius = 0.5f * std::max(localScale.x, std::max(localScale.y, localScale.z));

        //If we have kids
        if (!children.empty()) { 
            glm::vec3 avgLocation = bounds.center; //We need to be in the middle of our family
            float furthestDistance = bounds.radius; //And we need to know which child is furthest away
            
            //Go through our children
            for (Node* child : children) {
                //Figure out where the kid is (relative to where we are)
                glm::vec3 childLocation = child->GetWorldPosition();

                //Update the average location so we get closer to the middle of the family
                avgLocation += childLocation;
            }

            //Find the mid-point between all the kiddos' locations
            avgLocation = avgLocation / ((float) children.size() + 1);

            bounds.center = avgLocation; //The midpoint is our bounds' new center

            for (Node* child : children) {
                glm::vec3 childLocation = child->GetWorldPosition();
                float distanceToChild = glm::distance(GetWorldPosition(), childLocation) + child->bounds.radius;

                if (distanceToChild > furthestDistance) { //If this child is the furthest so far
                    furthestDistance = distanceToChild; //Remember the distance from us to them
                }
            }

            bounds.radius = furthestDistance; //The distance between us and the furthest child's bounds is our new radius
        }
    }

    //Return our bounds
    return bounds;
}