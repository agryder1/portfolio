//NOTE: This Camera code was taken from learnopengl.com and edited as necessary
//https://learnopengl.com/code_viewer_gh.php?code=includes/learnopengl/camera.h

#ifndef CAMERA_H
#define CAMERA_H

#include "general.h"
#include "..\thirdparty\glew\include\GL\glew.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <vector>

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

// Default camera values
const float YAW         =  0.0f;
const float PITCH       =  0.0f;
const float SPEED       =  50.0f;
const float SENSITIVITY =  0.1f;
const float ZOOM        =  60.0f;
const float WIDTH       =  200.0f;
const float HEIGHT      =  600.0f;
const float NEAR        =  0.1f;
const float FAR         =  5000.0f;


// An abstract camera class that processes input and calculates the corresponding Euler Angles, Vectors and Matrices for use in OpenGL
class Camera
{
public:
    // constructor with vectors
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH, float screenWidth = WIDTH, float screenHeight = HEIGHT, float nearClip = NEAR, float farClip = FAR) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    {
        Position = position;
        WorldUp = up;
        Yaw = yaw;
        Pitch = pitch;
        Width = screenWidth;
        Height = screenHeight;
        Near = nearClip;
        Far = farClip;
        updateCameraVectors();
    }
    // constructor with scalar values
    Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch, float screenWidth, float screenHeight, float nearClip, float farClip) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    {
        Position = glm::vec3(posX, posY, posZ);
        WorldUp = glm::vec3(upX, upY, upZ);
        Yaw = yaw;
        Pitch = pitch;
        Width = screenWidth;
        Height = screenHeight;
        Near = nearClip;
        Far = farClip;
        updateCameraVectors();
    }

    //Getters and Setters for all camera attributes/settings
    glm::vec3 GetPosition() const {return Position;}
    void SetPosition(glm::vec3& p_vPosition) {Position = p_vPosition;}

    glm::vec3 GetFront() const {return Front;}
    void SetFront(glm::vec3& p_vFront) {Front = p_vFront;}

    glm::vec3 GetUp() const {return Up;}
    void SetUp(glm::vec3& p_vUp) {Up = p_vUp;}

    glm::vec3 GetWorldUp() const {return WorldUp;}
    void SetWorldUp(glm::vec3& p_vWorldUp) {WorldUp = p_vWorldUp;}

    float GetYaw() const {return Yaw;}
    void SetYaw(float p_fYaw) {Yaw = p_fYaw;}

    float GetPitch() const {return Pitch;}
    void SetPitch(float p_fPitch) {Pitch = p_fPitch;}

    float GetMovementSpeed() const {return MovementSpeed;}
    void SetMovementSpeed(float p_fSpeed) {MovementSpeed = p_fSpeed;}

    float GetMouseSensitivity() const {return MouseSensitivity;}
    void SetMouseSensitivity(float p_fSensitivity) {MouseSensitivity = p_fSensitivity;}

    float GetZoom() const {return Zoom;}
    void SetZoom(float p_fZoom) {Zoom = p_fZoom;}

    // returns the view matrix calculated using Euler Angles and the LookAt Matrix
    glm::mat4 GetViewMatrix() const
    {
        return glm::lookAt(Position, Position + Front, Up);
    }

    glm::mat4 GetProjectionMatrix() const {
        return glm::perspective(glm::radians(Zoom), Width / Height, Near, Far);
    }

    void UpdateScreenSize(float width, float height) {
        Width = width;
        Height = height;
    }

    // processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
    void ProcessKeyboard(Camera_Movement direction, float deltaTime)
    {
        float velocity = MovementSpeed * deltaTime;
        if (direction == FORWARD)
            Position += Front * velocity;
        if (direction == BACKWARD)
            Position -= Front * velocity;
        if (direction == LEFT)
            Position -= Right * velocity;
        if (direction == RIGHT)
            Position += Right * velocity;
    }

    // processes input received from a mouse input system. Expects the offset value in both the x and y direction.
    void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true)
    {
        xoffset *= MouseSensitivity;
        yoffset *= MouseSensitivity;

        Yaw   += xoffset;
        Pitch += yoffset;

        // make sure that when pitch is out of bounds, screen doesn't get flipped
        if (constrainPitch)
        {
            if (Pitch > 89.0f)
                Pitch = 89.0f;
            if (Pitch < -89.0f)
                Pitch = -89.0f;
        }

        // update Front, Right and Up Vectors using the updated Euler angles
        updateCameraVectors();
    }

    // processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
    void ProcessMouseScroll(float yoffset)
    {
        Zoom -= (float)yoffset;
        if (Zoom < 1.0f)
            Zoom = 1.0f;
        if (Zoom > 45.0f)
            Zoom = 45.0f;
    }

    Frustum* GetCameraFrustum() {
        //Compute the view frustum using Real Time Rendering's method (pages 983-984)
        Frustum cameraFrustum;
        glm::mat4 combinedMatrix = GetProjectionMatrix() * GetViewMatrix(); //M = PV
        
        //Compute Left Plane
        Plane left;
        left.a = combinedMatrix[0][3] + combinedMatrix[0][0];
        left.b = combinedMatrix[1][3] + combinedMatrix[1][0];
        left.c = combinedMatrix[2][3] + combinedMatrix[2][0];
        left.d = combinedMatrix[3][3] + combinedMatrix[3][0];
        left.Normalize();
        cameraFrustum.planes[0] = left;

        //Compute Right Plane
        Plane right;
        right.a = combinedMatrix[0][3] - combinedMatrix[0][0];
        right.b = combinedMatrix[1][3] - combinedMatrix[1][0];
        right.c = combinedMatrix[2][3] - combinedMatrix[2][0];
        right.d = combinedMatrix[3][3] - combinedMatrix[3][0];
        right.Normalize();
        cameraFrustum.planes[1] = right;

        //Compute Bottom Plane
        Plane bottom;
        bottom.a = combinedMatrix[0][3] + combinedMatrix[0][1];
        bottom.b = combinedMatrix[1][3] + combinedMatrix[1][1];
        bottom.c = combinedMatrix[2][3] + combinedMatrix[2][1];
        bottom.d = combinedMatrix[3][3] + combinedMatrix[3][1];
        bottom.Normalize();
        cameraFrustum.planes[2] = bottom;

        //Compute top Plane
        Plane top;
        top.a = combinedMatrix[0][3] - combinedMatrix[0][1];
        top.b = combinedMatrix[1][3] - combinedMatrix[1][1];
        top.c = combinedMatrix[2][3] - combinedMatrix[2][1];
        top.d = combinedMatrix[3][3] - combinedMatrix[3][1];
        top.Normalize();
        cameraFrustum.planes[3] = top;

        //Compute Near Plane
        Plane near;
        near.a = combinedMatrix[0][3] + combinedMatrix[0][2];
        near.b = combinedMatrix[1][3] + combinedMatrix[1][2];
        near.c = combinedMatrix[2][3] + combinedMatrix[2][2];
        near.d = combinedMatrix[3][3] + combinedMatrix[3][2];
        near.Normalize();
        cameraFrustum.planes[4] = near;

        //Compute Far Plane
        Plane far;
        far.a = combinedMatrix[0][3] - combinedMatrix[0][2];
        far.b = combinedMatrix[1][3] - combinedMatrix[1][2];
        far.c = combinedMatrix[2][3] - combinedMatrix[2][2];
        far.d = combinedMatrix[3][3] - combinedMatrix[3][2];
        far.Normalize();
        cameraFrustum.planes[5] = far;

        //Return frustum
        m_pFrustum = cameraFrustum;
        return &m_pFrustum;
    }

private:
    // camera Attributes
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    // screen Attributes
    float Width;
    float Height;
    // euler Angles
    float Yaw;
    float Pitch;
    // camera options
    float MovementSpeed;
    float MouseSensitivity;
    float Zoom;
    float Near;
    float Far;

    float lastFrame = 0.0f;
    Frustum m_pFrustum;

    // calculates the front vector from the Camera's (updated) Euler Angles
    void updateCameraVectors()
    {
        // calculate the new Front vector
        glm::vec3 front;
        front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        front.y = sin(glm::radians(Pitch));
        front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        Front = glm::normalize(front);
        // also re-calculate the Right and Up vector
        Right = glm::normalize(glm::cross(Front, WorldUp));  // normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
        Up    = glm::normalize(glm::cross(Right, Front));
    }
};
#endif