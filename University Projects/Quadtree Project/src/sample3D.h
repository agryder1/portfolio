#pragma once
#include "../wolf/wolf.h"
#include "../samplefw/Sample.h"
#include "Camera.h"
#include "DebugCube.h"

//Nothing really new here!

class Sample3D: public Sample
{
public:
    Sample3D(wolf::App* pApp) : Sample(pApp, "3D Transforms") {}
    ~Sample3D();

    void init() override;
    void update(float dt) override;
    void render(int width, int height) override;

private:
    wolf::VertexBuffer* m_pVB = 0;
    wolf::VertexDeclaration* m_pDecl = 0;
    wolf::Program* m_pProgram = 0;

    Camera* camera;
    float m_deltaTime = 0.0f;
    float currentTime = 0.0f;
    float lastTime = 0.0f;

    glm::vec2 lastMousePos;
    glm::vec2 mousePos;
};
