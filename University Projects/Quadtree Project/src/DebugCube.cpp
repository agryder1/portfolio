#include "DebugCube.h"

//Need to keep track of how many cubes we've created so that we can delete them later
int DebugCube::m_iNumCubes = 0;

//Shader variables
GLuint DebugCube::m_pProgram;
GLuint DebugCube::m_pVBO = 0;
GLuint DebugCube::m_pVAO = 0;
GLuint DebugCube::m_pEBO = 0;

//Shared geometry
const Vertex DebugCube::m_cubeVertices[] = 
{
	//Bottom
	{ -0.5f, -0.5f, -0.5f},
	{ -0.5f,  -0.5f, 0.5f},
	{  0.5f,  -0.5f, 0.5f},
	{  0.5f,  -0.5f, -0.5f},

	// Top
	{ -0.5f, 0.5f, -0.5f},
	{ -0.5f,  0.5f, 0.5f},
	{  0.5f,  0.5f, 0.5f},
	{  0.5f,  0.5f, -0.5f},
};

//Shared indices for said geometry
const unsigned int DebugCube::m_iCubeIndices[] =
{
	//Bottom
	0, 1, 2,
	2, 3, 0,

	//Top
	4, 5, 6,
	6, 7, 4,

	//Front
	5, 1, 2,
	2, 6, 5,

	//Right
	6, 2, 3,
	3, 7, 6,

	//Back
	7, 3, 0,
	0, 4, 7,

	//Left
	4, 0, 1,
	1, 5, 4
};

//Constructor
DebugCube::DebugCube(const glm::vec3& position) {
	//If this is the first cube (or we've deleted all of them and we're starting fresh)
	if (!m_pProgram) {
		//Set up our shader
		m_pProgram = wolf::LoadShaders("data/cube.vsh", "data/cube.fsh");

		glGenVertexArrays(1, &m_pVAO);
		glGenBuffers(1, &m_pVBO);
		glGenBuffers(1, &m_pEBO);

		glBindVertexArray(m_pVAO);
		glBindBuffer(GL_ARRAY_BUFFER, m_pVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * 8, m_cubeVertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pEBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * 36, m_iCubeIndices, GL_STATIC_DRAW);

		glVertexAttribPointer(0,       // Attribute location
							3,              // Number of components
							GL_FLOAT,       // Type of each component
							GL_FALSE,       // Normalize?
							sizeof(Vertex), // Stride
							0);             // Offset
		glEnableVertexAttribArray(0);
	}

	//Init our position and force an initial bounds and transform calculation
	this->SetPosition(position);
	this->GetBounds();
	this->GetLocalTransform();
	m_iNumCubes++;
}

//Destructor
DebugCube::~DebugCube() {
	if (m_iNumCubes == 1) { //If this is the last cube
		//Delete the shader and buffers
		glDeleteVertexArrays(1, &m_pVAO);
    	glDeleteBuffers(1, &m_pVBO);
    	glDeleteBuffers(1, &m_pEBO);
		glDeleteProgram(m_pProgram);
	}

	//Otherwise, just deplete the count and delete all our children
	m_iNumCubes -= 1;
	if (!children.empty()) {
		for (Node* child : children) {
			delete(child);
		}
	}

}

//Update
void DebugCube::Update(float deltaTime) {
	//Recalculate our transform and bounds
	this->GetLocalTransform();

	//Tell all our children to do the same
	if (!children.empty()) {
		for (Node* child : children) {
			child->Update(deltaTime);
		}
	}
}

//Render
void DebugCube::Render(const glm::mat4& p_mProjection, const glm::mat4& p_mView) {

	//Get our shader ready
	glUseProgram(m_pProgram);

	glUniformMatrix4fv(glGetUniformLocation(m_pProgram, "projection"), 1, GL_FALSE, glm::value_ptr(p_mProjection));
	glUniformMatrix4fv(glGetUniformLocation(m_pProgram, "view"), 1, GL_FALSE, glm::value_ptr(p_mView));
	//Recalculate world transform
	glUniformMatrix4fv(glGetUniformLocation(m_pProgram, "world"), 1, GL_FALSE, glm::value_ptr(this->GetWorldTransform()));
	glUniform4fv(glGetUniformLocation(m_pProgram, "u_color"), 1, glm::value_ptr(this->GetColor()));

	glBindVertexArray(m_pVAO);

	//Render our geometry
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

	//Tell all our children to render
	if (!children.empty()) {
		for (Node* child : children) {
			child->Render(p_mProjection, p_mView);
		}
	}

}