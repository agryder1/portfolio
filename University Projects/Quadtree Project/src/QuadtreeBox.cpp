#include "QuadtreeBox.h"

//This vector will be filled with all of the vertex data
//we need to render the quadtree after it has been built
std::vector<Vertex> QuadtreeBox::lineData;

//Shader variables
GLuint QuadtreeBox::m_pProgram;
GLuint QuadtreeBox::m_pVBO = 0;
GLuint QuadtreeBox::m_pVAO = 0;

//This constructor is public and is used to initalize a quadtree -- it creates a root
QuadtreeBox::QuadtreeBox(const Vertex* minMaxArray, std::vector<Node*> nodes) : level(0), interested(nodes) {
    //We should only be getting 8 points (4 bottom, 4 top)
    //But if by some sorcery we get more than that
    for (int i = 0; i < 8; i++) { //Pretend we didn't.
        points[i] = minMaxArray[i];
    }

    //All the boxes are identical and relative to each other
    //So we should be able to take the bottom left and top right
    //And use them as the min and max points
    BoundingAABB.minPoint = points[0];
    BoundingAABB.maxPoint = points[6];
}

//This constructor is private and is used to create branches and leaves
QuadtreeBox::QuadtreeBox(const Vertex* minMaxArray, int p_iLevel) : level(p_iLevel) {
    for (int i = 0; i < 8; i++) {
        points[i] = minMaxArray[i];
    }

    BoundingAABB.minPoint = points[0];
    BoundingAABB.maxPoint = points[6];
}

//Destructor
QuadtreeBox::~QuadtreeBox() {
    if (quads[0] != nullptr) { //Have to delete all the leaves and branches, too!
        delete(quads[0]);
        delete(quads[1]);
        delete(quads[2]);
        delete(quads[3]);
    }
}

//This method creates four new QuadtreeBoxes and sets them up as
//new quads for the calling instance
bool QuadtreeBox::Subdivide() {
    if (!hasSubdivisions) { //If we haven't already subdivided
        if (level == maxLevel) { //And we *can* subdivide
            return false;
        };

        //Find the mid points on the original Quadtreebox for both the bottom and top

        //Minimum midpoints
        //[min = 0][ TopMid  ][min = 3 ]
        //[LeftMid][ TrueMid ][RightMid]
        //[min = 1][BottomMid][min = 2 ]
        Vertex minTopMid    = {(points[0].x + points[3].x) / 2.0f, points[0].y, points[0].z};
        Vertex minLeftMid   = {points[0].x,                        points[0].y, (points[0].z + points[1].z) / 2.0f};
        Vertex minTrueMid   = {(points[0].x + points[3].x) / 2.0f, points[0].y, (points[0].z + points[1].z) / 2.0f};
        Vertex minBottomMid = {(points[1].x + points[2].x) / 2.0f, points[0].y, points[1].z};
        Vertex minRightMid  = {points[3].x,                        points[0].y, (points[3].z + points[2].z) / 2.0f};

        //Max midpoints
        //[max = 4][ TopMid  ][max = 7 ]
        //[LeftMid][ TrueMid ][RightMid]
        //[max = 5][BottomMid][max = 6 ]
        Vertex maxTopMid    = {(points[4].x + points[7].x) / 2.0f, points[4].y, points[4].z};
        Vertex maxLeftMid   = {points[4].x,                        points[4].y, (points[4].z + points[5].z) / 2.0f};
        Vertex maxTrueMid   = {(points[4].x + points[7].x) / 2.0f, points[4].y, (points[4].z + points[5].z) / 2.0f};
        Vertex maxBottomMid = {(points[5].x + points[6].x) / 2.0f, points[4].y, points[5].z};
        Vertex maxRightMid  = {points[7].x,                        points[4].y, (points[7].z + points[6].z) / 2.0f};

        //Then assemble new quads using the midpoints and the original geometry

        //[X][ ] <-- quadZero is here
        //[ ][ ]
        Vertex quadZero[] = {
            points[0],
            minLeftMid,
            minTrueMid,
            minTopMid,
            points[4],
            maxLeftMid,
            maxTrueMid,
            maxTopMid
        };

        //[ ][X] <-- quadOne is here
        //[ ][ ]
        Vertex quadOne[] = {
            minTopMid,
            minTrueMid,
            minRightMid,
            points[3],
            maxTopMid,
            maxTrueMid,
            maxRightMid,
            points[7]
        };

        //[ ][ ]
        //[X][ ] <-- here's quadTwo
        Vertex quadTwo[] = {
            minLeftMid,
            points[1],
            minBottomMid,
            minTrueMid,
            maxLeftMid,
            points[5],
            maxBottomMid,
            maxTrueMid
        };

        //[ ][ ]
        //[ ][X] <-- and this is quadThree
        Vertex quadThree[] = {
            minTrueMid,
            minBottomMid,
            points[2],
            minRightMid,
            maxTrueMid,
            maxBottomMid,
            points[6],
            maxRightMid
        };

        //Now that we've assembled our quads we can store them
        quads[0] = new QuadtreeBox(quadZero,  level+1); //Don't forget: these guys are one level higher (lower?) than us
        quads[1] = new QuadtreeBox(quadOne,   level+1);
        quads[2] = new QuadtreeBox(quadTwo,   level+1);
        quads[3] = new QuadtreeBox(quadThree, level+1);

        //Prevent future subdivisions for this QuadtreeBox
        hasSubdivisions = true;
    }

    //We've been subdivided!
    return true;
}

//Recursively build the quadtree
void QuadtreeBox::BuildQuadtree() {
    //First check if our subdivisions have been made (if we can have them!)
    if (Subdivide()) {
        //Then go through all of the nodes that are currently at our level
        for (Node* node : interested) {
            for (QuadtreeBox* quad : quads) { //And distribute them amongst our subdivisions
                if (SphereInBoxCheck(quad, node)) { //Do a Sphere/AABB check to figure out where each node goes
                    quad->interested.push_back(node);
                }
            }
        }

        //Then go through all of our subdivisions
        for (QuadtreeBox* quad : quads) {
            //And if we've given them too many nodes
            if (quad->interested.size() > maxNodes) {
                //Tell them to follow the same process we just did to subdivide and redistribute them
                quad->BuildQuadtree();
            }
        }

        //By this point we shouldn't have direct access to any of the nodes so we can clear our list
        //(We do this here instead of earlier cause you shouldn't try to iterate and remove things simultaneously)
        interested.clear(); //(There are ways to do it.... but eh)
    }
    if (level == 0) { //If this is the root node
        //Then by this point the tree is built and we need to figure out what
        //our renderable geometry looks like

        //Use this recursive method to traverse the tree and build up vertex data
        FindLineData();

        //If the QuadtreeBox shader/buffer hasn't been made yet
        if (!m_pProgram) {
            //Set 'er up
            m_pProgram = wolf::LoadShaders("data/line.vsh", "data/line.fsh");

            glGenVertexArrays(1, &m_pVAO);
            glGenBuffers(1, &m_pVBO);

            glBindVertexArray(m_pVAO);
            glBindBuffer(GL_ARRAY_BUFFER, m_pVBO);
            glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * lineData.size(), lineData.data(), GL_STATIC_DRAW);

            glVertexAttribPointer(0,       // Attribute location
                                3,              // Number of components
                                GL_FLOAT,       // Type of each component
                                GL_FALSE,       // Normalize?
                                sizeof(Vertex), // Stride
                                0);             // Offset
            glEnableVertexAttribArray(0);
        }
    }
}

//Recursive function to traverse the tree and collect our
//vertex data for rendering later (called by a root node in BuildQuadtree())
void QuadtreeBox::FindLineData() {
    if (quads[0] != nullptr) { //If we have subdivisions
        for (QuadtreeBox* quad : quads) {
            quad->FindLineData(); //We're not at the lowest level(s) yet so go down
        }
    }
    else { //If we've reached a leaf node
        //Store it's top vertices in the order that makes GL_LINES happy

        //Line 1
        lineData.push_back(points[4]);
        lineData.push_back(points[5]);

        //Line 2
        lineData.push_back(points[5]);
        lineData.push_back(points[6]);

        //Line 3
        lineData.push_back(points[6]);
        lineData.push_back(points[7]);

        //Line 4
        lineData.push_back(points[7]);
        lineData.push_back(points[4]);
    }
}

//Sphere in AABB check that lets us figure out what subdivision a node goes in
bool QuadtreeBox::SphereInBoxCheck(const QuadtreeBox* quad, Node* node) {
    //Get the bounds for each shape
    AABB box = quad->BoundingAABB;
    BoundingSphere sphere = node->GetBounds();

    //Find the point on the AABB that is closest to the sphere's center
    glm::vec3 closestPoint = glm::vec3(
        std::max(box.minPoint.x, std::min(sphere.center.x, box.maxPoint.x)),
        std::max(box.minPoint.y, std::min(sphere.center.y, box.maxPoint.y)),
        std::max(box.minPoint.z, std::min(sphere.center.z, box.maxPoint.z))
    );

    //Calculate the distance (squared) between the two
    double distanceMin = glm::distance(sphere.center, closestPoint);

    //If the distance is less than the radius then we're intersecting
    if (distanceMin <= sphere.radius) {return true;}
    else {return false;}
}

//AABB in Frustum check used to figure out what quads can be seen by the camera
bool QuadtreeBox::BoxInFrustumCheck(const QuadtreeBox* quad, const Frustum* frustum) {
    //Go through each of the planes on the frustum
    for (Plane plane : frustum->planes) {
        //If all 8 of the AABB's corner points fail to intersect with a plane
        //then we're not in the frustum
        int nonIntersectingPoints = 0;

        for (Vertex point : quad->points) { //Check each of the AABB's corner points
            //If we plug the point into the plane's equation
            float result = plane.a*point.x + plane.b*point.y + plane.c*point.z + plane.d;
            if (result < 0) { //And it isn't intersecting the plane
                nonIntersectingPoints++; //Then mark it as a failed point
            }
        }
        if (nonIntersectingPoints == 8) { //If we've gotten 8 failed points for 1 plane
            return false; //Then we're not in the frustum
        }
    }
    return true; //Otherwise, we're in the frustum!
}

//This method recursively traverses the tree to figure out what quads/nodes are in the frustum and should be rendered
std::vector<Node*> QuadtreeBox::RenderCheck(const Frustum* frustum) {
    std::vector<Node*> shouldBeRendered; //Vector we'll add nodes to as we travel the tree

    if (BoxInFrustumCheck(this, frustum)) { //If this quad is in the frustum
        if (quads[0] != nullptr) { //And it has subdivisions
            for (QuadtreeBox* quad : quads) {
                std::vector<Node*> inFrustum = quad->RenderCheck(frustum); //Tell that quad to do the same check

                //Take whatever nodes the quad returns (if any)
                if (inFrustum.size() > 0) {
                    //And add them to the list of nodes to be rendered
                    shouldBeRendered.insert(shouldBeRendered.end(), inFrustum.begin(), inFrustum.end());
                }
            }
        }
        else { //If this quad does NOT have subdivisions it's a leaf
            for (Node* node : interested) { //So we go through all of its nodes
                if (SphereInFrustumCheck(&node->GetBounds(), frustum)) { //And if they're in the frustum
                    shouldBeRendered.push_back(node); //We let whoever called us know that they should be rendered
                }
            }
        }
    }
    return shouldBeRendered; //Otherwise, we return an empty list of nodes
}

//Render the quadtree
void QuadtreeBox::Render(const glm::mat4& p_mProjection, const glm::mat4& p_mView) {
    //Get the shader ready
	glUseProgram(m_pProgram);

	glUniformMatrix4fv(glGetUniformLocation(m_pProgram, "projection"), 1, GL_FALSE, glm::value_ptr(p_mProjection));
	glUniformMatrix4fv(glGetUniformLocation(m_pProgram, "view"), 1, GL_FALSE, glm::value_ptr(p_mView));
    glm::mat4 world = glm::mat4(1.0f); //Our world matrix isn't anything special
	glUniformMatrix4fv(glGetUniformLocation(m_pProgram, "world"), 1, GL_FALSE, glm::value_ptr(world));

	glBindVertexArray(m_pVAO);

    //Draw lines with the data we set up earlier
	glDrawArrays(GL_LINES, 0, (GLsizei) lineData.size());
}
