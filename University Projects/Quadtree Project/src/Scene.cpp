#include "Scene.h"
#include "DebugCube.h"

//Static variable to hold the Singleton instance
Scene* Scene::m_pSceneInstance;

//Add a node to the scene
bool Scene::AddNode(Node* p_pNode) {
    if (!m_pNodes.empty()) { //If we have nodes already
        //Make sure we're not adding a duplicate
        std::vector<Node*>::iterator it = std::find(m_pNodes.begin(), m_pNodes.end(), p_pNode);
        if (it != m_pNodes.end()) {
            return false;
        }
    }

    //If we don't have nodes yet then this can't be a duplicate so we just add it
    m_pNodes.push_back(p_pNode);
    return true;
}

//Remove a node from the scene
bool Scene::RemoveNode(Node* p_pNode) {
    if (!m_pNodes.empty()) { //If we have nodes
        std::vector<Node*>::iterator it = std::find(m_pNodes.begin(), m_pNodes.end(), p_pNode);
        if (it != m_pNodes.end()) { //And the node we're trying to remove is in the scene
            m_pNodes.erase(it); //Remove it
            return true;
        }
    }
    return false; //Otherwise, don't do anything!
}

//Update the scene
void Scene::Update(float deltatTime) {
    m_pNodesToRender.clear(); //Clear out the list of things we're gonna render

    for (Node* node : m_pNodes) { //Go through all of our nodes
        node->Update(deltatTime); //Get them to update their transforms and bounds
        //m_pNodesToRender.push_back(node);
    }

    //If we have a quadtree use it to figure out what to render
    if (m_pQuadtree != nullptr) {
       m_pNodesToRender = m_pQuadtree->RenderCheck(m_pSceneCamera->GetCameraFrustum());
    }
    else if (m_pSceneCamera != nullptr) {
        for (Node* node : m_pNodes) {
            if (SphereInFrustumCheck(&node->GetBounds(), m_pSceneCamera->GetCameraFrustum())) {
                m_pNodesToRender.push_back(node);
            }
        }
    }
    else { //Otherwise just render everything...
        m_pNodesToRender.insert(m_pNodesToRender.end(), m_pNodes.begin(), m_pNodes.end());
    }

}

//Render the scene
void Scene::Render() {
    //Can't render without a camera!
    if (m_pSceneCamera != nullptr) {
        //Get our projection and view matrices from the camera
        glm::mat4 projection = m_pSceneCamera->GetProjectionMatrix();
        glm::mat4 view = m_pSceneCamera->GetViewMatrix();

        if (renderQuadtree) {
            //Render the quadtree
            m_pQuadtree->Render(projection, view);
        }

        //Then go through our list of things to be rendered
        for (Node* node : m_pNodesToRender) {
            node->Render(projection, view); //And tell them to render with the matrices we just retrieved
        }
    }
}