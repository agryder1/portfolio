#pragma once
#include "general.h"
#include "Node.h"
#include "Camera.h"
#include "QuadtreeBox.h"

//Singleton Scene that holds all our nodes and our quadtree
class Scene {
public:
    //Create an instance
    static void CreateInstance() {
        assert(m_pSceneInstance == NULL); //As long as there isn't already one!
        m_pSceneInstance = new Scene();
    };

    //Destroy the instance
    static void DestroyInstance() {
        assert(m_pSceneInstance != NULL); //If it exists..
        delete(m_pSceneInstance);
        m_pSceneInstance = NULL;
    };

    //Get the instance
    static Scene* Instance() {
        assert(m_pSceneInstance); //If it exists..
        return m_pSceneInstance;
    };

    //Add/remove a camera
    void SetActiveCamera(Camera* p_pCamera) {m_pSceneCamera = p_pCamera;};
    bool RemoveActiveCamera() {
        if (m_pSceneCamera != nullptr) {
            free(m_pSceneCamera);
            return true;
        }
        return false;
    };

    //Add/remove a quadtree (note that removing the quadtree will destroy it)
    void SetQuadtree(QuadtreeBox* p_pQuadtreeBox) {m_pQuadtree = p_pQuadtreeBox;};
    bool DestroyQuadtree() {
        if (m_pQuadtree != nullptr) {
            delete(m_pQuadtree); //We want to delete it because it has access to our nodes!
            free(m_pQuadtree);
            return true;
        }
        return false;
    };

    //Add/remove nodes from the scene
    bool AddNode(Node* p_pNode);
    bool RemoveNode(Node* p_pNode);

    void ToggleQuadtreeRendering() {renderQuadtree = !renderQuadtree;};

    //Update and render the scene's contents
    void Update(float deltaTime);
    void Render();

private:
    //Private constructor and destructor 'cause we're a Singleton
    Scene() {};
    ~Scene() {
        //Don't forget to delete our camera and quadtree
        free(m_pSceneCamera);
        free(m_pQuadtree);

        //And destroy all the nodes in the scene
        if (!m_pNodes.empty()) {
            for (Node* node : m_pNodes) {
                node->~Node();
            }
        }
    };

    static Scene* m_pSceneInstance; //The instance variable
    std::vector<Node*> m_pNodes; //The list of nodes in the scene
    std::vector<Node*> m_pNodesToRender; //The nodes that we're going to render

    //Variables to hold our camera and quadtree
    Camera* m_pSceneCamera = nullptr;
    QuadtreeBox* m_pQuadtree = nullptr;

    bool renderQuadtree = true;
};