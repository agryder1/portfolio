#pragma once
#include "general.h"
#include <unordered_map>

class Node { //DebugCube inherits from this class
    protected:
        const std::string name; //Our name shouldn't change once we set it

        BoundingSphere bounds; //Nodes can only have BoundingSpheres at the moment

        //Hierarchy variables
        Node* parent = nullptr;
        std::vector<Node*> children;

        //Local transform variables
        glm::vec3 localScale = glm::vec3(1.0f, 1.0f, 1.0f);
        glm::vec3 localRotation = glm::vec3(0.0f, 0.0f, 0.0f);
        glm::vec3 localPosition = glm::vec3(0.0f, 0.0f, 0.0f);
        glm::mat4 localTransform = glm::mat4(1.0f);
        bool dirty = false; //Flag to prevent unnecessary transform recalculations

        //Only Nodes should be able to add/remove their parent
        bool AddParent(Node* newDad) {
            if (parent != NULL) { //If we already have a parent
                return false; //We can't have another one until we remove the last one properly
            }
            parent = newDad; //Otherwise just adopt the new guy
            return true;
        }

        bool RemoveParent() {
            if (parent != NULL) { //If we have a parent
                parent = NULL; //Disown (but don't destroy) them
                return true;
            }
            return false;
        }

    public:
        Node() {}; //It is possible (although a bit silly) to make empty Nodes
        virtual ~Node() {}; //Subclasses should implement their own destructor

        std::string GetName() const {return name;} //Get our name

        //Set our local transform variables directly (overwrites old transform)
        void SetScale(const glm::vec3& newScale) {localScale = newScale; dirty = true;};
        void SetRotation(const glm::vec3& newRotation) {localRotation = newRotation; dirty = true;};
        void SetPosition(const glm::vec3& newPosition) {localPosition = newPosition; dirty = true;};

        //Perform a transformation based on our current transform (adjusts current transform)
        void Scale(const glm::vec3& scale) {localScale *= scale; dirty = true;};
        void Rotation(const glm::vec3& rotate) {localRotation += rotate; dirty = true;};
        void Position(const glm::vec3& position) {localPosition += position; dirty = true;};

        BoundingSphere GetBounds(); //Retrieve our bounds

        //Get our local transform (relative to our origin)
        //(defined and declared here because its not likely to change)
        glm::mat4 GetLocalTransform() {
            if (dirty) { //Only recalculate if necessary
                localTransform = glm::translate(glm::mat4(1.0f), localPosition);
                localTransform = localTransform * glm::mat4_cast(glm::quat(glm::vec3(glm::radians(localRotation.x), glm::radians(localRotation.y), glm::radians(localRotation.z))));
                localTransform = glm::scale(localTransform, localScale);

                GetBounds(); //Keep our bounds up to date
                dirty = false;
            }
            return localTransform;
        }

        //Get our global transform (relative to our parent's origin)
        //(defined and declared here because its not likely to change)
        glm::mat4 GetWorldTransform() {
            if (parent != NULL) { //If we have a parent
                return parent->GetWorldTransform() * GetLocalTransform(); //Get our transform relative to them
            }
            return GetLocalTransform(); //Otherwise, just return our local transform
        }

        //Sometimes we *just* want position (relative to our origin)
        glm::vec3 GetLocalPosition() const {
            return localPosition;
        }

        //And sometimes we just want our *world* position (relative to our parents' origin)
        glm::vec3 GetWorldPosition() const {
            if (parent != NULL) { //If we have a parent
                return localPosition + parent->GetLocalPosition(); //Get our position relative to theirs
            }
            return localPosition; //Otherwise, just return our local position
        }

        //Each subclass of Node should provide its own Update and Render methods because its possible
        //that a Node won't have to do either. Default behavior is just to call our childrens' methods
        virtual void Update(float deltaTime) {
            if (!children.empty()) {
                for (Node* child : children) {
                    child->Update(deltaTime);
                }
            }
        };
        virtual void Render(const glm::mat4& p_mProjection, const glm::mat4& p_mView) { // <-- VGA's naming conventions are following me...
            if (!children.empty()) {
                for (Node* child : children) {
                    child->Render(p_mProjection, p_mView);
                }
            }
        };
        
        //Adoption!
        bool AddChild(Node* newSon) {
            //If we haven't already adopted this kiddo
            if (std::find(children.begin(), children.end(), newSon) == children.end()) {
                //Become their parent and add 'em to the list
                newSon->AddParent(this);
                children.push_back(newSon);
                return true;
            }
            return false; //I love kids too but we really don't need to adopt the same kid twice..
        };

        //Child abandonment builds character, right?
        bool RemoveChild(Node* child) {
            //If this is one of our kids
            std::vector<Node*>::iterator it = std::find(children.begin(), children.end(), child);
            if (it != children.end()) {
                children.at(it - children.begin())->RemoveParent(); //Then disown them
                children.erase(it); //And take them off the list
                return true;
            }
            return false; //We can't disown a kid that isn't ours
        }
};